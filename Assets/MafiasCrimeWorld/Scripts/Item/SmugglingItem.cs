﻿// only usable items need minLevel and usage functions
using Newtonsoft.Json;
using System.Text;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/Items/Smuggling Item", order = 999)]
public class SmugglingItem : ScriptableItem
{
    public override void OnValidate()
    {
        base.OnValidate();

        sellable = false;
    }
}
