﻿// only usable items need minLevel and usage functions
using Newtonsoft.Json;
using System.Text;
using UnityEngine;

public abstract class UsableItem : ScriptableItem
{
    [Header("Requirements")]
    [JsonIgnore]
    public int strengthRequirement;
    [JsonIgnore]
    public int intelligenceRequirement;

    [Header("Cooldown Buff")]
    [JsonIgnore]
    public ScriptableBuff cooldownBuff;

    // [Server] Use logic: make sure to call base.Use() in overrides too.
    public virtual void Use(Player player, int inventoryIndex)
    {
        // start cooldown buff (if any)
        if (cooldownBuff != null)
        {
            if (player.strength >= strengthRequirement && player.intelligence >= intelligenceRequirement)
            {
                // apply the buff
                cooldownBuff.Apply(player);
            }
        }
    }

    // usage ///////////////////////////////////////////////////////////////////
    // [Server] and [Client] CanUse check for UI, Commands, etc.
    public virtual bool CanUse(Player player, int inventoryIndex)
    {
        return player.strength >= strengthRequirement && player.intelligence >= intelligenceRequirement;
    }

    public virtual void OnUsed(Player player) { }

}
