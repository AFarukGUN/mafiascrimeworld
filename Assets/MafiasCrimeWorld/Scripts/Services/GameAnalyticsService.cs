﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class GameAnalyticsService : MonoBehaviour
{
    public static GameAnalyticsService instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        GameAnalytics.Initialize();


    }

    public void AddBusinessEvent(string currency, int amount, string itemType, string itemId, string cartType = "")
    {
        GameAnalytics.NewBusinessEvent( currency,  amount,  itemType, itemId, cartType);
    }

    public void AddProgressionEvent(GAProgressionStatus gAProgressionStatus, string progression, int score)
    {
        GameAnalytics.NewProgressionEvent(gAProgressionStatus, progression, score);
    }

    public void AddDesignEvent(string eventName, float eventValue)
    {
        GameAnalytics.NewDesignEvent( eventName, eventValue);
    }

    public void AddErrorEvent(GAErrorSeverity severity, string message)
    {
        GameAnalytics.NewErrorEvent(severity, message);
    }
}
