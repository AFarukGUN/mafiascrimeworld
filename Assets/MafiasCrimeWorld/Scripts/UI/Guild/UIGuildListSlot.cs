﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGuildListSlot : MonoBehaviour {

    public Text textGuildName;
    	

    public void ApplyforGuild()
    {
        MafiasCrimeWorldClient.instance.ApplyToGuildRequest(textGuildName.text);
    }
}
