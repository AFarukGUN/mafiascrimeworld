﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInvitedGuildSlot : MonoBehaviour {
    
    public Text textGuildName;
    
    
    public void AcceptGuildRequest()
    {
        MafiasCrimeWorldClient.instance.AcceptGuildRequest(textGuildName.text);
    }

    public void RejectGuildRequest()
    {
        MafiasCrimeWorldClient.instance.RejectGuildRequest(textGuildName.text);
    }
}
