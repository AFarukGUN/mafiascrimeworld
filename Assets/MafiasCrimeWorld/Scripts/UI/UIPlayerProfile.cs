﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerProfile : MonoBehaviour
{

    public static UIPlayerProfile instance;

    public Text charName;

    public Text totalPvEKill;
    public Text totalPvEDeath;

    public Text totalArenaKill;
    public Text totalArenaDeath;

    public Image vipImage;
    public Text vipName;
    public Text vipTimeRemaining;

    public Sprite noVipSprite;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.instance == null)
            return;

        Player player = Player.instance;

        charName.text = player.charName;

        totalPvEKill.text = player.totalPvEKill.ToString();
        totalPvEDeath.text = player.totalPvEDeath.ToString();

        totalArenaKill.text = player.totalArenaKill.ToString();
        totalArenaDeath.text = player.totalArenaDeath.ToString();

        if (player.vip.hash == 0)
        {
            vipImage.sprite = noVipSprite;
            vipName.text = "VIP";

            vipTimeRemaining.gameObject.SetActive(false);
            
        }
        else
        {
            vipImage.sprite = player.vip.data.image;
            vipName.text = player.vip.data.name;

            float VIPTimeRemaining = Player.instance.vip.VIPTimeRemaining();

            TimeSpan ts = TimeSpan.FromSeconds(VIPTimeRemaining);

            vipTimeRemaining.text = ts.Days + ":" + ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;

            vipTimeRemaining.gameObject.SetActive(true);
            
        }
    }

    public void VIPButtonClick()
    {
        UIManager.instance.CloseMenuPanels();
        UIManager.instance.OpenVIPMenuPanel(true);
    }
}
