﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICommitCrime : MonoBehaviour {

    public ScriptableEnemy enemy;

    public Image image;

    public Text nameText;
    public Text descText;

    public Text healthText;
    public Text moneyText;
    public Text crimePointText;

    public Button doJobButton;
    public Text remainingTimeText;


    private void Awake()
    {
        if (enemy != null)
        {
            image.sprite = enemy.image;

            healthText.text = enemy.healthRequirement.ToString();
            moneyText.text = enemy.moneyMin.ToString() + "-" + enemy.moneyMax.ToString();
            crimePointText.text = enemy.crimePointMin.ToString() + "-" + enemy.crimePointMax;
        }
    }

    public void Update()
    {
        if (enemy != null)
        {
            nameText.text = LocalizationManager.GetTermTranslation(enemy._name);
            descText.text = LocalizationManager.GetTermTranslation(enemy.description);
        }

        float timeRemaining = (float)(Player.instance.lastAttackTime - GameManager.instance.ServerTime).TotalSeconds;
        
        if (timeRemaining > 0)
        {
            TimeSpan ts = TimeSpan.FromSeconds(timeRemaining);

            remainingTimeText.text = ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;
            doJobButton.gameObject.SetActive(false);
            remainingTimeText.gameObject.SetActive(true);
        }
        else
        {
            remainingTimeText.gameObject.SetActive(false);
            doJobButton.gameObject.SetActive(true);
        }
    }

    public void CommitACrime()
    {
        float winRate = Player.instance.CommitCrimeSuccessChance(ScriptableEnemy.dict[enemy.name.GetStableHashCode()]);        

        string winRateText = "";

        if (winRate >= 80)
        {
            winRateText = "tooEasy";
        }
        else if (winRate >= 60)
        {
            winRateText = "easy";
        }
        else if (winRate >= 50)
        {
            winRateText = "balanced";
        }
        else if (winRate >= 20)
        {
            winRateText = "hard";
        }
        else if (winRate >= 0)
        {
            winRateText = "tooHard";
        }

        CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("enemyInfo"),
                                    LocalizationManager.GetTermTranslation(winRateText),
                                    LocalizationManager.GetTermTranslation("ATTACK"), () =>
                                    {
                                        if (Player.instance != null && MafiasCrimeWorldClient.instance != null)
                                        {
                                            MafiasCrimeWorldClient.instance.CommitCrimeRequest(enemy.name.GetStableHashCode());
                                        }
                                    },
                                    LocalizationManager.GetTermTranslation("GiveUp"), () => CGUIPopUp.instance.Hide());        
    }
}
