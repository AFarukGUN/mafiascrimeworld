﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using I2.Loc;

public class UIMailSlot : MonoBehaviour {

    public Mail mail;

    public Text textSender;
    public Text textSubject;

    public void Update()
    {
        if (mail == null)
            return;

        textSender.text = UIMail.instance.LocalizeSubstrings(mail.sender);
        textSubject.text = UIMail.instance.LocalizeSubstrings(mail.subject);
    }

    public void Click()
    {
        UIMail.instance.readingMail = mail;
    }
}
