﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBuyRealEstate : MonoBehaviour {

    public ScriptableRealEstate realEstate;

    public Image image;

    public Text nameText;
    public Text descText;
    public Text ownedText;

    public Text moneyText;

    public Text incomeTimeText;
    public Button getIncomeButton;


    private void Awake()
    {
        if (realEstate != null)
        {
            image.sprite = realEstate.image;
        }
    }

    public void Update()
    {
        if (Player.instance == null)
            return;

        if (realEstate != null)
        {
            nameText.text = LocalizationManager.GetTermTranslation(realEstate._name);
            descText.text = LocalizationManager.GetTermTranslation(realEstate.description);
        }

        float vipBonus = Player.instance.vip.hash != 0 ? Player.instance.vip.data.incomeIncreaser : 0;

        moneyText.text = realEstate.buyPrice + "/" + Mathf.FloorToInt(realEstate.income * (1 + vipBonus / 100)).ToString();

        int realEstatesIndex = Player.instance.GetRealEstateIndexByName(realEstate.name);

        if (realEstatesIndex != -1)
        {
            ownedText.text = LocalizationManager.GetTermTranslation("owned").Replace("{owned}", Player.instance.realEstates[realEstatesIndex].count.ToString()); 
            
            if (Player.instance.realEstates[realEstatesIndex].GetIncomeTimeRemaining() > 0)
            {
                float timeRemaining = Player.instance.realEstates[realEstatesIndex].GetIncomeTimeRemaining();

                TimeSpan ts = TimeSpan.FromSeconds(timeRemaining);

                incomeTimeText.text = ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;

                getIncomeButton.gameObject.SetActive(false);
                incomeTimeText.gameObject.SetActive(true);
            }
            else
            {
                getIncomeButton.gameObject.SetActive(true);
                incomeTimeText.gameObject.SetActive(false);
            }

        }
        else
        {
            ownedText.text = LocalizationManager.GetTermTranslation("notOwned");
            getIncomeButton.gameObject.SetActive(false);
            incomeTimeText.gameObject.SetActive(false);
        }

    }

    public void BuyRealEstate()
    {
        if(Player.instance != null && MafiasCrimeWorldClient.instance != null)
        {
            MafiasCrimeWorldClient.instance.BuyRealEstateRequest(realEstate.name.GetStableHashCode());
        }
    }
    public void GetRealEstateIncome()
    {
        if (Player.instance != null && MafiasCrimeWorldClient.instance != null)
        {
            MafiasCrimeWorldClient.instance.GetIncomeRealEstateRequest(realEstate.name.GetStableHashCode());
        }
    }
}
