﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAnnouncement : MonoBehaviour {

    private int lastAnnouncementNo = -1;
    public AnnouncementDatas announcementData;

    public int announcementNo = -1;

    public Text header;
    public Text text;

    public Button detailsButton;

    public Image image;
    
    public void Update()
    {
        if (Player.instance == null)
            return;

        if(announcementNo != lastAnnouncementNo)
        {
            announcementData = Player.instance.announcements[announcementNo];

            header.text = announcementData.header;
            text.text = announcementData.text;

            detailsButton.onClick.AddListener(() => Application.OpenURL(announcementData.clickLink));

            StartCoroutine(LoadImageUrl(announcementData.imageLink));

            lastAnnouncementNo = announcementNo;
        }
    }
    IEnumerator LoadImageUrl(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        image.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
    }
}
