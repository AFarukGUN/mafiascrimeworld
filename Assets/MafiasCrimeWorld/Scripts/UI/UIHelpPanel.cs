﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHelpPanel : MonoBehaviour {

    public GameObject helpPanel;
    
    public void OpenPanel(bool open)
    {
        helpPanel.SetActive(open);
    }
}
