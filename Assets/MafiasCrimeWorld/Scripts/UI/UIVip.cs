﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVip : MonoBehaviour {

    public ScriptableVIP vip;
    public Image image;

    public Text nameText;
    public Text descText;

    public Text textVipDay;
    public Text textInventorySize;
    public Text textAttackTimeDecreaser;
    public Text textArenaAttackTimeDecreaser;
    public Text textStealMoneyAttackTimeDecreaser;
    public Text incomeIncreaser;
    public Text jailTimeDecreaser;
    public Text maxMoneyBankIncreaser;

    public Button buyVIPButton;

    public void Awake()
    {
        buyVIPButton.gameObject.SetActive(false);
    }

    public void Update()
    {
        if (Player.instance == null)
            return;

        if (vip != null)
        {
            image.sprite = vip.image;

            nameText.text = LocalizationManager.GetTermTranslation(vip._name);
            descText.text = LocalizationManager.GetTermTranslation(vip.description);

            textVipDay.text =  vip.vipDay.ToString();
            textInventorySize.text = "+" + vip.inventorySizeBonus.ToString();
            textAttackTimeDecreaser.text = "-%" + vip.attackTimeDecreaser.ToString();
            textArenaAttackTimeDecreaser.text = "-%" + vip.arenaAttackTimeDecreaser.ToString();
            textStealMoneyAttackTimeDecreaser.text = "-%" + vip.stealMoneyAttackTimeDecreaser.ToString();
            incomeIncreaser.text = "+%" + vip.incomeIncreaser.ToString();
            jailTimeDecreaser.text = "-%" + vip.jailTimeDecreaser.ToString();
            maxMoneyBankIncreaser.text = "+%" + vip.maxMoneyBankIncreaser.ToString();

            buyVIPButton.gameObject.SetActive(Player.instance.vip.hash == 0 || Player.instance.vip.data.vipLvl < vip.vipLvl);
        }
    }

    public void BuyVIP()
    {
        if (Player.instance != null && MafiasCrimeWorldClient.instance != null && vip != null)
        {
#if UNITY_EDITOR
            PaymentService.instance.BuyProductID(vip.androidSKU);
#elif UNITY_ANDROID
            PaymentService.instance.BuyProductID(vip.androidSKU);
#elif UNITY_IOS
            PaymentService.instance.BuyProductID(vip.iosSKU);
#else
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                     LocalizationManager.GetTermTranslation("unexpectedPlatform"),
                                     "OK", () => { CGUIPopUp.instance.Hide(); });
#endif
        }
    }
}
