﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStealMoneySlot : MonoBehaviour {
    
    public Text nameText;

    public Button attackButton;

    // Update is called once per frame
    void Update()
    {        
        attackButton.gameObject.SetActive(nameText.text != Player.instance.charName);
    }

    public void Click()
    {
        MafiasCrimeWorldClient.instance.StealMoneyRequest(nameText.text);
    }
}
