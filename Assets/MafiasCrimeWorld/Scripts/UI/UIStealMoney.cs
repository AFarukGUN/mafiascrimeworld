﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStealMoney : MonoBehaviour
{

    public static UIStealMoney instance;

    public UIStealMoneySlot slot;
    public Transform content;

    public Text stealMoneyTimer;

    public InputField ifPlayerName;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.instance == null)
            return;

        Player player = Player.instance;


        float timeRemaining = (float)(player.lastStealMoneyTime - GameManager.instance.ServerTime).TotalSeconds;

        if (timeRemaining > 0)
        {
            TimeSpan ts = TimeSpan.FromSeconds(timeRemaining);

            stealMoneyTimer.text = ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;
            stealMoneyTimer.gameObject.SetActive(true);
        }
        else
        {
            stealMoneyTimer.gameObject.SetActive(false);
        }

        List<string> stealMoneyPlayerList = player.stealMoneyPlayerList;

        UIUtils.BalancePrefabs(slot.gameObject, stealMoneyPlayerList.Count, content);

        for (int i = 0; i < stealMoneyPlayerList.Count; ++i)
        {
            UIStealMoneySlot slot = content.GetChild(i).GetComponent<UIStealMoneySlot>();
            slot.nameText.text = stealMoneyPlayerList[i];
        }
    }

    public void GetStealMoneyPlayersList()
    {
        MafiasCrimeWorldClient.instance.GetStealMoneyPlayersList();
    }

    public void StealMoneyRequest()
    {
        if(!String.IsNullOrEmpty(ifPlayerName.text))
        {
            MafiasCrimeWorldClient.instance.StealMoneyRequest(ifPlayerName.text);
        }
    }
}
