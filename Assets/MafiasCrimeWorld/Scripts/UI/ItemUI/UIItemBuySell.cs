﻿using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemBuySell : MonoBehaviour {
    
    public static UIItemBuySell instance;
    public bool bInventory;
    public PlayerEquipmentTypes eqType;
    public int no;

    public Image image;
    public Text itemName;
    public Text decsription;

    public Text attack;
    public Text defense;

    public Text bagSize;

    public Text strengthReq;
    public Text intelligenceReq;

    public Text buySellPriceText;

    public Text buySellText;
    public Button buySellButton;

    public GameObject panel;

    private void Awake()
    {
        instance = this;
    }


    public void SetItem(bool _bInventory, PlayerEquipmentTypes _eqType, int _no)
    {
        bInventory = _bInventory;
        eqType = _eqType;
        no = _no;
    }

    public void Click()
    {
        List<ScriptableItem> shopItems = new List<ScriptableItem>();

        string shopListName = eqType.ToString() + "ShopList";

        if (ScriptableShopList.dict.ContainsKey(shopListName.GetStableHashCode()))
        {
            shopItems = ScriptableShopList.dict[shopListName.GetStableHashCode()].items;
        }

        if (bInventory && Player.instance.inventory.Count > no && no != -1)
        {
            ScriptableItemAndAmount scriptableItemAndAmount = Player.instance.inventory[no];
            ScriptableItem item = scriptableItemAndAmount.amount > 0 ? scriptableItemAndAmount.item : null;

            if (item != null)
            {
                image.sprite = item.image;
                itemName.text = item.name;
                decsription.text = item.description;

                MafiasCrimeWorldClient.instance.SellShopItemRequest(no, 1);
            }
        }
        else if (!bInventory && shopItems.Count > no && no != -1)
        {
            ScriptableItem item = shopItems[no];

            if (item.hash != 0)
            {
                image.sprite = item.image;
                itemName.text = item.name;
                decsription.text = item.description;

                if(item is EquipmentItem)
                {
                    MafiasCrimeWorldClient.instance.BuyShopItemRequest((item as EquipmentItem).category, no, 1);
                }
            }
        }
    }

    // Update is called once per frame
    void Update () {
        if (Player.instance == null)
            return;

        List<ScriptableItem> shopItems = new List<ScriptableItem>();

        string shopListName = eqType.ToString() + "ShopList";

        if (ScriptableShopList.dict.ContainsKey(shopListName.GetStableHashCode()))
        {
            shopItems = ScriptableShopList.dict[shopListName.GetStableHashCode()].items;
        }

        if (bInventory && Player.instance.inventory.Count > no && no != -1)
        {
            ScriptableItemAndAmount scriptableItemAndAmount = Player.instance.inventory[no];
            ScriptableItem item = scriptableItemAndAmount.amount > 0 ? scriptableItemAndAmount.item : null;

            if (item != null)
            {
                image.sprite = item.image;
                itemName.text = item.name;
                decsription.text = item.description;

                if (item is EquipmentItem)
                {
                    attack.text = (item as EquipmentItem).attackBonus.ToString();
                    defense.text = (item as EquipmentItem).defenseBonus.ToString();

                    bagSize.text = (item as EquipmentItem).inventorySizeBonus.ToString();
                    buySellPriceText.text = (item as EquipmentItem).sellPrice.ToString();

                    strengthReq.text = (item as EquipmentItem).strengthRequirement.ToString();
                    intelligenceReq.text = (item as EquipmentItem).intelligenceRequirement.ToString();

                    attack.gameObject.SetActive(true);
                    defense.gameObject.SetActive(true);

                    bagSize.gameObject.SetActive(true);
                    buySellPriceText.gameObject.SetActive(true);

                    strengthReq.gameObject.SetActive(true);
                    intelligenceReq.gameObject.SetActive(true);

                    buySellText.text = LocalizationManager.GetTermTranslation("SELL");

                    panel.SetActive(true);
                }
                else
                {
                    attack.gameObject.SetActive(false);
                    defense.gameObject.SetActive(false);

                    bagSize.gameObject.SetActive(false);
                    buySellPriceText.gameObject.SetActive(false);

                    strengthReq.gameObject.SetActive(false);
                    intelligenceReq.gameObject.SetActive(false);

                    panel.SetActive(false);
                }

            }
            else
            {
                panel.SetActive(false);
            }
        }
        else if (!bInventory && shopItems.Count > no && no != -1)
        {
            ScriptableItem item = shopItems[no];

            if (item.hash != 0)
            {
                image.sprite = item.image;
                itemName.text = item.name;
                decsription.text = item.description;

                if (item is EquipmentItem)
                {
                    attack.text = (item as EquipmentItem).attackBonus.ToString();
                    defense.text = (item as EquipmentItem).defenseBonus.ToString();

                    bagSize.text = (item as EquipmentItem).inventorySizeBonus.ToString();
                    buySellPriceText.text = (item as EquipmentItem).buyPrice.ToString();

                    strengthReq.text = (item as EquipmentItem).strengthRequirement.ToString();
                    intelligenceReq.text = (item as EquipmentItem).intelligenceRequirement.ToString();

                    attack.gameObject.SetActive(true);
                    defense.gameObject.SetActive(true);

                    bagSize.gameObject.SetActive(true);
                    buySellPriceText.gameObject.SetActive(true);

                    strengthReq.gameObject.SetActive(true);
                    intelligenceReq.gameObject.SetActive(true);

                    buySellText.text = LocalizationManager.GetTermTranslation("BUY");

                    panel.SetActive(true);
                }
                else
                {
                    attack.gameObject.SetActive(false);
                    defense.gameObject.SetActive(false);

                    bagSize.gameObject.SetActive(false);
                    buySellPriceText.gameObject.SetActive(false);

                    strengthReq.gameObject.SetActive(false);
                    intelligenceReq.gameObject.SetActive(false);

                    panel.SetActive(false);
                }
            }
            else
            {
                panel.SetActive(false);
            }
        }
        else
        {
            panel.SetActive(false);
        }
    }
}
