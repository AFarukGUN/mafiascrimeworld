﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShopSlot : MonoBehaviour {

    public PlayerEquipmentTypes eqType;
    public int no;

    public Image image;
    public Text textName;

    public ScriptableItem scriptableItem;

    // Update is called once per frame
    void Update()
    {
        if (scriptableItem.hash != 0)
        {
            image.sprite = scriptableItem.image;
            textName.text = scriptableItem.name;

            if(scriptableItem is EquipmentItem && (scriptableItem as EquipmentItem).category == PlayerEquipmentTypes.Weapon )
            {

            }

            image.gameObject.SetActive(true);
        }
        else
        {
            image.gameObject.SetActive(false);
        }
    }

    public void Click()
    {
        UIItemBuySell.instance.SetItem(false, eqType, no);
    }
}
