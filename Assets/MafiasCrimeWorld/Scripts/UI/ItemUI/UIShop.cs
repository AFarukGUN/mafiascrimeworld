﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShop : MonoBehaviour {

    public static UIShop instance;

    public UIShopSlot slot;
    public PlayerEquipmentTypes eqType = PlayerEquipmentTypes.Weapon;
    public Transform content;

    public void OpenShop(PlayerEquipmentTypes _eqType)
    {
        eqType = _eqType;
        UIShopCategoryButton.curEqType = _eqType;
    }

    private void Awake()
    {
        instance = this;
        OpenShop(PlayerEquipmentTypes.Weapon);
    }
    // Update is called once per frame
    void Update() {
        /*if (Player.instance == null)
            return;

        Player player = Player.instance;*/

        List<ScriptableItem> shopItems = new List<ScriptableItem>();

        string shopListName = eqType.ToString() + "ShopList";
        
        if(ScriptableShopList.dict.ContainsKey(shopListName.GetStableHashCode()))
        {
            shopItems = ScriptableShopList.dict[shopListName.GetStableHashCode()].items;
        }

        UIUtils.BalancePrefabs(slot.gameObject, shopItems.Count, content);

        for (int i = 0; i < shopItems.Count; ++i)
        {
            UIShopSlot slot = content.GetChild(i).GetComponent<UIShopSlot>();
            slot.eqType = eqType;
            slot.no = i;
            slot.scriptableItem = shopItems[i];
        }
    }
}
