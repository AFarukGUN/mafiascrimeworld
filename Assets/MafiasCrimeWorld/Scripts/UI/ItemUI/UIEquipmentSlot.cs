﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEquipmentSlot : MonoBehaviour {

    public int no;

    public Image image;
    public Image backgroundImage;

    public float doubleClickTime = 0.5f;
    public float clickCount = 0;
    public float lastClickTime = 0;

    // Update is called once per frame
    void Update()
    {
        if (Player.instance == null)
            return;

        if (Player.instance.inventory.Count > no && no != -1)
        {
            Item item = Player.instance.equipments[no];

            if (item.hash != 0)
            {
                image.sprite = item.image;

                backgroundImage.gameObject.SetActive(false);
                image.gameObject.SetActive(true);
            }
            else
            {
                image.gameObject.SetActive(false);
                backgroundImage.gameObject.SetActive(true);
            }
        }
    }

    public void Click()
    {
        if (Time.time - lastClickTime > doubleClickTime)
        {
            clickCount = 1;
            lastClickTime = Time.time;
        }
        else
        {
            clickCount += 1;
        }

        if (clickCount > 1)
        {
            Item item = Player.instance.equipments[no];

            if (item.hash != 0)
            {
                image.sprite = item.image;

                if (item.data is EquipmentItem)
                {
                    int invNo = Player.instance.GetFirstEmptyInventorySlot();
                    if (invNo != -1)
                    {
                        MafiasCrimeWorldClient.instance.SwapItemRequest(invNo, no);
                    }
                }
            }
        }
        else
        {
            UIItemEquipUnequip.instance.SetItem(false, no);
        }
           
    }
}
