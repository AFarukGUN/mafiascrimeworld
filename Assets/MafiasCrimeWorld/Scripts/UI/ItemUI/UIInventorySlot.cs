﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventorySlot : MonoBehaviour {

    public int no;

    public Image image;
    public Text countText;

    public float doubleClickTime = 0.5f;
    public float clickCount = 0;
    public float lastClickTime = 0;

    // Update is called once per frame
    void Update()
    {
        if (Player.instance == null)
            return;

        if (Player.instance.inventory.Count > no && no != -1)
        {
            ScriptableItemAndAmount scriptableItemAndAmount = Player.instance.inventory[no];
            ScriptableItem item = scriptableItemAndAmount.amount > 0 ? scriptableItemAndAmount.item : null;

            if (item != null)
            {
                image.sprite = item.image;
                countText.text = scriptableItemAndAmount.amount.ToString();

                image.gameObject.SetActive(true);
                countText.gameObject.SetActive(true);
            }
            else
            {
                image.gameObject.SetActive(false);
                countText.gameObject.SetActive(false);
            }
        }
    }

    public void Click()
    {
        if(Time.time - lastClickTime > doubleClickTime)
        {
            clickCount = 1;
            lastClickTime = Time.time;
        }
        else
        {
            clickCount += 1;
        }       

        if(clickCount > 1)
        {
            if (Player.instance.inventory.Count > no && no != -1)
            {
                ScriptableItemAndAmount scriptableItemAndAmount = Player.instance.inventory[no];
                ScriptableItem item = scriptableItemAndAmount.amount > 0 ? scriptableItemAndAmount.item : null;

                if (item != null)
                {
                    if (item is EquipmentItem)
                    {
                        int eqNo = Player.instance.GetEquipableIndex(item);
                        if (eqNo != -1)
                        {
                            MafiasCrimeWorldClient.instance.SwapItemRequest(no, eqNo);
                        }
                    }
                }
            }
        }
        else
        {
            if (UIItemEquipUnequip.instance != null)
            {
                UIItemEquipUnequip.instance.SetItem(true, no);
            }

            if (Player.instance.inventory.Count > no && no >= 0)
            {
                ScriptableItemAndAmount scriptableItemAndAmount = Player.instance.inventory[no];
                ScriptableItem item = scriptableItemAndAmount.amount > 0 ? scriptableItemAndAmount.item : null;

                if (item != null && item is EquipmentItem && UIItemBuySell.instance != null)
                {
                    if (UIItemBuySell.instance != null)
                    {
                        UIItemBuySell.instance.SetItem(true, (item as EquipmentItem).category, no);
                    }
                }
            }
        }
        
    }
}
