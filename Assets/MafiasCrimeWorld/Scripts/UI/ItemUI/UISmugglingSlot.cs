﻿using I2.Loc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISmugglingSlot : MonoBehaviour {

    public int index;

    public Image image;
    public InputField count;

    public int price;
    public Text nameText;
    public Text priceText;

    public ScriptableItem scriptableItem;

    // Update is called once per frame
    void Update()
    {
        if (scriptableItem.hash != 0)
        {
            image.sprite = scriptableItem.image;
            image.gameObject.SetActive(true);

            nameText.text = LocalizationManager.GetTermTranslation(scriptableItem.name.ToString());
            nameText.gameObject.SetActive(true);

            priceText.text = price.ToString();
            priceText.gameObject.SetActive(true);
        }
        else
        {
            image.gameObject.SetActive(false);
            priceText.gameObject.SetActive(false);
            nameText.gameObject.SetActive(false);
        }
    }

    public void ClickSell()
    {
        MafiasCrimeWorldClient.instance.SellSmugglingItemRequest(index, Convert.ToInt32(count.text) > 0 ? Convert.ToInt32(count.text) : 1);
    }
    public void ClickBuy()
    {
        MafiasCrimeWorldClient.instance.BuySmugglingItemRequest(index, Convert.ToInt32(count.text) > 0 ? Convert.ToInt32(count.text) : 1);
    }
}
