﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShopCategoryButton : MonoBehaviour
{
    public static PlayerEquipmentTypes curEqType;

    public PlayerEquipmentTypes eqType;

    public Image clickImage;

    // Update is called once per frame
    void Update()
    {
        if(eqType == curEqType)
        {
            clickImage.gameObject.SetActive(true);
        }
        else
        {
            clickImage.gameObject.SetActive(false);
        }
    }

    public void Click()
    {
        UIShop.instance.OpenShop(eqType);
    }
}
