﻿using I2.Loc;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class UIMail : MonoBehaviour {

    public static UIMail instance;

    public Text textSender;
    public Text textSubject;
    public Text textMessage;

    public Mail readingMail;

    public GameObject mailReadPanel;

    public UIMailSlot slot;
    public Transform content;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (readingMail.id != -1)
        {
            textSender.text = LocalizeSubstrings(readingMail.sender);
            textSubject.text = LocalizeSubstrings(readingMail.subject);
            textMessage.text = LocalizeSubstrings(readingMail.message);

            mailReadPanel.SetActive(true);
        }
        else
        {
            textSender.text = "";
            textSubject.text = "";
            textSubject.text = "";

            mailReadPanel.SetActive(false);
        }



        if (Player.instance == null)
            return;

        Player player = Player.instance; ;

        List<Mail> mails = player.mails;

        UIUtils.BalancePrefabs(slot.gameObject, mails.Count, content);

        for (int i = 0; i < mails.Count; ++i)
        {
            UIMailSlot slot = content.GetChild(mails.Count - 1 - i).GetComponent<UIMailSlot>();
            slot.mail = mails[i];
        }
    } 

    public string LocalizeSubstrings(string _string)
    {
        List<string> returnString = new List<string>();

        List<string> subStrings = Regex.Split(_string, "{").ToList();

        for(int i = 0; i < subStrings.Count; i++)
        {
            List<string> subStrings2 = Regex.Split(subStrings[i], "}").ToList();
            returnString.Add("{" + subStrings2[0] + "}");
        }

        for(int i = 0; i < returnString.Count; i++)
        {
            _string = _string.Replace(returnString[i], LocalizationManager.GetTermTranslation(returnString[i]));
        }

        return _string;
    }
}
