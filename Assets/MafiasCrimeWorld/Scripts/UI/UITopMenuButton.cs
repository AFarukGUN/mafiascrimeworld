﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITopMenuButton : MonoBehaviour
{
    private Sprite startSprite;

    public void Awake()
    {
        startSprite = GetComponent<Button>().image.sprite;        
    }

    public void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            if (UIManager.lastClickedTopMenuButton != null)
            {
                UIManager.lastClickedTopMenuButton.RefreshSprite();
            }
            UIManager.lastClickedTopMenuButton = this;
            GetComponent<Button>().image.sprite = UIManager.instance.topButtonActiveSprite;
        }
           );
    }

    public void RefreshSprite()
    {
        GetComponent<Button>().image.sprite = startSprite;
    }
}
