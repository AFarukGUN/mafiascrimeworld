﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/Management", order = 999)]
public class ScriptableManagement : ScriptableObject
{
    public Sprite image;
    
    public string _name;
    public string description;

    public int buyPrice;
    public int income;

    static Dictionary<int, ScriptableManagement> cache;
    public static Dictionary<int, ScriptableManagement> dict
    {
        get
        {
            // not loaded yet?
            if (cache == null)
            {
                // get all ScriptableSkills in resources
                ScriptableManagement[] managements = Resources.LoadAll<ScriptableManagement>("");

                // check for duplicates, then add to cache
                List<string> duplicates = managements.ToList().FindDuplicates(management => management.name);
                if (duplicates.Count == 0)
                {
                    cache = managements.ToDictionary(management => management.name.GetStableHashCode(), management => management);
                }
                else
                {
                    foreach (string duplicate in duplicates)
                        Debug.LogError("Resources folder contains multiple ScriptableManagement with the name " + duplicate + ".");
                }
            }
            return cache;
        }
    }
}
