﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/Enemy", order = 999)]
public class ScriptableEnemy : ScriptableObject
{
    public bool doWithClan = false;

    public int rank = -1;

    public Sprite image;

    public string _name;
    public string description;

    public int healthRequirement;

    public int attack;
    public int defense;

    public float strength;
    public float intelligence;

    public int moneyMin;
    public int moneyMax;

    public int crimePointMin;
    public int crimePointMax;

    public float attackPeriod = 60;

    public float jailRate;
    public float jailSeconds;

    static Dictionary<int, ScriptableEnemy> cache;
    public static Dictionary<int, ScriptableEnemy> dict
    {
        get
        {
            // not loaded yet?
            if (cache == null)
            {
                // get all ScriptableSkills in resources
                ScriptableEnemy[] enemies = Resources.LoadAll<ScriptableEnemy>("");

                // check for duplicates, then add to cache
                List<string> duplicates = enemies.ToList().FindDuplicates(enemy => enemy.name);
                if (duplicates.Count == 0)
                {
                    cache = enemies.ToDictionary(enemy => enemy.name.GetStableHashCode(), enemy => enemy);
                }
                else
                {
                    foreach (string duplicate in duplicates)
                        Debug.LogError("Resources folder contains multiple ScriptableEnemy with the name " + duplicate + ".");
                }
            }
            return cache;
        }
    }
}
