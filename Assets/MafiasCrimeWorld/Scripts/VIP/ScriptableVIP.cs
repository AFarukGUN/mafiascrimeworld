﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Mafias's Crime World/VIP", order = 999)]
public class ScriptableVIP : ScriptableObject
{
    public Sprite image;

    public string _name;
    public string description;

    public int vipMonth = 0;
    public int vipDay = 0;
    public int vipHour = 0;
    public int vipMinute = 0;

    public int attackBonus; //%
    public int defenseBonus; //%

    public float strengthBonus; // not %
    public float intelligenceBonus; // not %

    public int inventorySizeBonus;
    
    public float attackTimeDecreaser; //%
    public float arenaAttackTimeDecreaser; //%
    public float stealMoneyAttackTimeDecreaser;//%
    public float incomeIncreaser; //%
    public float jailTimeDecreaser; //%
    public float maxMoneyBankIncreaser; //% if it is 25 player can put %75 of his money

    public List<ScriptableItemAndAmount> bonusItems;

    public Color chatMessageColor = Color.black;

    public int vipLvl;
    public string androidSKU;
    public string iosSKU;

    // (has corrected target already)
    public void Apply(Player player)
    {
        player.vip = new VIP(this, GameManager.instance.ServerTime + new TimeSpan(vipMonth * 30 + vipDay, vipHour, vipMonth, 0, 0));
    }

    static Dictionary<int, ScriptableVIP> cache;
    public static Dictionary<int, ScriptableVIP> dict
    {
        get
        {
            // not loaded yet?
            if (cache == null)
            {
                // get all ScriptableSkills in resources
                ScriptableVIP[] vips = Resources.LoadAll<ScriptableVIP>("");

                // check for duplicates, then add to cache
                List<string> duplicates = vips.ToList().FindDuplicates(vip => vip.name);
                if (duplicates.Count == 0)
                {
                    cache = vips.ToDictionary(vip => vip.name.GetStableHashCode(), vip => vip);
                }
                else
                {
                    foreach (string duplicate in duplicates)
                        Debug.LogError("Resources folder contains multiple ScriptableVIP with the name " + duplicate + ".");
                }
            }
            return cache;
        }
    }
}
