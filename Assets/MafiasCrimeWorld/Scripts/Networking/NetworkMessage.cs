﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;
using UnityEngine;

[System.Serializable]
public class NetworkMessage
{

    public NetMessageTypes netMessageType;
    public string message;


    public NetworkMessage(NetMessageTypes _netMessageType, string _message)
    {
        netMessageType = _netMessageType;
        message = _message;
    }

    public NetworkMessage()
    {

    }

    // Convert an object to a byte array
    public byte[] ObjectToByteArray()
    {
        if (this == null)
            return null;

        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, this);

        return ms.ToArray();
    }

    // Convert a byte array to an Object
    public void ByteArrayToObject(byte[] arrBytes)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(arrBytes, 0, arrBytes.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        NetworkMessage obj = (NetworkMessage)binForm.Deserialize(memStream);

        netMessageType = obj.netMessageType;
        message = obj.message;
    }
}


public enum NetMessageTypes
{    
    RegisterRequest = 0,
    RegisterSucceed = 1,
    RegisterFailed = 2,

    LoginRequest = 10,
    LoginSucceed = 11,
    LoginFailed = 12,

    SetCharnameRequest = 20,
    SetCharnameSucceed = 21,
    SetCharnameFailed = 22,

    SyncHealth = 30,
    SyncMoney = 31,

    CommitCrimeRequest = 40,
    CommitCrimeSucceed = 41,
    CommitCrimeFailed = 42,
    TryCommitCrimeFailed = 43,

    BuyRealEstateRequest = 50,
    BuyRealEstateSucceed = 51,
    BuyRealEstateFailed = 52,
    TryBuyRealEstateFailed = 53,

    GetIncomeRealEstateRequest = 60,
    GetIncomeRealEstateSucceed = 61,
    GetIncomeRealEstateFailed = 62,
    TryGetIncomeRealEstateFailed = 63,

    BuyManagementRequest = 70,
    BuyManagementSucceed = 71,
    BuyManagementFailed = 72,
    TryBuyManagementFailed = 73,

    GetIncomeManagementRequest = 80,
    GetIncomeManagementSucceed = 81,
    GetIncomeManagementFailed = 82,
    TryGetIncomeManagementFailed = 83,

    TrainRequest = 90,
    TrainSucceed = 91,
    TrainFailed = 92,
    TryTrainFailed = 93,

    GiveABribeRequest = 100,
    GiveABribeSucceed = 101,
    GiveABribeFailed = 102,
    TryGiveABribeFailed = 103,

    EscapeFromPrisonRequest = 105,
    EscapeFromPrisonSucceed = 106,
    EscapeFromPrisonFailed = 107,
    TryEscapeFromPrisonFailed = 108,

    MoneyBankTransferRequest = 110,
    MoneyBankTransferSucceed = 111,
    MoneyBankTransferFailed = 112,
    TryMoneyBankTransferFailed = 113,
    
    GetAnnouncementsSucceed = 120,
    GetAnnouncementsFailed = 121,

    DoStoryRequest = 130,
    DoStorySucceed = 131,
    DoStoryFailed = 132,
    TryDoStoryFailed = 133,

    SwapItemRequest = 140,
    SwapItemSucceed = 141,
    SwapItemFailed = 142,
    TrySwapItemFailed = 143,

    BuyShopItemRequest = 150,
    BuyShopItemSucceed = 151,
    BuyShopItemFailed = 152,
    TryBuyShopItemFailed = 153,

    SellShopItemRequest = 160,
    SellShopItemSucceed = 161,
    SellShopItemFailed = 162,
    TrySellShopItemFailed = 163,
    
    GetRankCrimePointsRequest = 170,
    GetRankCrimePointsSucceed = 171,
    GetRankCrimePointsFailed = 172,

    GetRankMoneyRequest = 180,
    GetRankMoneySucceed = 181,
    GetRankMoneyFailed = 182,

    GetRankTotalPvEKillRequest = 190,
    GetRankTotalPvEKillSucceed = 191,
    GetRankTotalPvEKillFailed = 192,

    GetRankTotalPvEDeathRequest = 200,
    GetRankTotalPvEDeathSucceed = 201,
    GetRankTotalPvEDeathFailed = 202,

    //Rank datas add

    GetSmugglingItemsRequest = 300,
    GetSmugglingItemsSucceed = 301,
    GetSmugglingItemsFailed = 302,

    SellSmugglingItemsRequest = 310,
    SellSmugglingItemsSucceed = 311,
    SellSmugglingItemsFailed = 312,
    TrySellSmugglingItemsFailed = 313,
    
    BuySmugglingItemsRequest = 320,
    BuySmugglingItemsSucceed = 321,
    BuySmugglingItemsFailed = 322,
    TryBuySmugglingItemsFailed = 323,


    ArenaAttackRequest = 330,
    ArenaAttackSucceed = 331,
    ArenaAttackFailed = 332,
    TryArenaAttackFailed = 333,

    GetArenaListRequest = 340,
    GetArenaListSucceed = 341,
    GetArenaListFailed = 342,


    SendChatMessage = 350,

    SetServerTime = 360,

    CreateGuildRequest = 370,
    LeaveGuildRequest = 371,
    TerminateGuildRequest = 372,
    SetGuildNoticeRequest = 373,
    KickFromGuildRequest = 374,
    InviteToGuildRequest = 375,
    ApplyToGuildRequest = 376,
    PromoteMemberRequest = 377,
    DemoteMemberRequest = 378,
    GetGuildNamesForApplyRequest = 379,

    BroadcastGuildData = 380,

    AcceptGuildRequest = 381,
    RejectGuildRequest = 382,

    SetInvitedGuilds = 383,

    AcceptGuildApplyRequest = 384,
    RejectGuildApplyRequest = 385,

    SendGuildNamesForApplyRequest = 386,

    SyncMails = 400,
    GetItemsFromMailRequest = 401,
    GetItemsFromMailSucceed = 402,
    GetItemsFromMailFailed = 403,
    
    GetStealMoneyPlayersList = 411,
    GetStealMoneyPlayersListSucceed = 412,
    GetStealMoneyPlayersListFailed = 413,
    StealMoneyRequest = 414,
    StealMoneySucceed = 415,
    StealMoneyFailed = 416,
    TryStealMoneyFailed = 417,

    InAppPurchaseRequest = 430,
    InAppPurchaseSucceed = 431,
    InAppPurchaseFailed = 432,

    GetFreeEnergyRequest = 440,
    GetFreeEnergySucceed = 441,
    GetFreeEnergyFailed = 442,

    GetFreeMoneyRequest = 450,
    GetFreeMoneySucceed = 451,
    GetFreeMoneyFailed = 452,
}

[System.Serializable]
public class PlayerMailPassword
{
    public int version;
    public string mail;
    public string mail2;
    public string password;
    public string password2;
}

[System.Serializable]
public class PlayerDatas
{
    public int id;
    public bool online;

    public string charName;

    public float strength;
    public float intelligence;

    public PlayerGrade playerGrade;
    public int crimePoints;
    public int money;

    public int totalPvEKill;
    public int totalPvEDeath;

    public int totalArenaKill;
    public int totalArenaDeath;

    public int vip;
    public string vipTimeEnd;

    public string inventory;
    public string equipments;

    public int curHealth;
    public string lastAttackTime;
    public string lastArenaAttackTime;

    public string buffs;
    public string jailFinishTime;
    public string realEstates;
    public string managements;

    public int moneyBank;
    public int lastSucceedStoryNo;

    public List<string> invitedGuilds;

    public string mails;

    public string lastStealMoneyTime;

    public string lastEnergyCollected;

    public void CopyPlayerDatas(Player player)
    {
        id = player.playerID;
        online = player.online;

        charName = player.charName;

        strength = player._strength;
        intelligence = player._intelligence;

        playerGrade = player.playerGrade;
        crimePoints = player.crimePoints;
        money = player.money;

        totalPvEKill = player.totalPvEKill;
        totalPvEDeath = player.totalPvEDeath;

        totalArenaKill = player.totalArenaKill;
        totalArenaDeath = player.totalArenaDeath;

        vip = player.vip.hash;
        vipTimeEnd = player.vip.hash != 0 ? (player.vip.vipTimeEnd - GameManager.instance.ServerTime).ToString() : "";

        inventory = player.inventory.Count == 0 ? "" : JsonConvert.SerializeObject(player.inventory);
        equipments = player.equipments.Count == 0 ? "" : JsonConvert.SerializeObject(player.equipments);

        curHealth = player.curHealth;
        lastAttackTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.lastAttackTime));
        lastArenaAttackTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.lastArenaAttackTime));

        buffs = player.buffs.Count == 0 ? "" : JsonConvert.SerializeObject(player.buffs);
        jailFinishTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.jailFinishTime));
        realEstates = player.realEstates.Count == 0 ? "" : JsonConvert.SerializeObject(player.realEstates);
        managements = player.managements.Count == 0 ? "" : JsonConvert.SerializeObject(player.managements);

        moneyBank = player.moneyBank;
        lastSucceedStoryNo = player.lastSucceedStoryNo;
        invitedGuilds = player.invitedGuilds;
        mails = player.mails.Count == 0 ? "" : JsonConvert.SerializeObject(player.mails);
        lastStealMoneyTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.lastStealMoneyTime));
        lastEnergyCollected = JsonConvert.SerializeObject(new SYSTEMTIME(player.lastEnergyCollected));
    }

    public void CopyPlayer(Player player)
    {
        player.playerID = id;
        player.online = online;

        player.charName = charName;

        player._strength = strength;
        player._intelligence = intelligence;

        player.playerGrade = playerGrade;


        player.SetCrimePointsMoney(crimePoints, money);

        /*player.money = money;
        player.crimePoints = crimePoints;*/

        player.CheckGrade();


        player.totalPvEKill = totalPvEKill;
        player.totalPvEDeath = totalPvEDeath;

        player.totalArenaKill = totalArenaKill;
        player.totalArenaDeath = totalArenaDeath;

        
       TimeSpan vipTs = vip == 0 ? new TimeSpan() : TimeSpan.Parse(vipTimeEnd);

        player.vip = vip == 0 ? new VIP() : new VIP(ScriptableVIP.dict[vip], GameManager.instance.ServerTime + vipTs);

        List<Item> tempEq = String.IsNullOrEmpty(equipments) ? new List<Item>() : JsonConvert.DeserializeObject<List<Item>>(equipments);

        if (tempEq.Count > 0)
        {
            for (int i = 0; i < tempEq.Count; i++)
            {
                if (tempEq[i].hash != 0)
                {
                    player.equipments[i] = new Item(ScriptableItem.dict[tempEq[i].data.hash]);
                }
            }
        }


        player.inventory.Clear();

        for(int i = 0; i < player.inventorySize; i++)
        {
            player.inventory.Add(new ScriptableItemAndAmount());
        }

        List<ScriptableItemAndAmount> tempInv = String.IsNullOrEmpty(inventory) ? new List<ScriptableItemAndAmount>() : JsonConvert.DeserializeObject<List<ScriptableItemAndAmount>>(inventory);

        if(tempInv.Count > 0)
        {         
            for (int i = 0; i< tempInv.Count; i++)
            {
                if (tempInv[i].amount > 0 && tempInv[i].item.hash != 0)
                {
                    player.inventory[i] = new ScriptableItemAndAmount(ScriptableItem.dict[tempInv[i].item.hash], tempInv[i].amount);
                }
            }
        }

        player.RefreshInventorySize();

        player.curHealth = curHealth;

        player.lastAttackTime = JsonConvert.DeserializeObject<SYSTEMTIME>(lastAttackTime).Get();
        player.lastArenaAttackTime = JsonConvert.DeserializeObject<SYSTEMTIME>(lastArenaAttackTime).Get();

        player.buffs = String.IsNullOrEmpty(buffs) ? player.buffs : JsonConvert.DeserializeObject<List<Buff>>(buffs);
        player.jailFinishTime = JsonConvert.DeserializeObject<SYSTEMTIME>(jailFinishTime).Get();
        player.realEstates = String.IsNullOrEmpty(realEstates) ? player.realEstates : JsonConvert.DeserializeObject<List<RealEstate>>(realEstates);
        player.managements = String.IsNullOrEmpty(managements) ? player.managements : JsonConvert.DeserializeObject<List<Management>>(managements);

        player.moneyBank = moneyBank;
        player.lastSucceedStoryNo = lastSucceedStoryNo;

        player.invitedGuilds = invitedGuilds;
        player.mails = String.IsNullOrEmpty(mails) ? player.mails : JsonConvert.DeserializeObject<List<Mail>>(mails);
        player.lastStealMoneyTime = JsonConvert.DeserializeObject<SYSTEMTIME>(lastStealMoneyTime).Get();
        player.lastEnergyCollected = JsonConvert.DeserializeObject<SYSTEMTIME>(lastEnergyCollected).Get();
    }
}

[System.Serializable]
public class HealthEnemyMoneyCrimePointsAttackTimeJailTime
{
    public int curHealth;
    public int enemyHash;
    public int curMoney;
    public int curCrimePoints;
    public string lastAttackTime;
    public string jailFinishTime;

    public void CopyPlayer(Player player)
    {
        player.curHealth = curHealth;
        /*player.money = curMoney;
        player.crimePoints = curCrimePoints;*/

        player.SetCrimePointsMoney(curCrimePoints, curMoney);

        player.CheckGrade();

        player.lastAttackTime = JsonConvert.DeserializeObject<SYSTEMTIME>(lastAttackTime).Get();
        player.SetJailFinishTime(JsonConvert.DeserializeObject<SYSTEMTIME>(jailFinishTime).Get());
    }
}

[System.Serializable]
public class EnemyHashHealthJail
{
    public int enemyHash;
    public int health;
    public string lastAttackTime;
    public string jailFinishTime;
}

[System.Serializable]
public class PlayerRealEstateMoney
{
    public int realEstateHash;
    public int money;
}

[System.Serializable]
public class PlayerManagementMoney
{
    public int managementHash;
    public int money;
}

[System.Serializable]
public class PlayerHealthMoneyStrengthIntelligence
{
    public int health;
    public int money;
    public float strength;
    public float intelligence;
}

[System.Serializable]
public class PlayerMoneyBankMoney
{
    public int money;
    public int bankMoney;
    public bool fromBankToInventory;
}

[System.Serializable]
public class AnnouncementDatas
{
    public int id;
    public string header;
    public string text;
    public string imageLink;
    public string clickLink;
}

[System.Serializable]
public class NewDatas
{
    public int id;
    public string header;
    public string text;
    public NewTypes newType;
}

public enum NewTypes
{
    
}

[System.Serializable]
public class HealthEnemyMoneyCrimePointsLastSucceedStoryNo
{
    public int curHealth;
    public int enemyHash;
    public int curMoney;
    public int curCrimePoints;
    public int lastSucceedStoryNo;

    public void CopyPlayer(Player player)
    {
        player.curHealth = curHealth;

        player.SetCrimePointsMoney(curCrimePoints, curMoney);
        /*player.money = curMoney;
        player.crimePoints = curCrimePoints;*/

        player.CheckGrade();

        player.lastSucceedStoryNo = lastSucceedStoryNo;
    }
}

[System.Serializable]
public class SwapItemDatas
{
    public int invNo;
    public int eqNo;
}
[System.Serializable]
public class ShopItemDatas
{
    public PlayerEquipmentTypes eqType;
    public int index;
    public int amount = 1;
}

[System.Serializable]
public class RankDatasCrimePoint
{
    public int no;
    public string charName;
    public int crimePoint;
}

[System.Serializable]
public class RankDatasMoney
{
    public int no;
    public string charName;
    public int money;
}

[System.Serializable]
public class RankDatasKillDeath
{
    public int no;
    public string charName;
    public int kill;
    public int death;
}

[System.Serializable]
public class GetRankDatas
{
    public bool myRank;
    public RankDataTypes rankDataType;
    public int pageNo;
    public int userPerPage;
}

public enum RankDataTypes
{
    crimepoints = 0,
    money = 1,
    totalPvEKill = 2,
    totalPvEDeath = 3,
}

[System.Serializable]
public class ScriptableItemAndPrice
{
    public int hash;
    public int price;
}

[System.Serializable]
public class ArenaAttackDatas
{
    public string enemyName;
    public int curHealth;
    public int curMoney;
    public int curCrimePoints;
    public string lastArenaAttackTime;

    public void CopyPlayer(Player player)
    {
        player.curHealth = curHealth;

        player.SetCrimePointsMoney(curCrimePoints, curMoney);

        /*player.money = curMoney;
        player.crimePoints = curCrimePoints;*/

        player.CheckGrade();

        /*
        TimeZoneInfo easternZone = TimeZoneInfo.Utc;
        
        player.lastArenaAttackTime = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(lastArenaAttackTime), easternZone);
        */
        player.lastArenaAttackTime = JsonConvert.DeserializeObject<SYSTEMTIME>(lastArenaAttackTime).Get();
    }
}

[System.Serializable]
public class ArenaListDatas
{
    public int no = -1;
    public string name;
    public int crimePoints;
}

[System.Serializable]
public class GuildDatas
{
    public int id;
    public string name;
    public string notice;
    public int crimePoints;
    public int money;
}

[Serializable]
public class ChatMessage
{
    public string sender;
    public string identifier;
    public string message;
    public string replyPrefix; // copied to input when clicking the message 
    public int vipHash;

    public ChatMessage()
    {

    }

    public ChatMessage(string sender, string identifier, string message, string replyPrefix, int vipHash)
    {
        this.sender = sender;
        this.identifier = identifier;
        this.message = message;
        this.replyPrefix = replyPrefix;
        this.vipHash = vipHash;
    }
}

[Serializable]
public class Mail
{
    public int id;
    public string sender;
    public string getter;
    public string subject;
    public string message;
    public List<ScriptableItemAndAmount> items;
    public bool deleted;

    public Mail()
    {
        id = -1;
    }

    public Mail(int id, string sender, string getter, string subject, string message, List<ScriptableItemAndAmount> items, bool deleted)
    {
        this.id = id;
        this.sender = sender;
        this.getter = getter;
        this.subject = subject;
        this.message = message;
        this.items = items;
        this.deleted = deleted;
    }
}

[Serializable]
public class MailInventory
{
    public List<Mail> mails;
    public List<ScriptableItemAndAmount> inventory;
}

[System.Serializable]
public class StealMoneyDatas
{
    public string enemyName;
    public int curMoney;
    public string lastStealMoneyTime;

    public void CopyPlayer(Player player)
    {
        player.SetMoney(curMoney);
        
        player.lastStealMoneyTime = JsonConvert.DeserializeObject<SYSTEMTIME>(lastStealMoneyTime).Get();
    }
}

[System.Serializable]
public class SYSTEMTIME
{
    public int year;
    public int month;
    public int day;
    public int hour;
    public int minute;
    public int second;
    public int milliseconds;

    public SYSTEMTIME(DateTime dt)
    {
        year = dt.Year;
        month = dt.Month;
        day = dt.Day;
        hour = dt.Hour;
        minute = dt.Minute;
        second = dt.Second;
        milliseconds = dt.Millisecond;
    }

    public void Set(DateTime dt)
    {
        year = dt.Year;
        month = dt.Month;
        day = dt.Day;
        hour = dt.Hour;
        minute = dt.Minute;
        second = dt.Second;
        milliseconds = dt.Millisecond;
    }

    public DateTime Get()
    {
        return new DateTime(year, month, day, hour, minute, second, milliseconds);
    }    
}

[System.Serializable]
public class InAppPurchaseDatas
{
    public string definitionId;
    public string receipt;
    public string transactionID;

    public DateTime purchaseTime;

    public int playerID;
    public string playerName;

    public bool status; //0, 1 //0 payback 1 valid
}