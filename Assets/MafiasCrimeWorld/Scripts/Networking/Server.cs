﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System;
using I2.Loc;
using Telepathy;
using System.Net;
using System.Net.Sockets;

public class Server : MonoBehaviour
{
    public int version = 1;
    public static Server instance;

    private Telepathy.Server _netServer;

    public Dictionary<int , Player> players;
    public Dictionary<string, Guild> guilds;

    [Header("MySQL")]
    public string serverMainName = "";
    public int serverPort = 3306;
    public string serverDatabase = "";
    public string serverUsername = "";
    public string serverPassword = "";
    string connectionString;

    public MySqlConnection connection;

    #region Server Statistics
    public int totalPvESucceed = 0;
    public int totalPvEFailed = 0;

    public int totalPvPSucceed = 0;
    public int totalPvPFailed = 0;

    public int jailedPeople = 0;

    public int totalRealEstateBuy = 0;
    public int totalRealEstateIncomeCount = 0;
    public int totalRealEstateIncomeMoney = 0;

    public int totalManagementBuy = 0;
    public int totalManagementIncomeCount = 0;
    public int totalManagementIncomeMoney = 0;

    public int totalTrained = 0;
    public int totalTrainingOutcome = 0;

    public int totalTrainingStrengthIncome = 0;
    public int totalTrainingIntelligenceIncome = 0;

    public int lastSucceedStoryNo = -1;

    #endregion

    public List<AnnouncementDatas> announcementDatas = new List<AnnouncementDatas>();
    
    public DateTime nextChangeTimeSmugglintItemsPrices;
    public List<SmugglingItemDatas> smugglingItemDatas = new List<SmugglingItemDatas>();
    public List<ScriptableItemAndPrice> smugglingItems = new List<ScriptableItemAndPrice>();

    private void Awake()
    {
        instance = this;

        connectionString = "Server=" + serverMainName
       + "; Port=" + serverPort + "; Database=" + serverDatabase + "; Uid="
       + serverUsername + "; Pwd=" + serverPassword + ";";

        connection = new MySqlConnection(connectionString);
               
        players = new Dictionary<int, Player>();
        guilds = new Dictionary<string, Guild>();

        RefreshSmugglingItemsPrices(true);
    }
    // Use this for initialization
    void Start()
    {
        if (connection.State == System.Data.ConnectionState.Closed)
        {
            try
            {   
                connection.ConnectionString = connectionString;
                connection.Open();

                StartServer();

                LoadAllGuilds();
            }
            catch (System.Exception)
            {
                Debug.LogWarning("Can't Connect");
                throw;
            }
        }

        announcementDatas = GetAnnouncements();

        InvokeRepeating("GetAnnouncements", 300, 300);

        InvokeRepeating("RefreshSmugglingItemsPricesInvoke", 
            UnityEngine.Random.Range(GameManager.instance.smugglingItemChangeMinMinute * 60, GameManager.instance.smugglingItemChangeMaxMinute * 60),
            UnityEngine.Random.Range(GameManager.instance.smugglingItemChangeMinMinute * 60, GameManager.instance.smugglingItemChangeMaxMinute * 60));        
    }

    public void StartServer()
    {
        try
        {
            _netServer = new Telepathy.Server();

            _netServer.Start(50900);

            Debug.LogError("Server Started...");
        }
        catch
        {
            Debug.LogError("Server Cant Started...");
        }
    }

    public void OnApplicationQuit()
    {
        for (int i = 0; i < players.Count; i++)
            UserSave(players.Values.ToList()[i]);

        connection.Close();

        if(_netServer != null)
            _netServer.Stop();
    }

    void Update()
    {
        if (_netServer != null && _netServer.Active)
        {
            // show all new messages
            Telepathy.Message msg;
            while (_netServer.GetNextMessage(out msg))
            {
                switch (msg.eventType)
                {
                    case Telepathy.EventType.Connected:
                        Debug.Log(msg.connectionId + " Connected");
                        SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.SetServerTime, JsonConvert.SerializeObject(new SYSTEMTIME(GameManager.instance.ServerTime))));
                        break;
                    case Telepathy.EventType.Data:
                        Debug.Log(msg.connectionId + " Data: " + BitConverter.ToString(msg.data));

                        OnNetworkReceive(msg);
                            
                        break;
                    case Telepathy.EventType.Disconnected:

                        Debug.Log(msg.connectionId + " Disconnected");
                        if (players.ContainsKey(msg.connectionId))
                        {
                            Player player = players[msg.connectionId];

                            if (player != null)
                            {
                                string _name = player.charName;

                                UserSave(player);

                                player.online = false;

                                // save last logout time and return true
                                ExecuteNonQuery("UPDATE users SET lastLogout=@lastLogout WHERE id=@id", new MySqlParameter("@id", player.playerID), new MySqlParameter("@lastLogout", GameManager.instance.ServerTime.ToString()));

                                if (!String.IsNullOrEmpty(player.guild.name))
                                {
                                    SetGuildOnline(player, false);
                                }

                                players.Remove(msg.connectionId);

                                Destroy(player.gameObject);
                            }
                        }

                        break;
                }
            }
        }
    }

    void OnDestroy()
    {
        if (_netServer != null)
            _netServer.Stop();
    }

    public void OnNetworkReceive(Message msg)
    {
        var netMessage = new NetworkMessage();
        netMessage.ByteArrayToObject(msg.data);

        Debug.LogError(netMessage.netMessageType);

        switch (netMessage.netMessageType)
        {
            case NetMessageTypes.RegisterRequest:
                {
                    PlayerMailPassword pmp = JsonConvert.DeserializeObject<PlayerMailPassword>(netMessage.message);

                    int _version = pmp.version;
                    string mail = pmp.mail;
                    string mail2 = pmp.mail2;
                    string password = pmp.password;
                    string password2 = pmp.password2;

                    if (version > _version)
                    {
                        
                        SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.LoginFailed, "Versionsarenotsame"));

                        return;
                    }

                    //check mails are same?
                    //check passwords are same?
                    //check mail correct?
                    //check register before with mail?
                    // mail registered before?

                    //var result = ExecuteScalar("SELECT id FROM users WHERE mail=@mail AND password=@password AND banned=0", new MySqlParameter("@mail", mail), new MySqlParameter("@password", GameUtils.MD5Hash(password)));

                    var result = ExecuteScalar("SELECT id FROM users WHERE mail=@mail", new MySqlParameter("@mail", mail));

                    if (result != null)
                    {
                        //mail already used
                        
                        SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.RegisterFailed, "Thismailalreadyused"));                      
                            
                    }
                    else
                    {
                        if (mail == mail2 && !String.IsNullOrEmpty(mail))
                        {
                            if (password == password2 && !String.IsNullOrEmpty(password))
                            {
                                if (password.Length >= GameManager.instance.minPasswordLenght)
                                {
                                    if (GameUtils.IsValidEmail(mail))
                                    {
                                        password = GameUtils.MD5Hash(password);

                                        ExecuteNonQuery("INSERT INTO users (mail, password, created) VALUES (@mail, @password, @created)", new MySqlParameter("@mail", mail), new MySqlParameter("@password", password), new MySqlParameter("@created", GameManager.instance.ServerTime.ToString()));

                                        // check account name, password, banned status
                                        bool valid = ((long)ExecuteScalar("SELECT Count(*) FROM users WHERE mail=@mail AND password=@password AND banned=0", new MySqlParameter("@mail", mail), new MySqlParameter("@password", password))) == 1;
                                        if (valid)
                                        {
                                            int playerID = ((int)ExecuteScalar("SELECT id FROM users WHERE mail=@mail AND password=@password AND banned=0", new MySqlParameter("@mail", mail), new MySqlParameter("@password", password)));

                                            // save last login time and return true
                                            ExecuteNonQuery("UPDATE users SET lastlogin=@lastlogin WHERE mail=@mail", new MySqlParameter("@mail", mail), new MySqlParameter("@lastlogin", GameManager.instance.ServerTime.ToString()));

                                            GameObject go = new GameObject();
                                            go.AddComponent<Player>();
                                            Player player = go.GetComponent<Player>();

                                            player.connectionID = msg.connectionId;

                                            UserLoad(playerID, player);

                                            player.playerID = playerID;
                                            player.name = playerID + " - " + mail;

                                            player.SetCrimePointsMoney(0, GameManager.instance.playerStartMoney);

                                            //player.money = GameManager.instance.playerStartMoney;

                                            players.Add(msg.connectionId, player);

                                            PlayerDatas pd = new PlayerDatas();
                                            pd.CopyPlayerDatas(player);

                                            
                                            SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.LoginSucceed, JsonConvert.SerializeObject(pd))); //player datalarının hepsini at

                                            
                                                

                                            SendAnnouncements(player);
                                            GetSmugglingItemsSucceed(player);
                                        }
                                    }
                                    else
                                    {
                                        //mail adress is not valid
                                        
                                        SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.RegisterFailed, "Mailaddressisnotvalid"));

                                        
                                            
                                    }
                                }
                                else
                                {
                                    //password is too short 
                                    
                                    SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.RegisterFailed, "Passwordistooshort"));

                                    
                                        
                                }
                            }
                            else
                            {
                                //passwords are not same
                                
                                SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.RegisterFailed, "Passwordsarenotsame"));

                                
                                    
                            }
                        }
                        else
                        {
                            //mails are not same
                            
                            SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.RegisterFailed, "Mailsarenotsame"));

                            
                                
                        }
                    }

                }
                break;

            case NetMessageTypes.LoginRequest:
                {
                    PlayerMailPassword pmp = JsonConvert.DeserializeObject<PlayerMailPassword>(netMessage.message);

                    int _version = pmp.version;
                    string mail = pmp.mail;
                    string mail2 = pmp.mail2;
                    string password = pmp.password;
                    string password2 = pmp.password2;

                    if (version > _version)
                    {
                        
                        SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.LoginFailed, "Versionsarenotsame"));

                        
                            

                        return;
                    }

                    //check login succeed?

                    var result = ExecuteScalar("SELECT id FROM users WHERE mail=@mail AND password=@password AND banned=0", new MySqlParameter("@mail", mail), new MySqlParameter("@password", GameUtils.MD5Hash(password)));

                    if (result != null)
                    {
                        int playerID = Convert.ToInt32(result);

                        var entry = players.Where(e => e.Value.playerID == playerID).Select(p => p.Value.playerID);

                        if (entry.Sum() != 0)
                        {
                            SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.LoginFailed, "AccountCurrentlyOnline"));                                                           

                            return;
                        }

                        // save last login time and return true
                        ExecuteNonQuery("UPDATE users SET lastlogin=@lastlogin WHERE mail=@mail", new MySqlParameter("@mail", mail), new MySqlParameter("@lastlogin", GameManager.instance.ServerTime.ToString()));

                        GameObject go = new GameObject();
                        go.AddComponent<Player>();
                        Player player = go.GetComponent<Player>();

                        player.connectionID = msg.connectionId;

                        UserLoad(playerID, player);

                        player.playerID = playerID;
                        player.name = playerID + " - " + mail;

                        players.Add(msg.connectionId, player);

                        var logoutResult = (string)ExecuteScalar("SELECT lastLogout FROM users WHERE id = @id", new MySqlParameter("@id", playerID));

                        if (!String.IsNullOrEmpty(logoutResult))
                        {
                            DateTime logoutTime = Convert.ToDateTime(logoutResult);

                            player.curHealth += (int)(((GameManager.instance.ServerTime - logoutTime).TotalMinutes / GameManager.instance.regenerationMinute) * GameManager.instance.regenerationHealth);

                            player.curHealth = Mathf.Min(100, player.curHealth);
                        }

                        PlayerDatas pd = new PlayerDatas();
                        pd.CopyPlayerDatas(player);

                        
                        SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.LoginSucceed, JsonConvert.SerializeObject(pd)));

                        
                            

                        if (!String.IsNullOrEmpty(player.guild.name))
                        {
                            SetGuildOnline(player, true);
                        }

                        SendAnnouncements(player);
                        GetSmugglingItemsSucceed(player);
                    }
                    else
                    {
                        result = ExecuteScalar("SELECT id FROM users WHERE mail=@mail AND password=@password AND banned=1", new MySqlParameter("@mail", mail), new MySqlParameter("@password", GameUtils.MD5Hash(password)));

                        if (result != null)
                        {
                            
                            SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.LoginFailed, "Youarebanned"));

                            
                                
                        }
                        else
                        {
                            
                            SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.LoginFailed, "LoginFailed"));

                            
                                
                        }


                    }
                }
                break;

            case NetMessageTypes.SetCharnameRequest:
                {
                    Player player = players[msg.connectionId];

                    if (player != null)
                    {
                        List<List<object>> table = ExecuteReader("SELECT charname FROM users WHERE id=@id", new MySqlParameter("@id", player.playerID));
                        if (table.Count == 1)
                        {
                            List<object> mainrow = table[0];

                            if (mainrow[0] == DBNull.Value)
                            {
                                string charname = netMessage.message;

                                

                                if (!string.IsNullOrEmpty(charname))
                                {
                                    try
                                    {
                                        ExecuteNonQuery("UPDATE users SET charname=@charname WHERE id=@id;", new MySqlParameter("@charname", charname), new MySqlParameter("@id", player.playerID));
                                    }
                                    catch (Exception e)
                                    {
                                        SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.SetCharnameFailed, "SetCharnameFailed"));

                                        
                                            

                                        Debug.Log(e.ToString());
                                        break;
                                    }

                                    player.charName = charname;

                                    SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.SetCharnameSucceed, charname));

                                    
                                        
                                }
                                else
                                {
                                    SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.SetCharnameFailed, "SetCharnameFailed"));

                                    
                                        
                                }
                            }
                            else
                            {
                                
                                SendData(msg.connectionId, new NetworkMessage(NetMessageTypes.SetCharnameFailed, "SetCharnameFailed"));

                                
                                    
                            }
                        }
                    }
                }
                break;

            case NetMessageTypes.CommitCrimeRequest:
                {
                    Player player = players[msg.connectionId];

                    int enemyHash = Convert.ToInt32(netMessage.message);

                    if (ScriptableEnemy.dict.ContainsKey(enemyHash))
                    {
                        player.CommitCrime(ScriptableEnemy.dict[enemyHash]);
                    }
                }
                break;

            case NetMessageTypes.BuyRealEstateRequest:
                {
                    Player player = players[msg.connectionId];

                    int realEstateHash = Convert.ToInt32(netMessage.message);

                    if (ScriptableRealEstate.dict.ContainsKey(realEstateHash))
                    {
                        player.BuyRealEstate(ScriptableRealEstate.dict[realEstateHash]);
                    }
                }
                break;

            case NetMessageTypes.GetIncomeRealEstateRequest:
                {
                    Player player = players[msg.connectionId];

                    int realEstateHash = Convert.ToInt32(netMessage.message);

                    if (ScriptableRealEstate.dict.ContainsKey(realEstateHash))
                    {
                        player.GetIncomeRealEstate(ScriptableRealEstate.dict[realEstateHash]);
                    }
                }
                break;


            case NetMessageTypes.BuyManagementRequest:
                {
                    Player player = players[msg.connectionId];

                    int managementHash = Convert.ToInt32(netMessage.message);

                    if (ScriptableManagement.dict.ContainsKey(managementHash))
                    {
                        player.BuyManagement(ScriptableManagement.dict[managementHash]);
                    }
                }
                break;

            case NetMessageTypes.GetIncomeManagementRequest:
                {
                    Player player = players[msg.connectionId];

                    int managementHash = Convert.ToInt32(netMessage.message);

                    if (ScriptableManagement.dict.ContainsKey(managementHash))
                    {
                        player.GetIncomeManagement(ScriptableManagement.dict[managementHash]);
                    }
                }
                break;

            case NetMessageTypes.TrainRequest:
                {
                    Player player = players[msg.connectionId];

                    string trainType = netMessage.message;

                    player.Train(trainType);
                }
                break;

            case NetMessageTypes.GiveABribeRequest:
                {
                    Player player = players[msg.connectionId];

                    player.GiveABribe();
                }
                break;

            case NetMessageTypes.EscapeFromPrisonRequest:
                {
                    Player player = players[msg.connectionId];

                    player.EscapeFromPrison();
                }
                break;

            case NetMessageTypes.MoneyBankTransferRequest:
                {
                    Player player = players[msg.connectionId];

                    PlayerMoneyBankMoney pmbm = JsonConvert.DeserializeObject<PlayerMoneyBankMoney>(netMessage.message);

                    player.PutMoneyBank(pmbm.fromBankToInventory, pmbm.money);
                }
                break;

            case NetMessageTypes.DoStoryRequest:
                {
                    Player player = players[msg.connectionId];

                    int storyHash = Convert.ToInt32(netMessage.message);

                    if (ScriptableStory.dict.ContainsKey(storyHash))
                    {
                        player.DoStory(ScriptableStory.dict[storyHash]);
                    }
                }
                break;

            case NetMessageTypes.SwapItemRequest:
                {
                    Player player = players[msg.connectionId];

                    SwapItemDatas sid = JsonConvert.DeserializeObject<SwapItemDatas>(netMessage.message);

                    player.SwapInventoryEquip(sid.invNo, sid.eqNo);
                }
                break;


            case NetMessageTypes.BuyShopItemRequest:
                {
                    Player player = players[msg.connectionId];

                    ShopItemDatas sid = JsonConvert.DeserializeObject<ShopItemDatas>(netMessage.message);

                    player.BuyShopItem(sid.eqType, sid.index, sid.amount);
                }
                break;

            case NetMessageTypes.SellShopItemRequest:
                {
                    Player player = players[msg.connectionId];

                    ShopItemDatas sid = JsonConvert.DeserializeObject<ShopItemDatas>(netMessage.message);

                    player.SellShopItem(sid.index, sid.amount);
                }
                break;

            case NetMessageTypes.GetRankCrimePointsRequest:
                {
                    Player player = players[msg.connectionId];

                    GetRankDatas grd = JsonConvert.DeserializeObject<GetRankDatas>(netMessage.message);

                    if (grd.myRank)
                    {
                        GetMyRank(player, grd.rankDataType, grd.userPerPage);
                    }
                    else
                    {
                        GetRank(player, grd.rankDataType, grd.pageNo, grd.userPerPage);
                    }
                }
                break;

            case NetMessageTypes.GetRankMoneyRequest:
                {
                    Player player = players[msg.connectionId];

                    GetRankDatas grd = JsonConvert.DeserializeObject<GetRankDatas>(netMessage.message);

                    if (grd.myRank)
                    {
                        GetMyRank(player, grd.rankDataType, grd.userPerPage);
                    }
                    else
                    {
                        GetRank(player, grd.rankDataType, grd.pageNo, grd.userPerPage);
                    }
                }
                break;

            case NetMessageTypes.GetRankTotalPvEKillRequest:
                {
                    Player player = players[msg.connectionId];

                    GetRankDatas grd = JsonConvert.DeserializeObject<GetRankDatas>(netMessage.message);

                    if (grd.myRank)
                    {
                        GetMyRank(player, grd.rankDataType, grd.userPerPage);
                    }
                    else
                    {
                        GetRank(player, grd.rankDataType, grd.pageNo, grd.userPerPage);
                    }
                }
                break;

            case NetMessageTypes.GetRankTotalPvEDeathRequest:
                {
                    Player player = players[msg.connectionId];

                    GetRankDatas grd = JsonConvert.DeserializeObject<GetRankDatas>(netMessage.message);

                    if (grd.myRank)
                    {
                        GetMyRank(player, grd.rankDataType, grd.userPerPage);
                    }
                    else
                    {
                        GetRank(player, grd.rankDataType, grd.pageNo, grd.userPerPage);
                    }
                }
                break;

            case NetMessageTypes.GetSmugglingItemsRequest:
                {
                    Player player = players[msg.connectionId];

                    GetSmugglingItemsSucceed(player);
                }
                break;

            case NetMessageTypes.BuySmugglingItemsRequest:
                {
                    Player player = players[msg.connectionId];

                    ShopItemDatas sid = JsonConvert.DeserializeObject<ShopItemDatas>(netMessage.message);

                    player.BuySmugglingItem(sid.index, sid.amount);
                }
                break;

            case NetMessageTypes.SellSmugglingItemsRequest:
                {
                    Player player = players[msg.connectionId];

                    ShopItemDatas sid = JsonConvert.DeserializeObject<ShopItemDatas>(netMessage.message);

                    player.SellSmugglingItem(sid.index, sid.amount);
                }
                break;

            case NetMessageTypes.ArenaAttackRequest:
                {
                    Player player = players[msg.connectionId];

                    string enemyName = netMessage.message;

                    //TODO: Arena sıralamalarını karşılaştır

                    int index = players.Values.ToList().FindIndex(x => x.charName == enemyName);
                    if (index >= 0)
                    {
                        Player enemy = players.Values.ToList()[index];
                        player.ArenaAttack(enemy);
                    }
                    else
                    {
                        GameObject go = new GameObject();
                        Player _enemy = go.AddComponent<Player>();

                        bool temp = UserLoadArena(enemyName, _enemy);

                        if (temp)
                        {
                            player.ArenaAttack(_enemy);
                        }
                        else
                        {
                            TryArenaAttackFailed(player, "PlayerNotFound");
                        }

                        Destroy(_enemy.gameObject);
                    }

                }
                break;

            case NetMessageTypes.GetArenaListRequest:
                {
                    Player player = players[msg.connectionId];

                    GetArenaList(player, 10);
                }
                break;

            case NetMessageTypes.SendChatMessage:
                {
                    Player player = players[msg.connectionId];

                    ChatMessage cm = JsonConvert.DeserializeObject<ChatMessage>(netMessage.message);

                    ProcessChatMessage(player, cm);
                }
                break;

            case NetMessageTypes.CreateGuildRequest:
                {
                    Player player = players[msg.connectionId];

                    string guildName = netMessage.message;

                    CreateGuild(player, guildName);
                }
                break;

            case NetMessageTypes.LeaveGuildRequest:
                {
                    Player player = players[msg.connectionId];

                    LeaveGuild(player);
                }
                break;

            case NetMessageTypes.TerminateGuildRequest:
                {
                    Player player = players[msg.connectionId];

                    TerminateGuild(player);
                }
                break;

            case NetMessageTypes.SetGuildNoticeRequest:
                {
                    Player player = players[msg.connectionId];

                    string notice = netMessage.message;

                    SetGuildNotice(player, notice);
                }
                break;

            case NetMessageTypes.KickFromGuildRequest:
                {
                    Player player = players[msg.connectionId];

                    string member = netMessage.message;

                    KickFromGuild(player, member);
                }
                break;

            case NetMessageTypes.InviteToGuildRequest:
                {
                    Player player = players[msg.connectionId];

                    string member = netMessage.message;

                    InviteToGuild(player, member);
                }
                break;

            case NetMessageTypes.ApplyToGuildRequest:
                {
                    Player player = players[msg.connectionId];

                    string guildName = netMessage.message;

                    ApplyToGuild(player, guildName);
                }
                break;

            case NetMessageTypes.PromoteMemberRequest:
                {
                    Player player = players[msg.connectionId];

                    string memeber = netMessage.message;

                    PromoteMember(player, memeber);
                }
                break;

            case NetMessageTypes.DemoteMemberRequest:
                {
                    Player player = players[msg.connectionId];

                    string memeber = netMessage.message;

                    DemoteMember(player, memeber);
                }
                break;
            case NetMessageTypes.GetGuildNamesForApplyRequest:
                {
                    Player player = players[msg.connectionId];

                    string guildName = netMessage.message;

                    GetGuildNamesForApply(player, guildName);
                }
                break;

            case NetMessageTypes.AcceptGuildRequest:
                {
                    Player player = players[msg.connectionId];

                    string guildName = netMessage.message;

                    AcceptGuildRequest(player, guildName);
                }
                break;

            case NetMessageTypes.RejectGuildRequest:
                {
                    Player player = players[msg.connectionId];

                    string guildName = netMessage.message;

                    RejectGuildRequest(player, guildName);
                }
                break;

            case NetMessageTypes.AcceptGuildApplyRequest:
                {
                    Player player = players[msg.connectionId];

                    string memberName = netMessage.message;

                    AcceptGuildApply(player, memberName);
                }
                break;

            case NetMessageTypes.RejectGuildApplyRequest:
                {
                    Player player = players[msg.connectionId];

                    string memberName = netMessage.message;

                    RejectGuildApply(player, memberName);
                }
                break;

            case NetMessageTypes.GetItemsFromMailRequest:
                {
                    Player player = players[msg.connectionId];

                    int id = Convert.ToInt32(netMessage.message);

                    player.GetItemsFromMail(id);
                }
                break;

            case NetMessageTypes.GetStealMoneyPlayersList:
                {
                    Player player = players[msg.connectionId];

                    GetStealMoneyPlayersList(player, 10);
                }
                break;

            case NetMessageTypes.StealMoneyRequest:
                {
                    Player player = players[msg.connectionId];

                    string enemyName = netMessage.message;

                    StealMoney(player, enemyName);
                }
                break;

            case NetMessageTypes.InAppPurchaseRequest:
                {
                    Player player = players[msg.connectionId];

                    InAppPurchaseDatas iapData = JsonConvert.DeserializeObject<InAppPurchaseDatas>(netMessage.message);

                    ProcessInAppPurchase(player, iapData);
                }
                break;

            case NetMessageTypes.GetFreeEnergyRequest:
                {
                    Player player = players[msg.connectionId];

                    ProcessFreeEnergyRequest(player);
                }
                break;

            case NetMessageTypes.GetFreeMoneyRequest:
                {
                    Player player = players[msg.connectionId];

                    ProcessFreeMoneyRequest(player);
                }
                break;

            default:

                break;
        }
    }

    public void SendData(int connectionId, NetworkMessage networkMessage)
    {
        _netServer.Send(connectionId, networkMessage.ObjectToByteArray());
    }

    // helper functions ////////////////////////////////////////////////////////
    // run a query that doesn't return anything
    public void ExecuteNonQuery(string sql, params MySqlParameter[] args)
    {
        using (MySqlCommand command = new MySqlCommand(sql, connection))
        {
            foreach (MySqlParameter param in args)
                command.Parameters.Add(param);

            Debug.LogError(command.CommandText);

            command.ExecuteNonQuery();
        }
    }

    // run a query that returns a single value
    public object ExecuteScalar(string sql, params MySqlParameter[] args)
    {
        using (MySqlCommand command = new MySqlCommand(sql, connection))
        {
            foreach (MySqlParameter param in args)
                command.Parameters.Add(param);
            return command.ExecuteScalar();
        }
    }

    // run a query that returns several values
    // note: sqlite has long instead of int, so use Convert.ToInt32 etc.
    public List<List<object>> ExecuteReader(string sql, params MySqlParameter[] args)
    {
        List<List<object>> result = new List<List<object>>();

        using (MySqlCommand command = new MySqlCommand(sql, connection))
        {
            foreach (MySqlParameter param in args)
                command.Parameters.Add(param);

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                //
                //result.Load(reader); (DataTable)
                //
                // UPDATE: DataTable.Load(reader) works in Net 4.X now, but it's
                //         20x slower than the current approach.
                //         select * from character_inventory x 1000:
                //           425ms before
                //          7303ms with DataRow
                while (reader.Read())
                {
                    object[] buffer = new object[reader.FieldCount];
                    reader.GetValues(buffer);
                    result.Add(buffer.ToList());
                }
            }
        }
        return result;
    }

    public bool UserLoad(int id, Player player)
    {
        List<List<object>> table = ExecuteReader("SELECT * FROM users WHERE id=@id", new MySqlParameter("@id", id));

        if (table.Count == 1)
        {
            List<object> mainrow = table[0];

            player.playerID = id;
            player.online = true;

            if (mainrow[7] != DBNull.Value)
            {
                player.charName = (string)mainrow[7];
            }
            else
            {
                player.charName = "";
            }

            player._strength = (float)mainrow[8];
            player._intelligence = (float)mainrow[9];

            player.playerGrade = (PlayerGrade)Convert.ToInt32(mainrow[10]);
            player.crimePoints = Convert.ToInt32(mainrow[11]);

            player.CheckGrade();

            player.money = Convert.ToInt32(mainrow[12]);

            player.totalPvEKill = Convert.ToInt32(mainrow[13]);
            player.totalPvEDeath = Convert.ToInt32(mainrow[14]);

            player.totalArenaKill = Convert.ToInt32(mainrow[15]);
            player.totalArenaDeath = Convert.ToInt32(mainrow[16]);

            int vipHash = Convert.ToInt32(mainrow[18]);

            string vipTimeEnd = (string)mainrow[30];

            if (vipHash != 0)
            {
                if (!String.IsNullOrEmpty(vipTimeEnd))
                {
                    player.vip = new VIP(ScriptableVIP.dict[vipHash], Convert.ToDateTime(vipTimeEnd));
                }
            }

            if (mainrow[20] != DBNull.Value)
            {
                List<Item> tempEq = JsonConvert.DeserializeObject<List<Item>>((string)mainrow[20]);

                if (tempEq.Count > 0)
                {
                    for (int i = 0; i < tempEq.Count; i++)
                    {
                        if (tempEq[i].hash != 0)
                        {
                            player.equipments[i] = new Item(ScriptableItem.dict[tempEq[i].data.hash]);
                        }
                    }
                }
            }

            if (mainrow[19] != DBNull.Value)
            {
                List<ScriptableItemAndAmount> tempInv = JsonConvert.DeserializeObject<List<ScriptableItemAndAmount>>((string)mainrow[19]);

                if (tempInv.Count > 0)
                {
                    for (int i = 0; i < tempInv.Count; i++)
                    {
                        if (tempInv[i].amount > 0 && tempInv[i].item.hash != 0)
                        {
                            if (player.inventory.Count > i)
                            {
                                player.inventory[i] = new ScriptableItemAndAmount(ScriptableItem.dict[tempInv[i].item.hash], tempInv[i].amount);
                            }
                            else
                            {
                                player.inventory.Add(new ScriptableItemAndAmount(ScriptableItem.dict[tempInv[i].item.hash], tempInv[i].amount));
                            }
                        }
                    }
                }
            }

            player.RefreshInventorySize();

            player.curHealth = Convert.ToInt32(mainrow[21]);

            string lastAttackTime = (string)mainrow[22];

            if (!String.IsNullOrEmpty(lastAttackTime))
            {
                player.lastAttackTime = Convert.ToDateTime(lastAttackTime);
            }

            if (mainrow[23] != DBNull.Value)
            {
                player.buffs = JsonConvert.DeserializeObject<List<Buff>>((string)mainrow[23]);
            }

            string jailFinishTime = (string)mainrow[24];

            if (!String.IsNullOrEmpty(jailFinishTime))
            {
                player.jailFinishTime = Convert.ToDateTime(jailFinishTime);
            }

            if (mainrow[25] != DBNull.Value)
            {
                player.realEstates = JsonConvert.DeserializeObject<List<RealEstate>>((string)mainrow[25]);
            }

            if (mainrow[26] != DBNull.Value)
            {
                player.managements = JsonConvert.DeserializeObject<List<Management>>((string)mainrow[26]);
            }

            player.moneyBank = Convert.ToInt32(mainrow[27]);
            player.lastSucceedStoryNo = Convert.ToInt32(mainrow[28]);

            string lastArenaAttackTime = (string)mainrow[29];

            if (!String.IsNullOrEmpty(lastArenaAttackTime))
            {
                player.lastArenaAttackTime = Convert.ToDateTime(lastArenaAttackTime);
            }

            //30 vip time end

            string guild = (string)mainrow[31];
            GuildRank rank = (GuildRank)Convert.ToInt32(mainrow[32]);

            if(!String.IsNullOrEmpty(guild))
            {
                player.guild = guilds[guild];
            }

            string invitedGuilds = (string)mainrow[33];

            player.invitedGuilds = String.IsNullOrEmpty(invitedGuilds) ? new List<string>() : JsonConvert.DeserializeObject<List<string>>(invitedGuilds);

            player.mails = GetPlayerMails(player);

            string lastStealMoneyTime = (string)mainrow[34];

            if (!String.IsNullOrEmpty(lastStealMoneyTime))
            {
                player.lastStealMoneyTime = Convert.ToDateTime(lastStealMoneyTime);
            }

            string lastEnergyCollected = (string)mainrow[35];
            
            if (!String.IsNullOrEmpty(lastEnergyCollected))
            {
                player.lastEnergyCollected = Convert.ToDateTime(lastEnergyCollected);
            }

            

            return true;
        }

        return false;
    }

    // adds or overwrites character data in the database
    public void UserSave(Player player)
    {
        ExecuteNonQuery("UPDATE users SET strength = @strength, intelligence = @intelligence," +
            " playerGrade = @playerGrade, crimePoints = @crimePoints, money = @money, totalPvEKill = @totalPvEKill, " +
            "totalPvEDeath = @totalPvEDeath, totalArenaKill = @totalArenaKill, totalArenaDeath = @totalArenaDeath, " +
            "VIP=@vip, inventory=@inventory, equipments=@equipments, curHealth=@curHealth, lastAttackTime=@lastAttackTime," +
            " buffs = @buffs, jailFinishTime=@jailFinishTime, realEstates=@realEstates, managements=@managements, moneyBank=@moneyBank," +
            " lastSucceedStoryNo=@lastSucceedStoryNo, lastArenaAttackTime=@lastArenaAttackTime, vipTimeEnd=@vipTimeEnd," +
            " invitedGuilds=@invitedGuilds, lastStealMoneyTime = @lastStealMoneyTime, lastEnergyCollected = @lastEnergyCollected  WHERE id=@id",
                        new MySqlParameter("@id", player.playerID),
                        new MySqlParameter("@strength", player._strength),
                        new MySqlParameter("@intelligence", player._intelligence),
                        new MySqlParameter("@playerGrade", (int)player.playerGrade),
                        new MySqlParameter("@crimePoints", player.crimePoints),
                        new MySqlParameter("@money", player.money),
                        new MySqlParameter("@totalPvEKill", player.totalPvEKill),
                        new MySqlParameter("@totalPvEDeath", player.totalPvEDeath),
                        new MySqlParameter("@totalArenaKill", player.totalArenaKill),
                        new MySqlParameter("@totalArenaDeath", player.totalArenaDeath),
                        new MySqlParameter("@vip", player.vip.hash),
                        new MySqlParameter("@inventory", player.inventory.Count == 0 ? (object)DBNull.Value : JsonConvert.SerializeObject(player.inventory)),
                        new MySqlParameter("@equipments", player.equipments.Count == 0 ? (object)DBNull.Value : JsonConvert.SerializeObject(player.equipments)),
                        new MySqlParameter("@curHealth", player.curHealth),
                        new MySqlParameter("@lastAttackTime", player.lastAttackTime.ToString()),
                        new MySqlParameter("@buffs", player.buffs.Count == 0 ? (object)DBNull.Value : JsonConvert.SerializeObject(player.buffs)),
                        new MySqlParameter("@jailFinishTime", player.jailFinishTime.Year >= 2020 ? player.jailFinishTime.ToString() : ""),
                        new MySqlParameter("@realEstates", player.realEstates.Count == 0 ? (object)DBNull.Value : JsonConvert.SerializeObject(player.realEstates)),
                        new MySqlParameter("@managements", player.managements.Count == 0 ? (object)DBNull.Value : JsonConvert.SerializeObject(player.managements)),
                        new MySqlParameter("@moneyBank", player.moneyBank.ToString()),
                        new MySqlParameter("@lastSucceedStoryNo", player.lastSucceedStoryNo),
                        new MySqlParameter("@lastArenaAttackTime", player.lastArenaAttackTime.ToString()),
                        new MySqlParameter("@vipTimeEnd", player.vip.hash != 0 ? player.vip.vipTimeEnd.ToString() : ""),
                        new MySqlParameter("@invitedGuilds", player.invitedGuilds.Count > 0 ? player.invitedGuilds.ToString() : ""),
                        new MySqlParameter("@lastStealMoneyTime", player.lastStealMoneyTime.ToString()),
                        new MySqlParameter("@lastEnergyCollected", player.lastEnergyCollected.ToString()));   
    }

    public bool UserLoadArena(string charName, Player player)
    {
        List<List<object>> table = ExecuteReader("SELECT * FROM users WHERE charName = @charName", new MySqlParameter("@charName", charName));
        
        if (table.Count == 1)
        {
            List<object> mainrow = table[0];

            player.playerID = Convert.ToInt32(mainrow[0]);

            if (mainrow[7] != DBNull.Value)
            {
                player.charName = (string)mainrow[7];
            }
            else
            {
                player.charName = "";
            }

            player._strength = (float)mainrow[8];
            player._intelligence = (float)mainrow[9];

            player.playerGrade = (PlayerGrade)Convert.ToInt32(mainrow[10]);
            player.crimePoints = Convert.ToInt32(mainrow[11]);

            player.CheckGrade();

            player.money = Convert.ToInt32(mainrow[12]);

            player.totalPvEKill = Convert.ToInt32(mainrow[13]);
            player.totalPvEDeath = Convert.ToInt32(mainrow[14]);

            player.totalArenaKill = Convert.ToInt32(mainrow[15]);
            player.totalArenaDeath = Convert.ToInt32(mainrow[16]);

            int vipHash = Convert.ToInt32(mainrow[18]);

            string vipTimeEnd = (string)mainrow[30];

            if (vipHash != 0)
            {
                if (!String.IsNullOrEmpty(vipTimeEnd))
                {
                    player.vip = new VIP(ScriptableVIP.dict[vipHash], Convert.ToDateTime(vipTimeEnd));                   
                }
            }

            if (mainrow[20] != DBNull.Value)
            {
                List<Item> tempEq = JsonConvert.DeserializeObject<List<Item>>((string)mainrow[20]);

                if (tempEq.Count > 0)
                {
                    for (int i = 0; i < tempEq.Count; i++)
                    {
                        if (tempEq[i].hash != 0)
                        {
                            player.equipments[i] = new Item(ScriptableItem.dict[tempEq[i].data.hash]);
                        }
                    }
                }
            }

            if (mainrow[19] != DBNull.Value)
            {
                List<ScriptableItemAndAmount> tempInv = JsonConvert.DeserializeObject<List<ScriptableItemAndAmount>>((string)mainrow[19]);

                if (tempInv.Count > 0)
                {
                    for (int i = 0; i < tempInv.Count; i++)
                    {
                        if (tempInv[i].amount > 0 && tempInv[i].item.hash != 0)
                        {
                            if (player.inventory.Count > i)
                            {
                                player.inventory[i] = new ScriptableItemAndAmount(ScriptableItem.dict[tempInv[i].item.hash], tempInv[i].amount);
                            }
                            else
                            {
                                player.inventory.Add(new ScriptableItemAndAmount(ScriptableItem.dict[tempInv[i].item.hash], tempInv[i].amount));
                            }
                        }
                    }
                }
            }

            player.RefreshInventorySize();

            player.curHealth = Convert.ToInt32(mainrow[21]);

            string lastAttackTime = (string)mainrow[22];

            if (!String.IsNullOrEmpty(lastAttackTime))
            {
                player.lastAttackTime = Convert.ToDateTime(lastAttackTime);
            }

            if (mainrow[23] != DBNull.Value)
            {
                player.buffs = JsonConvert.DeserializeObject<List<Buff>>((string)mainrow[23]);
            }

            string jailFinishTime = (string)mainrow[24];

            if (!String.IsNullOrEmpty(jailFinishTime))
            {
                player.jailFinishTime = Convert.ToDateTime(jailFinishTime);
            }

            if (mainrow[25] != DBNull.Value)
            {
                player.realEstates = JsonConvert.DeserializeObject<List<RealEstate>>((string)mainrow[25]);
            }

            if (mainrow[26] != DBNull.Value)
            {
                player.managements = JsonConvert.DeserializeObject<List<Management>>((string)mainrow[26]);
            }

            player.moneyBank = Convert.ToInt32(mainrow[27]);
            player.lastSucceedStoryNo = Convert.ToInt32(mainrow[28]);

            string lastArenaAttackTime = (string)mainrow[29];

            if (!String.IsNullOrEmpty(lastAttackTime))
            {
                player.lastArenaAttackTime = Convert.ToDateTime(lastArenaAttackTime);
            }

            //30 vip time end

            string guild = (string)mainrow[31];
            GuildRank rank = (GuildRank)Convert.ToInt32(mainrow[32]);

            if (!String.IsNullOrEmpty(guild))
            {
                player.guild = guilds[guild];
            }

            string invitedGuilds = (string)mainrow[33];

            player.invitedGuilds = String.IsNullOrEmpty(invitedGuilds) ? new List<string>() : JsonConvert.DeserializeObject<List<string>>(invitedGuilds);

            player.mails = GetPlayerMails(player);

            string lastStealMoneyTime = (string)mainrow[34];

            if (!String.IsNullOrEmpty(lastStealMoneyTime))
            {
                player.lastStealMoneyTime = Convert.ToDateTime(lastStealMoneyTime);
            }

            string lastEnergyCollected = (string)mainrow[35];

            if (!String.IsNullOrEmpty(lastEnergyCollected))
            {
                player.lastEnergyCollected = Convert.ToDateTime(lastEnergyCollected);
            }
            

            return true;
        }

        return false;
    }

    public void SyncHealth(Player player)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SyncHealth, player.curHealth.ToString()));
    }

    public void SyncMoney(Player player)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SyncMoney, player.money.ToString()));

        
            
    }

    

    #region Commit Crime
    public void CommitCrimeSucceed(Player player, int enemyHash)
    {
        HealthEnemyMoneyCrimePointsAttackTimeJailTime hemcpat = new HealthEnemyMoneyCrimePointsAttackTimeJailTime();
        hemcpat.curHealth = player.curHealth;
        hemcpat.enemyHash = enemyHash;
        hemcpat.curMoney = player.money;
        hemcpat.curCrimePoints = player.crimePoints;
        hemcpat.lastAttackTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.lastAttackTime));
        hemcpat.jailFinishTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.jailFinishTime));

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.CommitCrimeSucceed, JsonConvert.SerializeObject(hemcpat)));

        
            
    }

    public void CommitCrimeFailed(Player player, int enemyHash, bool jailed)
    {
        EnemyHashHealthJail ehj = new EnemyHashHealthJail();
        ehj.enemyHash = enemyHash;
        ehj.health = player.curHealth;
        ehj.lastAttackTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.lastAttackTime));

        if (jailed) ehj.jailFinishTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.jailFinishTime));

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.CommitCrimeFailed, JsonConvert.SerializeObject(ehj)));

        
            
    }

    public void TryCommitCrimeFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryCommitCrimeFailed, reason));

        
            
    }

    #endregion

    #region Real Estates

    public void BuyRealEstateSucceed(Player player, int realEstateHash)
    {
        PlayerRealEstateMoney prem = new PlayerRealEstateMoney();
        prem.realEstateHash = realEstateHash;
        prem.money = player.money;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.BuyRealEstateSucceed, JsonConvert.SerializeObject(prem)));

        
            
    }

    public void BuyRealEstateFailed(Player player, int realEstateHash)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.BuyRealEstateFailed, realEstateHash.ToString()));

        
            
    }

    public void TryBuyRealEstateFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryBuyRealEstateFailed, reason));

        
            
    }

    public void GetRealEstateIncomeSucceed(Player player, int realEstateHash)
    {
        PlayerRealEstateMoney prem = new PlayerRealEstateMoney();
        prem.realEstateHash = realEstateHash;
        prem.money = player.money;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetIncomeRealEstateSucceed, JsonConvert.SerializeObject(prem)));

        
            
    }

    public void GetRealEstateIncomeFailed(Player player, int realEstateHash)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryGetIncomeRealEstateFailed, realEstateHash.ToString()));

        
            
    }

    public void TryGetRealEstateIncomeFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryGetIncomeRealEstateFailed, reason));

        
            
    }

    #endregion

    #region Management

    public void BuyManagementSucceed(Player player, int managementHash)
    {
        PlayerManagementMoney pmm = new PlayerManagementMoney();
        pmm.managementHash = managementHash;
        pmm.money = player.money;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.BuyManagementSucceed, JsonConvert.SerializeObject(pmm)));

        
            
    }

    public void BuyManagementFailed(Player player, int managementHash)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.BuyManagementFailed, managementHash.ToString()));

        
            
    }

    public void TryBuyManagementFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryBuyManagementFailed, reason));

        
            
    }

    public void GetManagementIncomeSucceed(Player player, int managementHash)
    {
        PlayerManagementMoney prem = new PlayerManagementMoney();
        prem.managementHash = managementHash;
        prem.money = player.money;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetIncomeManagementSucceed, JsonConvert.SerializeObject(prem)));

        
            
    }

    public void GetManagementIncomeFailed(Player player, int managementHash)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryGetIncomeManagementFailed, managementHash.ToString()));

        
            
    }

    public void TryGetManagementIncomeFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryGetIncomeManagementFailed, reason));

        
            
    }

    #endregion


    #region Train

    public void TrainSucceed(Player player, string trainType)
    {
        PlayerHealthMoneyStrengthIntelligence phmsi = new PlayerHealthMoneyStrengthIntelligence();
        phmsi.health = player.curHealth;
        phmsi.money = player.money;
        phmsi.strength = player._strength;
        phmsi.intelligence = player._intelligence;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TrainSucceed, JsonConvert.SerializeObject(phmsi)));

        
            
    }

    public void TrainFailed(Player player, string trainType)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TrainFailed, trainType));

        
            
    }

    public void TryTrainFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryTrainFailed, reason));

        
            
    }

    #endregion

    #region Prison

    public void GiveABribeSucceed(Player player)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GiveABribeSucceed, player.money.ToString()));

        
            
    }

    public void GiveABribeFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GiveABribeFailed, reason));

        
            
    }

    public void TryGiveABribeFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryGiveABribeFailed, reason));

        
            
    }

    public void EscapeFromPrisonSucceed(Player player)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.EscapeFromPrisonSucceed, ""));

        
            
    }

    public void EscapeFromPrisonFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.EscapeFromPrisonFailed, reason));

        
            
    }

    public void TryEscapeFromPrisonFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryEscapeFromPrisonFailed, reason));

        
            
    }

    #endregion

    #region Bank
    public void MoneyBankTransferSucceed(Player player)
    {
        PlayerMoneyBankMoney pmbm = new PlayerMoneyBankMoney();
        pmbm.money = player.money;
        pmbm.bankMoney = player.moneyBank;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.MoneyBankTransferSucceed, JsonConvert.SerializeObject(pmbm)));

        
            
    }

    public void MoneyBankTransferFailed(Player player)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.MoneyBankTransferFailed, ""));

        
            
    }

    public void TryMoneyBankTransferFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryMoneyBankTransferFailed, reason));

        
            
    }

    #endregion

    #region GetAnnouncement
    public List<AnnouncementDatas> GetAnnouncements()
    {
        List<AnnouncementDatas> announcementDatas = new List<AnnouncementDatas>();

        List<List<object>> table = ExecuteReader("SELECT * FROM announcements WHERE active=@active", new MySqlParameter("@active", 1));

        if (table.Count > 0)
        {
            for (int i = 0; i < table.Count; i++)
            {
                List<object> mainrow = table[i];

                AnnouncementDatas announcementData = new AnnouncementDatas();
                announcementData.id = Convert.ToInt32(mainrow[0]);
                announcementData.header = (string)mainrow[1];
                announcementData.text = (string)mainrow[2];
                announcementData.imageLink = (string)mainrow[3];
                announcementData.clickLink = (string)mainrow[4];

                announcementDatas.Add(announcementData);
            }
        }

        return announcementDatas;
    }
    public void SendAnnouncements(Player player)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetAnnouncementsSucceed, JsonConvert.SerializeObject(announcementDatas)));

        
            
    }

    #endregion

    #region Story
    public void DoStorySucceed(Player player, int storyHash)
    {
        HealthEnemyMoneyCrimePointsLastSucceedStoryNo hemcplssn = new HealthEnemyMoneyCrimePointsLastSucceedStoryNo();
        hemcplssn.curHealth = player.curHealth;
        hemcplssn.enemyHash = storyHash;
        hemcplssn.curMoney = player.money;
        hemcplssn.curCrimePoints = player.crimePoints;
        hemcplssn.lastSucceedStoryNo = player.lastSucceedStoryNo;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.DoStorySucceed, JsonConvert.SerializeObject(hemcplssn)));

        
            
    }

    public void DoStoryFailed(Player player, int storyHash)
    {
        EnemyHashHealthJail ehj = new EnemyHashHealthJail();
        ehj.enemyHash = storyHash;
        ehj.health = player.curHealth;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.DoStoryFailed, JsonConvert.SerializeObject(ehj)));

        
            
    }

    public void TryDoStoryFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryDoStoryFailed, reason));

        
            
    }
    #endregion

    #region Swap Item
    public void SwapItemSucceed(Player player, int invNo, int eqNo)
    {
        SwapItemDatas sid = new SwapItemDatas();
        sid.invNo = invNo;
        sid.eqNo = eqNo;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SwapItemSucceed, JsonConvert.SerializeObject(sid)));

        
            
    }

    public void SwapItemFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SwapItemFailed, reason));

        
            
    }

    public void TrySwapItemFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TrySwapItemFailed, reason));

        
            
    }
    #endregion

    #region Buy Shop Item
    public void BuyShopItemSucceed(Player player, PlayerEquipmentTypes eqType, int index, int amount = 1)
    {
        ShopItemDatas sid = new ShopItemDatas();
        sid.eqType = eqType;
        sid.index = index;
        sid.amount = amount;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.BuyShopItemSucceed, JsonConvert.SerializeObject(sid)));

        
            
    }

    public void BuyShopItemFailed(Player player)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.BuyShopItemFailed, ""));

        
            
    }

    public void TryBuyShopItemFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryBuyShopItemFailed, reason));

        
            
    }
    #endregion

    #region Sell Shop Item
    public void SellShopItemSucceed(Player player, int index, int amount)
    {
        ShopItemDatas sid = new ShopItemDatas();
        sid.index = index;
        sid.amount = amount;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SellShopItemSucceed, JsonConvert.SerializeObject(sid)));

        
            
    }

    public void SellShopItemFailed(Player player)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SellShopItemFailed, ""));

        
            
    }

    public void TrySellShopItemFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TrySellShopItemFailed, reason));

        
            
    }
    #endregion

    #region Ranks
    //page no start from 1
    public void GetRank(Player player, RankDataTypes rankType, int pageNo, int userPerPage)
    {
        List<List<object>> table = ExecuteReader("SELECT charname, " + rankType + " FROM users ORDER BY "+ rankType + " DESC, charname ASC LIMIT " + (pageNo - 1) * userPerPage + "," + userPerPage + "" );
        if (table.Count > 0)
        {
            

            switch (rankType)
            {
                case RankDataTypes.crimepoints:

                    List<RankDatasCrimePoint> rankDatasCrimePoints = new List<RankDatasCrimePoint>();

                    for (int i = 0; i < table.Count; i++)
                    {
                        RankDatasCrimePoint rankDatasCrimePoint = new RankDatasCrimePoint();

                        List<object> mainrow = table[i];

                        if (mainrow[0] != DBNull.Value)
                        {
                            rankDatasCrimePoint.charName = (string)mainrow[0];
                            rankDatasCrimePoint.crimePoint = Convert.ToInt32(mainrow[1]);
                            rankDatasCrimePoint.no = (pageNo - 1) * userPerPage + i;

                            rankDatasCrimePoints.Add(rankDatasCrimePoint);
                        }
                    }
                    if(rankDatasCrimePoints.Count > 0)
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankCrimePointsSucceed, JsonConvert.SerializeObject(rankDatasCrimePoints)));
                    }
                    else
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankCrimePointsFailed, "datasareempty"));
                    }


                    break;
                case RankDataTypes.money:

                    List<RankDatasMoney> rankDatasMoneys = new List<RankDatasMoney>();

                    for (int i = 0; i < table.Count; i++)
                    {
                        RankDatasMoney rankDatasMoney = new RankDatasMoney();

                        List<object> mainrow = table[i];

                        if (mainrow[0] != DBNull.Value)
                        {
                            rankDatasMoney.charName = (string)mainrow[0];
                            rankDatasMoney.money = Convert.ToInt32(mainrow[1]);
                            rankDatasMoney.no = (pageNo - 1) * userPerPage + i;

                            rankDatasMoneys.Add(rankDatasMoney);
                        }
                    }
                    if (rankDatasMoneys.Count > 0)
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankMoneySucceed, JsonConvert.SerializeObject(rankDatasMoneys)));
                    }
                    else
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankMoneyFailed, "datasareempty"));
                    }


                    break;
                case RankDataTypes.totalPvEKill:

                    List<RankDatasKillDeath> rankDatasKills = new List<RankDatasKillDeath>();

                    for (int i = 0; i < table.Count; i++)
                    {
                        RankDatasKillDeath rankDatasKill = new RankDatasKillDeath();

                        List<object> mainrow = table[i];

                        if (mainrow[0] != DBNull.Value)
                        {
                            rankDatasKill.charName = (string)mainrow[0];
                            rankDatasKill.kill = Convert.ToInt32(mainrow[1]);
                            rankDatasKill.no = (pageNo - 1) * userPerPage + i;

                            rankDatasKills.Add(rankDatasKill);
                        }
                    }
                    if (rankDatasKills.Count > 0)
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankTotalPvEKillSucceed, JsonConvert.SerializeObject(rankDatasKills)));
                    }
                    else
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankTotalPvEKillFailed, "datasareempty"));
                    }                    

                    break;
                case RankDataTypes.totalPvEDeath:

                    List<RankDatasKillDeath> rankDatasDeaths = new List<RankDatasKillDeath>();

                    for (int i = 0; i < table.Count; i++)
                    {
                        RankDatasKillDeath rankDatasDeath = new RankDatasKillDeath();

                        List<object> mainrow = table[i];

                        if (mainrow[0] != DBNull.Value)
                        {
                            rankDatasDeath.charName = (string)mainrow[0];
                            rankDatasDeath.death = Convert.ToInt32(mainrow[1]);
                            rankDatasDeath.no = (pageNo - 1) * userPerPage + i;

                            rankDatasDeaths.Add(rankDatasDeath);
                        }
                    }
                    if (rankDatasDeaths.Count > 0)
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankTotalPvEDeathSucceed, JsonConvert.SerializeObject(rankDatasDeaths)));
                    }
                    else
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankTotalPvEDeathFailed, "datasareempty"));
                    }

                    break;
                default:
                    break;
            }

            
                          
        }
    }

    public void GetMyRank(Player player, RankDataTypes rankType, int userPerPage)
    {
        int myRank = (int)((long)ExecuteScalar("SELECT COUNT(*) FROM users AS us WHERE us." + rankType + " >= (SELECT " + rankType + " AS point FROM users AS sb WHERE sb.charName = '" + player.charName + "')"));

        Debug.LogError(myRank);

        int pageNo = Mathf.CeilToInt(((float)myRank - 0.5f)/userPerPage);

        List<List<object>> table = ExecuteReader("SELECT charname, " + rankType + " FROM users ORDER BY " + rankType + " DESC, charname ASC LIMIT " + (pageNo - 1) * userPerPage + "," + userPerPage + "");
        if (table.Count > 0)
        {
            

            switch (rankType)
            {
                case RankDataTypes.crimepoints:

                    List<RankDatasCrimePoint> rankDatasCrimePoints = new List<RankDatasCrimePoint>();

                    for (int i = 0; i < table.Count; i++)
                    {
                        RankDatasCrimePoint rankDatasCrimePoint = new RankDatasCrimePoint();

                        List<object> mainrow = table[i];

                        if (mainrow[0] != DBNull.Value)
                        {
                            rankDatasCrimePoint.charName = (string)mainrow[0];
                            rankDatasCrimePoint.crimePoint = Convert.ToInt32(mainrow[1]);
                            rankDatasCrimePoint.no = (pageNo - 1) * userPerPage + i;

                            rankDatasCrimePoints.Add(rankDatasCrimePoint);
                        }
                    }
                    if (rankDatasCrimePoints.Count > 0)
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankCrimePointsSucceed, JsonConvert.SerializeObject(rankDatasCrimePoints)));
                    }
                    else
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankCrimePointsFailed, "datasareempty"));
                    }


                    break;
                case RankDataTypes.money:

                    List<RankDatasMoney> rankDatasMoneys = new List<RankDatasMoney>();

                    for (int i = 0; i < table.Count; i++)
                    {
                        RankDatasMoney rankDatasMoney = new RankDatasMoney();

                        List<object> mainrow = table[i];

                        if (mainrow[0] != DBNull.Value)
                        {
                            rankDatasMoney.charName = (string)mainrow[0];
                            rankDatasMoney.money = Convert.ToInt32(mainrow[1]);
                            rankDatasMoney.no = (pageNo - 1) * userPerPage + i;

                            rankDatasMoneys.Add(rankDatasMoney);
                        }
                    }
                    if (rankDatasMoneys.Count > 0)
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankMoneySucceed, JsonConvert.SerializeObject(rankDatasMoneys)));
                    }
                    else
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankMoneyFailed, "datasareempty"));
                    }


                    break;
                case RankDataTypes.totalPvEKill:

                    List<RankDatasKillDeath> rankDatasKills = new List<RankDatasKillDeath>();

                    for (int i = 0; i < table.Count; i++)
                    {
                        RankDatasKillDeath rankDatasKill = new RankDatasKillDeath();

                        List<object> mainrow = table[i];

                        if (mainrow[0] != DBNull.Value)
                        {
                            rankDatasKill.charName = (string)mainrow[0];
                            rankDatasKill.kill = Convert.ToInt32(mainrow[1]);
                            rankDatasKill.no = (pageNo - 1) * userPerPage + i;

                            rankDatasKills.Add(rankDatasKill);
                        }
                    }
                    if (rankDatasKills.Count > 0)
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankTotalPvEKillSucceed, JsonConvert.SerializeObject(rankDatasKills)));
                    }
                    else
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankTotalPvEKillFailed, "datasareempty"));
                    }

                    break;
                case RankDataTypes.totalPvEDeath:

                    List<RankDatasKillDeath> rankDatasDeaths = new List<RankDatasKillDeath>();

                    for (int i = 0; i < table.Count; i++)
                    {
                        RankDatasKillDeath rankDatasDeath = new RankDatasKillDeath();

                        List<object> mainrow = table[i];

                        if (mainrow[0] != DBNull.Value)
                        {
                            rankDatasDeath.charName = (string)mainrow[0];
                            rankDatasDeath.death = Convert.ToInt32(mainrow[1]);
                            rankDatasDeath.no = (pageNo - 1) * userPerPage + i;

                            rankDatasDeaths.Add(rankDatasDeath);
                        }
                    }
                    if (rankDatasDeaths.Count > 0)
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankTotalPvEDeathSucceed, JsonConvert.SerializeObject(rankDatasDeaths)));
                    }
                    else
                    {
                        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetRankTotalPvEDeathFailed, "datasareempty"));
                    }

                    break;
                default:
                    break;
            }

            
               
        }
    }

    #endregion


    #region Smuggling
    /*public float GetRemainingTimeForChangeSmugglingItemPrice()
    {
        if ((DateTime.UtcNow - nextChangeTimeSmugglintItemsPrices).TotalHours > 22)
        {
            return 0;
        }
        else
        {
            if (DateTime.UtcNow.Hour / 2 != nextChangeTimeSmugglintItemsPrices.Hour / 2)
            {
                return 0;
            }
            else
            {
                int nextChangeHour = (2 * (1 + (int)Math.Floor((double)DateTime.UtcNow.Hour / 2)));

                DateTime nextChangeTime = DateTime.UtcNow.AddHours(nextChangeHour - DateTime.UtcNow.Hour).AddMinutes(-1 * DateTime.UtcNow.Minute).AddSeconds(-1 * DateTime.UtcNow.Second).AddMilliseconds(-1 * DateTime.UtcNow.Millisecond);
                
                return (float)(nextChangeTime - DateTime.UtcNow).TotalSeconds;
            }
        }
    }*/

    public void RefreshSmugglingItemsPrices(bool firstInitialize = false)
    {
        Debug.Log(firstInitialize);
        if(!firstInitialize)
        {
            smugglingItems.Clear();
        }

        if (smugglingItemDatas.Count > 0)
        {
            for (int i = 0; i < smugglingItemDatas.Count; i++)
            {
                ScriptableItemAndPrice siap = new ScriptableItemAndPrice();
                siap.hash = smugglingItemDatas[i].item.hash;
                siap.price = UnityEngine.Random.Range(smugglingItemDatas[i].priceMin, smugglingItemDatas[i].priceMax);
                smugglingItems.Add(siap);
            }

            /*if (DateTime.UtcNow.Hour % 2 == 0)
            {
                nextChangeTimeSmugglintItemsPrices = DateTime.UtcNow.AddHours(2).AddMinutes(-1 * DateTime.UtcNow.Minute)
                    .AddSeconds(-1 * DateTime.UtcNow.Second).AddMilliseconds(-1 * DateTime.UtcNow.Millisecond);
            }
            else
            {
                nextChangeTimeSmugglintItemsPrices = DateTime.UtcNow.AddHours(1).AddMinutes(-1 * DateTime.UtcNow.Minute)
                    .AddSeconds(-1 * DateTime.UtcNow.Second).AddMilliseconds(-1 * DateTime.UtcNow.Millisecond);
            }*/
        }

        if (!firstInitialize)
        {
            for(int i = 0; i < players.Count; i++)
            {
                //TODO: playerlere yeni fiyatları yolla

                GetSmugglingItemsSucceed(players.Values.ToList()[i]);
            }
        }
    }

    public void RefreshSmugglingItemsPricesInvoke()
    {
        RefreshSmugglingItemsPrices(false);
    }


    public void GetSmugglingItemsSucceed(Player player)
    {
        List<ScriptableItemAndPrice> siap = new List<ScriptableItemAndPrice>();
        siap = smugglingItems;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetSmugglingItemsSucceed, JsonConvert.SerializeObject(siap)));
        
    }

    public void GetSmugglingItemsFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetSmugglingItemsFailed, reason));
        
    }

    public void SellSmugglingItemsSucceed(Player player, int index, int amount)
    {
        ShopItemDatas sid = new ShopItemDatas();
        sid.index = index;
        sid.amount = amount;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SellSmugglingItemsSucceed, JsonConvert.SerializeObject(sid)));
        
    }

    public void SellSmugglingItemsFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SellSmugglingItemsFailed, reason));
        
    }

    public void TrySellSmugglingItemsFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TrySellSmugglingItemsFailed, reason));
        
    }

    public void BuySmugglingItemsSucceed(Player player, int index, int amount)
    {
        ShopItemDatas sid = new ShopItemDatas();
        sid.index = index;
        sid.amount = amount;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.BuySmugglingItemsSucceed, JsonConvert.SerializeObject(sid)));
        
    }

    public void BuySmugglingItemsFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.BuySmugglingItemsFailed, reason));
        
    }

    public void TryBuySmugglingItemsFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryBuySmugglingItemsFailed, reason));
  
    }

    #endregion

    #region Arena Attack
    public void ArenaAttackSucceed(Player player, Player enemy)
    {
        ArenaAttackDatas aad = new ArenaAttackDatas();
        aad.enemyName = enemy.charName;
        aad.curHealth = player.curHealth;
        aad.curMoney = player.money;
        aad.curCrimePoints = player.crimePoints;
        aad.lastArenaAttackTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.lastArenaAttackTime));

        if (player.connectionID != null)
        {
            
            SendData(player.connectionID, new NetworkMessage(NetMessageTypes.ArenaAttackSucceed, JsonConvert.SerializeObject(aad)));
 
        }
    }

    public void ArenaAttackFailed(Player player, Player enemy)
    {
        ArenaAttackDatas aad = new ArenaAttackDatas();
        aad.enemyName = enemy.charName;
        aad.curHealth = player.curHealth;
        aad.curMoney = player.money;
        aad.curCrimePoints = player.crimePoints;
        aad.lastArenaAttackTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.lastArenaAttackTime));

        if (player.connectionID != null)
        {
            
            SendData(player.connectionID, new NetworkMessage(NetMessageTypes.ArenaAttackFailed, JsonConvert.SerializeObject(aad)));
            
        }
    }

    public void TryArenaAttackFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryArenaAttackFailed, reason));
        
    }

    public void GetArenaList(Player player, int userMinusPlus)
    {
        int myRank = (int)((long)ExecuteScalar("SELECT COUNT(*) FROM users AS us WHERE us.crimepoints >= (SELECT crimepoints AS point FROM users AS sb WHERE sb.charName = '" + player.charName + "')"));
        
        List<List<object>> table = ExecuteReader("SELECT charname, crimepoints FROM users ORDER BY crimepoints DESC, charname ASC LIMIT " + Mathf.Max((myRank - userMinusPlus), 0) + "," + userMinusPlus * 2 + "");

        if (table.Count > 0)
        {
            List<ArenaListDatas> alds = new List<ArenaListDatas>();

            for (int i = 0; i < table.Count; i++)
            {
                ArenaListDatas arenaListDatas = new ArenaListDatas();

                List<object> mainrow = table[i];
                arenaListDatas.no = i;
                arenaListDatas.name = (string)mainrow[0];
                arenaListDatas.crimePoints = Convert.ToInt32(mainrow[1]);

                alds.Add(arenaListDatas);
                
            }
            SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetArenaListSucceed, JsonConvert.SerializeObject(alds)));

            
                
        }
        else
        {
            SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetArenaListFailed, "error"));
        }
    }

    #endregion

    #region Chat
       
    public void ProcessChatMessage(Player player, ChatMessage cm)
    {
        string text = cm.message;

        if (!string.IsNullOrWhiteSpace(text))
        {
            // command in the commands list?
            // note: we don't do 'break' so that one message could potentially
            //       be sent to multiple channels (see mmorpg local chat)
            string lastcommand = "";
            if (text.StartsWith(GameManager.instance.whisper.command))
            {
                // whisper
                string[] parsed = ParsePM(GameManager.instance.whisper.command, text);
                string user = parsed[0];
                string msg = parsed[1];
                if (!string.IsNullOrWhiteSpace(user) && !string.IsNullOrWhiteSpace(msg))
                {
                    if (user != name)
                    {
                        lastcommand = GameManager.instance.whisper.command + " " + user + " ";
                        MsgWhisper(player, user, msg);
                    }
                    else print("cant whisper to self");
                }
                else print("invalid whisper format: " + user + "/" + msg);
            }
            else if (!text.StartsWith("/"))
            {
                // local chat is special: it has no command
                lastcommand = "";
                MsgLocal(player, text);
            }
            else if (text.StartsWith(GameManager.instance.guild.command))
            {
                // guild
                string msg = ParseGeneral(GameManager.instance.guild.command, text);
                if (!string.IsNullOrWhiteSpace(msg))
                {
                    lastcommand = GameManager.instance.guild.command + " ";
                    MsgGuild(player, msg);
                }
            }
            // input text should be set to lastcommand
            //return lastcommand;
        }
    }

    void MsgLocal(Player player, string message)
    {
        for(int index = 0; index < players.Count; index++)
        {
            Player messageToPlayer = players.Values.ToList()[index];

            ChatMessage cm = new ChatMessage();
            cm.identifier = GameManager.instance.local.command;
            cm.sender = player.charName;
            cm.message = message;
            cm.vipHash = player.vip.hash;

            
            SendData(messageToPlayer.connectionID, new NetworkMessage(NetMessageTypes.SendChatMessage, JsonConvert.SerializeObject(cm)));                
        }
    }

    void MsgGuild(Player player, string message)
    {
        // send message to all online guild members
        if (!String.IsNullOrEmpty(player.guild.name))
        {
            foreach (Player member in player.guild.onlineMembers)
            {
                ChatMessage cm = new ChatMessage();
                cm.identifier = GameManager.instance.guild.command;
                cm.sender = player.charName;
                cm.message = message;
                cm.vipHash = player.vip.hash;

                
                SendData(member.connectionID, new NetworkMessage(NetMessageTypes.SendChatMessage, JsonConvert.SerializeObject(cm)));                    
            }
        }
    }

    void MsgWhisper(Player player, string whisperTo, string message)
    {
        int index = players.Values.ToList().FindIndex(x => x.charName == whisperTo);
        if (index >= 0)
        {
            Player whisperToPlayer = players.Values.ToList()[index];

            ChatMessage cm = new ChatMessage();
            cm.identifier = GameManager.instance.whisper.command;
            cm.sender = player.charName;
            cm.message = message;
            cm.vipHash = player.vip.hash;

            
            SendData(whisperToPlayer.connectionID, new NetworkMessage(NetMessageTypes.SendChatMessage, JsonConvert.SerializeObject(cm)));  
        }
    }

    // parse a message of form "/command message"
    public string ParseGeneral(string command, string msg)
    {
        // return message without command prefix (if any)
        return msg.StartsWith(command + " ") ? msg.Substring(command.Length + 1) : "";
    }

    public string[] ParsePM(string command, string pm)
    {
        // parse to /w content
        string content = ParseGeneral(command, pm);

        // now split the content in "user msg"
        if (content != "")
        {
            // find the first space that separates the name and the message
            int i = content.IndexOf(" ");
            if (i >= 0)
            {
                string user = content.Substring(0, i);
                string msg = content.Substring(i + 1);
                return new string[] { user, msg };
            }
        }
        return new string[] { "", "" };
    }

    #endregion


    #region Guild

    public void LoadAllGuilds()
    {
        List<GuildMember> members = new List<GuildMember>();

        List<List<object>> table = ExecuteReader("SELECT name FROM guilds");

        foreach (List<object> row in table)
        {
            Guild guild = LoadGuildDatabase((string)row[0]);
            guilds.Add(guild.name, guild);
        }
    }

    // guilds //////////////////////////////////////////////////////////////////
    public bool GuildExists(string guild)
    {
        return ((long)ExecuteScalar("SELECT Count(*) FROM guilds WHERE name=@name", new MySqlParameter("@name", guild))) == 1;
    }

    Guild LoadGuildDatabase(string guildName)
    {
        Guild guild = new Guild();

        // set name
        guild.name = guildName;
        guild.onlineMembers = new List<Player>();

        // load guild info
        List<List<object>> table = ExecuteReader("SELECT notice, appliedPlayers FROM guilds WHERE name=@guild", new MySqlParameter("@guild", guildName));
        if (table.Count == 1)
        {
            List<object> row = table[0];
            guild.notice = (string)row[0];

            string appliedPlayers = (string)row[1];

            guild.appliedPlayers = String.IsNullOrEmpty(appliedPlayers) ? new List<string>() : JsonConvert.DeserializeObject<List<string>>(appliedPlayers);
        }

        // load members list
        List<GuildMember> members = new List<GuildMember>();
        table = ExecuteReader("SELECT charName, crimePoints, guildRank FROM users WHERE guild=@guild", new MySqlParameter("@guild", guildName));
        foreach (List<object> row in table)
        {
            GuildMember member = new GuildMember();
            member.name = (string)row[0];
            member.crimePoints = Convert.ToInt32(row[1]);
            member.rank = (GuildRank)Convert.ToInt32(row[2]);

            // is this player online right now? then use runtime data
            int index = players.Values.ToList().FindIndex(x => x.charName == member.name);
            if (index >= 0)
            {
                member.online = true;
            }
            else
            {
                member.online = false;
            }
            members.Add(member);
        }

        guild.members = members;
        return guild;
    }

    public void SaveGuildDatabase(Guild guild)
    {
        if(GuildExists(guild.name))
        {
            ExecuteNonQuery("UPDATE guilds SET notice = @notice, crimePoints = @crimePoints, money = @money, appliedPlayers =  @appliedPlayers where name = @guild",
                                        new MySqlParameter("@notice", guild.notice),
                                        new MySqlParameter("@crimePoints", guild.crimePoints),
                                        new MySqlParameter("@money", guild.money),
                                        new MySqlParameter("@appliedPlayers", guild.appliedPlayers.Count <= 0 ? "" : JsonConvert.SerializeObject(guild.appliedPlayers)),
                                        new MySqlParameter("@guild", guild.name));
        }
        else
        {    // guild info
            ExecuteNonQuery("INSERT INTO guilds (name, notice, crimePoints, money, appliedPlayers) VALUES (@guild, @notice, @crimePoints, @money, @appliedPlayers)",
                            new MySqlParameter("@guild", guild.name),
                            new MySqlParameter("@notice", guild.notice),
                            new MySqlParameter("@crimePoints", guild.crimePoints),
                            new MySqlParameter("@money", guild.money),
                            new MySqlParameter("@appliedPlayers", guild.appliedPlayers.Count <= 0 ? "" : JsonConvert.SerializeObject(guild.appliedPlayers)));
        }     

        foreach (GuildMember member in guild.members)
        {
            ExecuteNonQuery("UPDATE users SET guild = @guild, guildRank = @guildRank where charName = @charName",
                            new MySqlParameter("@charName", member.name),
                            new MySqlParameter("@guild", guild.name),
                            new MySqlParameter("@guildRank", member.rank));
        }
    }

    public void RemoveGuildDatabase(string guildName)
    {
        ExecuteNonQuery("DELETE FROM guilds WHERE name=@name", new MySqlParameter("@name", guildName));

        ExecuteNonQuery("UPDATE users SET guild = @guild, guildRank = @guildRank where guild = @guild",
                            new MySqlParameter("@guild", ""),
                            new MySqlParameter("@guildRank", GuildRank.Member));
    }
    public void LeaveGuildDatabase(string charName)
    {
        ExecuteNonQuery("UPDATE users SET guild = @guildName, guildRank = @guildRank where charName = @charName",
                            new MySqlParameter("@charName", charName), 
                            new MySqlParameter("@guildName", ""), 
                            new MySqlParameter("@guildRank", 0));
    }
    public void JoinGuildDatabase(string charName, string guildName)
    {
        ExecuteNonQuery("UPDATE users SET guild = @guildName, guildRank = @guildRank where charName = @charName",
                            new MySqlParameter("@charName", charName),
                            new MySqlParameter("@guildName", guildName),
                            new MySqlParameter("@guildRank", 0));
    }
    public void SaveCrimePointMoney(Player player)
    {
        ExecuteNonQuery("UPDATE users SET crimePoints = @crimePoints, money = @money where charName = @charName",
                            new MySqlParameter("@charName", player.charName),
                            new MySqlParameter("@crimePoints", player.crimePoints),
                            new MySqlParameter("@money", player.money));
    }

    

    // copy guild to someone
    public void BroadcastTo(Player player, Guild guild)
    {
        player.guild = guild;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.BroadcastGuildData, JsonConvert.SerializeObject(guild)));

        
            
    }

    // copy guild to all members & save in dictionary
    public void BroadcastChanges(Guild guild)
    {
        foreach (Player member in guild.onlineMembers)
            BroadcastTo(member, guild);

        guilds[guild.name] = guild;
    }

    // create a guild. health, near npc, etc. needs to be checked in the caller.
    public bool CreateGuild(Player player, string guildName)
    {
        // doesn't exist yet?
        if (GameUtils.IsValidGuildName(guildName) &&
            !GuildExists(guildName)) // db check only on server, no Guild.CanCreate function because client has no DB.
        {
            if(player.money >= GameManager.instance.GuildCreationPrice)
            {
                if(player.vip.hash != 0)
                {
                    // create guild and add creator to members list as highest rank
                    Guild guild = new Guild(player, guildName, new List<string>());

                    SaveGuildDatabase(guild);

                    guilds.Add(guild.name, guild);

                    //player.money -= GameManager.instance.GuildCreationPrice;

                    player.ChangeCrimePointsMoney(0, -1 * GameManager.instance.GuildCreationPrice);

                    SyncMoney(player);

                    // broadcast and save
                    BroadcastChanges(guild);
                    Debug.Log(player.charName + " created guild: " + guildName);
                    return true;
                }
                return false;
            }            
        }
        // exists or invalid regex
        return false;
    }

    public void LeaveGuild(Player player)
    {
        string guildName = player.guild.name;

        if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
        {
            Guild guild = guilds[guildName];

            if (guild.CanLeave(player.charName))
            {
                // remove from list
                guild.members = guild.members.Where(m => m.name != player.charName).ToList();
                guild.onlineMembers = guild.onlineMembers.Where(m => m.charName != player.charName).ToList();
                guilds[guildName] = guild;

                // clear for the person that left
                BroadcastTo(player, Guild.Empty);

                LeaveGuildDatabase(player.charName);

                // broadcast and save
                BroadcastChanges(guild);
            }
        }
    }

    public void LeaveGuild(string guildName, string member)
    {
        if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
        {
            Guild guild = guilds[guildName];
            
            if (guild.CanLeave(member))
            {
                guild.members = guild.members.Where(m => m.name != member).ToList();
                guild.onlineMembers = guild.onlineMembers.Where(m => m.charName != member).ToList();
                guilds[guildName] = guild;

                int index = players.Values.ToList().FindIndex(x => x.charName == member);
                if (index >= 0)
                {
                    // clear for the person that left
                    BroadcastTo(players.Values.ToList()[index], Guild.Empty);
                }

                // broadcast and save
                BroadcastChanges(guild);
            }
        }
    }

    public void TerminateGuild(Player player)
    {
        string guildName = player.guild.name;

        if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
        {
            Guild guild = guilds[guildName];

            if (guild.CanTerminate(player.charName))
            {
                // remove guild from database
                RemoveGuildDatabase(guildName);

                if(guilds.ContainsKey(guildName))
                    guilds.Remove(guildName);

                // clear for person that terminated
                BroadcastTo(player, Guild.Empty);
            }
        }
    }

    public bool SetGuildNotice(Player player, string notice)
    {
        string guildName = player.guild.name;

        if(!String.IsNullOrEmpty(guildName) && (guilds.ContainsKey(guildName)))
        {
            Guild guild = guilds[guildName];

            if (guild.CanNotify(player.charName) &&
        notice.Length <= GameManager.instance.GuildNoticeMaxLength)
            {
                // set notice and reset next time
                guild.notice = notice;
                guilds[guildName] = guild;

                SaveGuildDatabase(guild);

                // broadcast and save
                BroadcastChanges(guild);
                Debug.Log(player.charName + " changed guild notice to: " + guild.notice);
                return true;
            }
        }
        return false;
    }

    public void KickFromGuild(Player player, string member)
    {
        string guildName = player.guild.name;

        if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
        {
            Guild guild = guilds[guildName];

            if (guild.CanKick(player.charName, member))
            {
                // reuse Leave function
                LeaveGuild(guildName, member);
                Debug.Log(player.charName + " kicked " + member + " from guild: " + guildName);
            }
        }
    }

    public bool InviteToGuild(Player player, string member)
    {
        string guildName = player.guild.name;

        if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
        {
            Guild guild = guilds[guildName];

            if (guild.CanInvite(player.charName, member))
            {
                //TODO: online değilse bir kopyasını oluştur ve oradan arat
                int index = players.Values.ToList().FindIndex(x => x.charName == member);
                if (index >= 0)
                {
                    Player _member = players.Values.ToList()[index];
                    if (!_member.invitedGuilds.Contains(guildName))
                    {
                        _member.invitedGuilds.Add(guildName);

                        SendData(_member.connectionID, new NetworkMessage(NetMessageTypes.SetInvitedGuilds, JsonConvert.SerializeObject(_member.invitedGuilds)));
                                   
                        Debug.Log(player.charName + " invited " + member + " to guild: " + guildName);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public bool ApplyToGuild(Player player, string guildName)
    {
        if(String.IsNullOrEmpty(player.guild.name))
        {
            if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
            {
                Guild guild = guilds[guildName];

                if(!guild.appliedPlayers.Contains(player.charName))
                {
                    guild.appliedPlayers.Add(player.charName);

                    guilds[guildName] = guild;

                    BroadcastChanges(guild);

                    return true;
                }
            }
        }

        return false;
    }

    public void SetGuildOnline(Player player, bool online)
    {
        string guildName = player.guild.name;

        if(!String.IsNullOrEmpty(guildName))
        {
            if (guilds.ContainsKey(guildName))
            {
                Guild guild = guilds[guildName];

                // member in guild?
                int index = guild.members.FindIndex(m => m.name == player.charName);
                if (index != -1)
                {   
                    if(online)
                    {
                        var onlineMembers = guild.onlineMembers;
                        onlineMembers.Add(player);
                        guild.onlineMembers = onlineMembers;
                    }
                    else
                    {
                        var onlineMembers = guild.onlineMembers;
                        onlineMembers.Remove(player);
                        guild.onlineMembers = onlineMembers;
                    }

                    GuildMember gm = guild.members[index];
                    gm.online = online;
                    guild.members[index] = gm;
                    guilds[guildName] = guild;

                    // broadcast and save
                    BroadcastChanges(guild);
                }
            }
        }
    }

    public void SetGuildCrimePoint(Player player)
    {
        string guildName = player.guild.name;

        if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
        {
            Guild guild = guilds[guildName];

            // member in guild?
            int index = guild.members.FindIndex(m => m.name == player.charName);
            if (index != -1)
            {
                GuildMember gm = guild.members[index];
                gm.crimePoints = player.crimePoints;
                guild.members[index] = gm;
                guilds[guildName] = guild;

                // broadcast and save
                BroadcastChanges(guild);
            }
        }
    }

    public void PromoteMember(Player player, string member)
    {
        string guildName = player.guild.name;

        if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
        {
            Guild guild = guilds[guildName];

            if (guild.CanPromote(player.charName, member))
            {
                int index = guild.members.FindIndex(m => m.name == member);

                var rank = guild.members[index].rank;

                guild.members[index] = new GuildMember(guild.members[index].name, guild.members[index].crimePoints, guild.members[index].online, (GuildRank)(rank+1));

                guilds[guildName] = guild;

                // broadcast and save
                BroadcastChanges(guild);
                Debug.Log(player.charName + " promoted " + member + " in guild: " + guildName);
            }
        }
    }

    public void DemoteMember(Player player, string member)
    {
        string guildName = player.guild.name;

        // guild exists, requester can promote member?
        if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
        {
            Guild guild = guilds[guildName];

            if (guild.CanDemote(player.charName, member))
            {
                int index = guild.members.FindIndex(m => m.name == member);

                var rank = guild.members[index].rank;

                guild.members[index] = new GuildMember(guild.members[index].name, guild.members[index].crimePoints, guild.members[index].online, (GuildRank)(rank - 1));

                guilds[guildName] = guild;

                // broadcast and save
                BroadcastChanges(guild);
                Debug.Log(player.charName + " demoted " + member + " in guild: " + guildName);
            }
        }
    }

    public void GetGuildNamesForApply(Player player, string guildName)
    {
        //guild name varsa onu getir yoksa bir algoritmaya göre guild listesi döndür

        List<string> guildNames = new List<string>();

        if(String.IsNullOrEmpty(guildName))
        {
            // load guild info
            List<List<object>> table = ExecuteReader("SELECT name FROM guilds");
            if (table.Count == 1)
            {
                List<object> row = table[0];

                guildNames.Add((string)row[0]);
            }
        }
        else
        {
            // load guild info
            List<List<object>> table = ExecuteReader("SELECT name FROM guilds WHERE name=@guild", new MySqlParameter("@guild", guildName));
            if (table.Count == 1)
            {
                List<object> row = table[0];

                guildNames.Add((string)row[0]);
            }
        }

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SendGuildNamesForApplyRequest, JsonConvert.SerializeObject(guildNames)));

        
            
    }

    public void AcceptGuildRequest(Player player, string guildName)
    {
        if(player.invitedGuilds.Contains(guildName))
        {
            if (!String.IsNullOrEmpty(guildName) && guilds.ContainsKey(guildName))
            {
                Guild guild = guilds[guildName];

                var members = guild.members;
                members.Add(new GuildMember(player.charName, player.crimePoints, true, GuildRank.Member));
                guild.members = members;

                var onlineMembers = guild.onlineMembers;
                onlineMembers.Add(player);
                guild.onlineMembers = onlineMembers;

                guilds[guildName] = guild;

                player.guild = guilds[guildName];

                BroadcastChanges(guild);

                JoinGuildDatabase(player.charName, guildName);

                player.invitedGuilds.Clear();

                
                SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SetInvitedGuilds, JsonConvert.SerializeObject(player.invitedGuilds)));

                
                    
            }
        }
    }

    public void RejectGuildRequest(Player player, string guildName)
    {
        if (!String.IsNullOrEmpty(guildName) && player.invitedGuilds.Contains(guildName))
        {
            player.invitedGuilds.Remove(guildName);

            
            SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SetInvitedGuilds, JsonConvert.SerializeObject(player.invitedGuilds)));

            
                
        }
    }

    public void AcceptGuildApply(Player player, string appliedName)
    {
        Guild guild = player.guild;

        if (!String.IsNullOrEmpty(guild.name))
        {
            if (guild.appliedPlayers.Contains(appliedName))
            {
                guild.appliedPlayers.Remove(appliedName);

                Player _appliedPlayer;

                int index = players.Values.ToList().FindIndex(x => x.charName == appliedName);
                if (index >= 0)
                {
                    _appliedPlayer = players.Values.ToList()[index];
                }
                else
                {
                    GameObject go = new GameObject();
                    _appliedPlayer = go.AddComponent<Player>();

                    bool temp = UserLoadArena(appliedName, _appliedPlayer);
                }

                var members = guild.members;
                members.Add(new GuildMember(_appliedPlayer.charName, _appliedPlayer.crimePoints, _appliedPlayer.online, GuildRank.Member));
                                

                guild.members = members;

                if (index >= 0)
                {
                    var onlineMembers = guild.onlineMembers;
                    onlineMembers.Add(_appliedPlayer);

                    guild.onlineMembers = onlineMembers;
                }
                else
                {
                    Destroy(_appliedPlayer.gameObject);
                }

                JoinGuildDatabase(appliedName, player.guild.name);

                guilds[guild.name] = guild;

                BroadcastChanges(guild);
            }
        }
    }
    public void RejectGuildApply(Player player, string appliedName)
    {
        Guild guild = player.guild;

        if(!String.IsNullOrEmpty(guild.name))
        {
            if(guild.appliedPlayers.Contains(appliedName))
            {
                guild.appliedPlayers.Remove(appliedName);

                guilds[guild.name] = guild;

                BroadcastChanges(guild);
            }
        }
    }

    #endregion



    #region Mail

    public List<Mail> GetPlayerMails(Player player)
    {
        List<Mail> mails = new List<Mail>();
        List<List<object>> table = ExecuteReader("SELECT id, sender, getter, subject, message, items, deleted FROM mails WHERE getter=@getter",
            new MySqlParameter("@getter", player.charName));

        foreach (List<object> row in table)
        {
            Mail mail = new Mail();

            mail.id = Convert.ToInt32(row[0]);
            mail.sender = (string)row[1];
            mail.getter = (string)row[2];
            mail.subject = (string)row[3];
            mail.message = (string)row[4];
            mail.items = JsonConvert.DeserializeObject<List<ScriptableItemAndAmount>>((string)row[5]);
            mail.deleted = Convert.ToInt32(row[6]) == 0 ? false : true;

            mails.Add(mail);
        }

        return mails;
    }

    public void SaveMail(Mail mail)
    {
        ExecuteNonQuery("INSERT INTO mails (sender, getter, subject, message, items, deleted) VALUES (@sender, @getter, @subject, @message, @items, @deleted)", 
            new MySqlParameter("@sender", mail.sender), 
            new MySqlParameter("@getter", mail.getter),
            new MySqlParameter("@subject", mail.subject),
            new MySqlParameter("@message", mail.message),
            new MySqlParameter("@items", mail.items.Count == 0 ? "" : JsonConvert.SerializeObject(mail.items)), 
            new MySqlParameter("@deleted", mail.deleted ? 1 : 0));

    }

    public void SyncMail(Player player)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.SyncMails, player.mails.Count == 0 ? "" : JsonConvert.SerializeObject(player.mails)));

        
            
    }

    public void GetItemsFromMailSucceed(Player player)
    {
        MailInventory mi = new MailInventory();
        mi.inventory = player.inventory;
        mi.mails = player.mails;

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetItemsFromMailSucceed, JsonConvert.SerializeObject(mi)));

        
            
    }

    public void GetItemsFromMailFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetItemsFromMailFailed, reason));

        
            
    }

    #endregion

    public void StealMoney(Player player, string enemyName)
    {
        if (player.bJailed)
        {
            TryArenaAttackFailed(player, "Jailed");
            return;
        }

        if ((player.lastStealMoneyTime - GameManager.instance.ServerTime).TotalSeconds > 0)
        {
            TryArenaAttackFailed(player, "Needtowaitforstealmoney");
            return;
        }

        bool bOnline = false;
        bool tempLoad = false;
        Player enemy = null;
        
        int index = players.Values.ToList().FindIndex(x => x.charName == enemyName);
        if (index >= 0)
        {
            enemy = players.Values.ToList()[index];
            bOnline = true;
        }
        else
        {
            GameObject go = new GameObject();
            enemy = go.AddComponent<Player>();

            tempLoad = UserLoadArena(enemyName, enemy);
            bOnline = false;
        }
        
        if(!bOnline && !tempLoad)
        {
            TryStealMoneyFailed(player, "PlayerNotFound");
            return;
        }

        if(!string.IsNullOrEmpty(player.guild.name) && player.guild.name == enemy.guild.name)
        {
            TryStealMoneyFailed(player, "cantstealmoneyfromyourguild");
            return;
        }

        float luckPlayer = player.intelligence / enemy.intelligence;
        float luckEnemy = enemy.intelligence / player.intelligence;

        float playerAttackBonus = player.strength / enemy.strength; playerAttackBonus = Mathf.Max(playerAttackBonus, 1);
        float enemyAttackBonus = enemy.strength / player.strength; enemyAttackBonus = Mathf.Max(enemyAttackBonus, 1);

        float playerStealModifier = (player.crimePoints * playerAttackBonus) * luckPlayer ; playerStealModifier = Mathf.Max(playerStealModifier, 1);
        float enemyStealModifier = (enemy.crimePoints * enemyAttackBonus) * luckEnemy ; enemyStealModifier = Mathf.Max(enemyStealModifier, 1);

        float randomSteal = UnityEngine.Random.Range(0, playerStealModifier + enemyStealModifier);

        Debug.Log(luckPlayer + " " + luckEnemy + " " + playerAttackBonus + " " + enemyAttackBonus + " " + playerStealModifier + " " + enemyStealModifier + " " + randomSteal);

        float vipBonus = player.vip.hash != 0 ? player.vip.data.stealMoneyAttackTimeDecreaser : 0;

        player.lastStealMoneyTime = GameManager.instance.ServerTime + new TimeSpan(0, 0, Mathf.FloorToInt(GameManager.instance.stealMoneyAttackTime * (1 - vipBonus / 100)));

        if (randomSteal <= playerStealModifier)
        {
            Debug.LogError("Player Win");

            int money = Mathf.FloorToInt(enemy.money / 2);

            player.ChangeMoney(money);
            //SyncMoney(player);

            enemy.ChangeMoney(-1 * money);

            Mail enemyMail = new Mail(-1, "{system}", enemy.charName, "{stealMoney}", player.charName + " " + "{isstolemoneyfromyou}" + " - " + money, new List<ScriptableItemAndAmount>(), false);

            SaveMail(enemyMail);

            if (enemy.online)
            {
                SyncMoney(enemy);

                enemy.mails = GetPlayerMails(enemy);
                SyncMail(enemy);
            }

            totalPvPSucceed++;

            StealMoneySucceed(player, enemy.charName);
        }
        else
        {
            Debug.LogError("Enemy Win");

            Mail enemyMail = new Mail(-1, "{system}", enemy.charName, "{stealMoney}", player.charName + " " + "{istriedtostealmoneybutyoucatchhim}", new List<ScriptableItemAndAmount>(), false);

            SaveMail(enemyMail);

            if(enemy.online)
            {
                enemy.mails = GetPlayerMails(enemy);
                SyncMail(enemy);
            }

            totalPvPFailed++;

            StealMoneyFailed(player, "");
        }

        if(index >= 0)
        {

        }
        else
        {
            Destroy(enemy.gameObject);
        }
    }


    public void StealMoneySucceed(Player player, string enemyName)
    {
        StealMoneyDatas smd = new StealMoneyDatas();
        smd.curMoney = player.money;
        smd.enemyName = enemyName;
        smd.lastStealMoneyTime = JsonConvert.SerializeObject(new SYSTEMTIME(player.lastStealMoneyTime));

        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.StealMoneySucceed, JsonConvert.SerializeObject(smd)));

        
            
    }

    public void StealMoneyFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.StealMoneyFailed, reason));

        
            
    }
    public void TryStealMoneyFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.TryStealMoneyFailed, reason));

        
            
    }

    public void GetStealMoneyPlayersList(Player player, int userMinusPlus)
    {
        int myRank = (int)((long)ExecuteScalar("SELECT COUNT(*) FROM users AS us WHERE us.crimepoints >= (SELECT crimepoints AS point FROM users AS sb WHERE sb.charName = '" + player.charName + "')"));

        List<List<object>> table = ExecuteReader("SELECT charname FROM users ORDER BY crimepoints DESC, charname ASC LIMIT " + Mathf.Max((myRank - userMinusPlus), 0) + "," + userMinusPlus * 2 + "");

        if (table.Count > 0)
        {
            List<string> playerList = new List<string>();

            for (int i = 0; i < table.Count; i++)
            {
                List<object> mainrow = table[i];
                playerList.Add((string)mainrow[0]);

            }

            GetStealMoneyPlayersListSucceed(player, playerList);
        }
        else
        {
            GetStealMoneyPlayersListFailed(player, "error");
        }
    }

    public void GetStealMoneyPlayersListSucceed(Player player, List<string> playersList)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetStealMoneyPlayersListSucceed, 
            (playersList == null || playersList.Count == 0) ? "" : JsonConvert.SerializeObject(playersList)));

        
            
    }

    public void GetStealMoneyPlayersListFailed(Player player, string reason)
    {
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetStealMoneyPlayersListFailed, reason));

        
            
    }

    public void SaveInAppPurchaseToDatabase(Player player, InAppPurchaseDatas _iapData)
    {
        ExecuteNonQuery("INSERT INTO orders (transactionID, receipt, definitionId, purchaseTime, playerID, playerName, status) VALUES (@transactionID, @receipt, @definitionId, @purchaseTime, @playerID, @playerName, @status)",
                            new MySqlParameter("@transactionID", _iapData.transactionID),
                            new MySqlParameter("@receipt", _iapData.receipt),
                            new MySqlParameter("@definitionId", _iapData.definitionId),
                            new MySqlParameter("@purchaseTime", DateTime.UtcNow.ToString()),
                            new MySqlParameter("@playerID", player.playerID),
                            new MySqlParameter("@playerName", player.charName),
                            new MySqlParameter("@status", 1)
                            );
    }

    public void ProcessInAppPurchase(Player player, InAppPurchaseDatas _iapData)
    {
        SaveInAppPurchaseToDatabase(player, _iapData);        
        _iapData.purchaseTime = GameManager.instance.ServerTime;

        ScriptableVIP vip = new ScriptableVIP();

        switch (_iapData.definitionId)
        {
            case "VIP1":
                vip = ScriptableVIP.dict["Vip1".GetStableHashCode()];
                vip.Apply(player);
                break;
            case "VIP2":
                vip = ScriptableVIP.dict["Vip2".GetStableHashCode()];
                vip.Apply(player);
                break;
            case "VIP3":
                vip = ScriptableVIP.dict["Vip3".GetStableHashCode()];
                vip.Apply(player);
                break;
            case "VIP4":
                vip = ScriptableVIP.dict["Vip4".GetStableHashCode()];
                vip.Apply(player);
                break;

            default:
                break;
        }
        
        SendData(player.connectionID, new NetworkMessage(NetMessageTypes.InAppPurchaseSucceed, JsonConvert.SerializeObject(_iapData)));
        
    }
    public void ProcessFreeEnergyRequest(Player player)
    {
        if (player.GetEnergyTimeRemaining() <= 0)
        {
            //Succeed
            player.curHealth += GameManager.instance.freeEnergy;
            player.curHealth = Mathf.Min(player.health, player.curHealth);

            player.lastEnergyCollected = GameManager.instance.ServerTime;

            
            SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetFreeEnergySucceed, player.curHealth.ToString()));
            
        }
        else
        {
            //Failed
            
            SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetFreeEnergyFailed, ""));
 
        }
    }
    public void ProcessFreeMoneyRequest(Player player)
    {
        if (true/*player.GetEnergyTimeRemaining() <= 0*/)
        {
            //Succeed

            int earnedMoney = player.lastEarnedMoney * GameManager.instance.freeMoneyRatio;

            player.ChangeMoney(earnedMoney);

            
            SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetFreeMoneySucceed, (earnedMoney).ToString()));
            
        }
        else
        {
            //Failed
            
            SendData(player.connectionID, new NetworkMessage(NetMessageTypes.GetFreeMoneyFailed, ""));
            
        }
    }
}

[System.Serializable]
public class SmugglingItemDatas
{
    public SmugglingItem item;
    public int priceMin;
    public int priceMax;
}