﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;
using I2.Loc;
using Telepathy;
using System.Net;
using System.Net.Sockets;
using UnityEngine.Networking;

public class MafiasCrimeWorldClient : MonoBehaviour
{
    public static MafiasCrimeWorldClient instance;

    public int version = 1;

    private Client _netClient;

    public string ipAdress = "127.0.0.1";
    public string ipAdressV6 = "2a05:d018:64e:9b00:1004:be60:a435:6ffd";
    public int port = 50900;
    
    public bool isConnected = false;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);

        Application.runInBackground = true;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.targetFrameRate = 30;
                  
        if (PlayerPrefs.HasKey("CurrentLanguageCode"))
        {
            LocalizationManager.CurrentLanguageCode = PlayerPrefs.GetString("CurrentLanguageCode");
        }
        else
        {
            LocalizationManager.CurrentLanguageCode = GameUtils.Get2LetterISOCodeFromSystemLanguage();
            PlayerPrefs.SetString("CurrentLanguageCode", GameUtils.Get2LetterISOCodeFromSystemLanguage());
        }

        foreach (string s in LocalizationManager.GetAllLanguages())
        Debug.Log(s);


        _netClient = new Client();

        Telepathy.Logger.Log = Debug.Log;
        Telepathy.Logger.LogWarning = Debug.LogWarning;
        Telepathy.Logger.LogError = Debug.LogError;
    }

    IEnumerator TryToConnectIPv6()
    {
        UnityWebRequest www = UnityWebRequest.Get("https://ipv6.google.com");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Debug.LogError("IPv4");
            ConnectToServer(ipAdress);
        }
        else
        {
            // Show results as text
            Debug.LogError("IPv6");
            ConnectToServer(ipAdressV6);
        }
    }

    // Use this for initialization
    void Start()
    {
        //ConnectToServer();

        CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Info"),
            LocalizationManager.GetTermTranslation("connect"),
            "IPv4", () => ConnectWithIPv4(),
            "IPv6", () => ConnectWithIPv6());
    }

    /*public void ConnectToServer()
    {
        StartCoroutine(TryToConnectIPv6());
    }*/

    public void ChangeTimeoutTime(int timeoutTime)
    {
        _netClient.SendTimeout = timeoutTime;
    }

    public void ConnectWithIPv4()
    {
        ConnectToServer(ipAdress);
    }

    public void ConnectWithIPv6()
    {
        ConnectToServer(ipAdressV6);
    }

    public void ConnectToServer(string _ipAdress)
    {
        try
        {
            _netClient.Connect(_ipAdress, port);
/* 

#if UNITY_EDITOR
            _netClient.Connect(ipAdress, port, "mafias_crime_world");
#elif UNITY_ANDROID
            _netClient.Connect(ipAdress, port, "mafias_crime_world");
#elif UNITY_IOS
            _netClient.Connect(ipAdress, port, "mafias_crime_world");
#else
            _netClient.Connect(ipAdress, port, "mafias_crime_world");
#endif

*/

            Debug.LogError("Client Started...");
        }
        catch (Exception e)
        {
            Debug.LogError("Client Not Started...");
            Debug.LogError(e.ToString());
        }
    }

    void Update()
    {
        if (_netClient != null && _netClient.Connected)
        {
            // show all new messages
            Telepathy.Message msg;
            while (_netClient.GetNextMessage(out msg))
            {
                switch (msg.eventType)
                {
                    case Telepathy.EventType.Connected:
                        Debug.Log("Connected");
                        isConnected = true;

                        CGUIPopUp.instance.Hide();

                        UIManager.instance.CloseAllPanels();
                        UIManager.instance.OpenLoginRegisterPanel(true);
                        break;

                    case Telepathy.EventType.Data:
                        Debug.Log("Data: " + BitConverter.ToString(msg.data));

                        OnNetworkReceive(msg);

                        break;

                    case Telepathy.EventType.Disconnected:
                        Debug.Log("Disconnected");

                        isConnected = false;

                        if (Player.instance != null)
                        {
                            GameObject.Destroy(Player.instance.gameObject);
                        }

                        UIManager.instance.CloseAllPanels();

                        /*CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Disconnected"),
                            LocalizationManager.GetTermTranslation("Youhavebeendisconnected"),
                            LocalizationManager.GetTermTranslation("ReLogin"), () => ConnectToServer(),
                            LocalizationManager.GetTermTranslation("EXIT"), () => Application.Quit());*/

                        CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Disconnected"),
                            LocalizationManager.GetTermTranslation("reconnect"),
                            "IPv4", () => ConnectWithIPv4(),
                            "IPv6", () => ConnectWithIPv6());

                        break;
                }
            }
        }
    }

    void OnDestroy()
    {
        if (_netClient != null)
            _netClient.Disconnect();
    }

    public void OnNetworkReceive(Message msg)
    {
        NetworkMessage netMessage = new NetworkMessage();
        netMessage.ByteArrayToObject(msg.data);

        switch (netMessage.netMessageType)
        {
            case NetMessageTypes.RegisterSucceed:
                {
                    Debug.LogError("Register Succeed");

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation("RegisterSucceed"),
                    "OK", () => CGUIPopUp.instance.Hide());

                    GameObject go = new GameObject();
                    go.AddComponent<Player>();
                    Player player = go.GetComponent<Player>();

                    Player.instance = player;

                    PlayerDatas pd = JsonConvert.DeserializeObject<PlayerDatas>(netMessage.message);

                    pd.CopyPlayer(player);

                    UIManager.instance.OpenLoginRegisterPanel(false);

                    if (String.IsNullOrEmpty(pd.charName))
                    {
                        //Karakter ismi oluşturma ekranı göster
                        UIManager.instance.OpenSetCharacterNamePanel(true);
                    }

                }

                break;

            case NetMessageTypes.RegisterFailed:

                {
                    Debug.LogError("Register Failed");

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation("RegisterFailed") + " - " + LocalizationManager.GetTermTranslation(reason),
                    "OK", () => CGUIPopUp.instance.Hide());
                }

                break;

            case NetMessageTypes.LoginSucceed:
                {
                    //Debug.LogError("Login Succeed");

                    GameObject go = new GameObject();
                    go.AddComponent<Player>();
                    Player player = go.GetComponent<Player>();

                    Player.instance = player;

                    PlayerDatas pd = JsonConvert.DeserializeObject<PlayerDatas>(netMessage.message);

                    pd.CopyPlayer(player);

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation("LoginSucceed"),
                    "OK", () => CGUIPopUp.instance.Hide());

                    if (String.IsNullOrEmpty(pd.charName))
                    {
                        //Karakter ismi oluşturma ekranı göster
                        UIManager.instance.OpenSetCharacterNamePanel(true);
                    }
                    else
                    {
                        UIManager.instance.OpenLoginRegisterPanel(false);
                        UIManager.instance.OpenTopBarPanel(true);
                        UIManager.instance.OpenMainMenuPanel(true);
                    }
                }
                break;

            case NetMessageTypes.LoginFailed:
                {
                    Debug.LogError("Login Failed");

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation("LoginFailed") + " - " + LocalizationManager.GetTermTranslation(reason),
                    "OK", () => CGUIPopUp.instance.Hide());
                }
                break;

            case NetMessageTypes.SetCharnameSucceed:
                {
                    Debug.LogError("Set Charname Succeed");

                    string charname = netMessage.message;

                    Player.instance.charName = charname;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation("SetCharnameSucceed"),
                    "OK", () => {
                        CGUIPopUp.instance.Hide(); UIManager.instance.OpenTopBarPanel(true); UIManager.instance.OpenMainMenuPanel(true);

                        CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation("gamefirstMessage"),
                        "OK", () => { CGUIPopUp.instance.Hide(); UIManager.instance.OpenShopMenuPanel(true); });
                    });


                }
                break;

            case NetMessageTypes.SetCharnameFailed:
                {
                    Debug.LogError("Set Charname Failed");

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(reason),
                    "OK", () => { CGUIPopUp.instance.Hide(); UIManager.instance.OpenSetCharacterNamePanel(true); });
                }
                break;

            case NetMessageTypes.CommitCrimeSucceed:
                {
                    HealthEnemyMoneyCrimePointsAttackTimeJailTime hemcpat = JsonConvert.DeserializeObject<HealthEnemyMoneyCrimePointsAttackTimeJailTime>(netMessage.message);

                    HealthEnemyMoneyCrimePointsAttackTimeJailTime earnedValues = new HealthEnemyMoneyCrimePointsAttackTimeJailTime();

                    earnedValues.curHealth = hemcpat.curHealth - Player.instance.curHealth;
                    earnedValues.curMoney = hemcpat.curMoney - Player.instance.money;
                    earnedValues.curCrimePoints = hemcpat.curCrimePoints - Player.instance.crimePoints;

                    Player.instance.SetCurHealth(hemcpat.curHealth);
                    Player.instance.SetMoney(hemcpat.curMoney);
                    Player.instance.SetCrimePoints(hemcpat.curCrimePoints);

                    Player.instance.CheckGrade();

                    Player.instance.totalPvEKill++;

                    Player.instance.lastAttackTime = JsonConvert.DeserializeObject<SYSTEMTIME>(hemcpat.lastAttackTime).Get();
                    Player.instance.SetJailFinishTime(JsonConvert.DeserializeObject<SYSTEMTIME>(hemcpat.jailFinishTime).Get());

                    CGUIPopUp.instance.Hide();

#if UNITY_EDITOR && !UNITY_ANDROID && !UNITY_IOS
                    CGUIPopUpWinLose.instance.ShowStatus(LocalizationManager.GetTermTranslation("WIN"), LocalizationManager.GetTermTranslation("Commitcrimesucceed"),
                         earnedValues.curCrimePoints, earnedValues.curMoney, earnedValues.curHealth, true, null, CGUIPopUpWinLoseTypes.Win);
#elif UNITY_ANDROID
                                    CGUIPopUpWinLose.instance.ShowInfo(LocalizationManager.GetTermTranslation("WIN"), LocalizationManager.GetTermTranslation("Commitcrimesucceed"),
                                        LocalizationManager.GetTermTranslation("ShowAdsAndEarn2x"), () => { AdvertisementService.instance.ShowRewardedAds(rewardedAdsTypes.freeMoney); CGUIPopUpWinLose.instance.Hide();  },
                                        earnedValues.curCrimePoints, earnedValues.curMoney, earnedValues.curHealth, true, null, CGUIPopUpWinLoseTypes.Win);
#elif UNITY_IOS
                                    CGUIPopUpWinLose.instance.ShowInfo(LocalizationManager.GetTermTranslation("WIN"), LocalizationManager.GetTermTranslation("Commitcrimesucceed"),
                                        LocalizationManager.GetTermTranslation("ShowAdsAndEarn2x"), () => { AdvertisementService.instance.ShowRewardedAds(rewardedAdsTypes.freeMoney); CGUIPopUpWinLose.instance.Hide();  },
                                        earnedValues.curCrimePoints, earnedValues.curMoney, earnedValues.curHealth, true, null, CGUIPopUpWinLoseTypes.Win);
#else
            
                                    CGUIPopUpWinLose.instance.ShowStatus(LocalizationManager.GetTermTranslation("WIN"), LocalizationManager.GetTermTranslation("Commitcrimesucceed"),                                        
                                        earnedValues.curCrimePoints, earnedValues.curMoney, earnedValues.curHealth, true, null, CGUIPopUpWinLoseTypes.Win);
#endif


                    //Başarılı ekranı aç,
                    //anim oynat,
                }
                break;

            case NetMessageTypes.CommitCrimeFailed:
                {
                    CGUIPopUp.instance.Hide();

                    EnemyHashHealthJail ehj = JsonConvert.DeserializeObject<EnemyHashHealthJail>(netMessage.message);

                    int enemyHash = ehj.enemyHash;
                    int health = ehj.health;
                    string jailFinishTime = ehj.jailFinishTime;

                    Player.instance.SetCurHealth(health);

                    Player.instance.totalPvEDeath++;

                    Player.instance.lastAttackTime = JsonConvert.DeserializeObject<SYSTEMTIME>(ehj.lastAttackTime).Get();

                    if (string.IsNullOrEmpty(jailFinishTime))
                    {
                        CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(ScriptableEnemy.dict[enemyHash].name.ToString()) + " " + LocalizationManager.GetTermTranslation("Commitcrimefailed"),
                    "OK", () => { CGUIPopUp.instance.Hide(); });
                    }
                    else
                    {
                        Player.instance.SetJailFinishTime(JsonConvert.DeserializeObject<SYSTEMTIME>(ehj.jailFinishTime).Get());

                        CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation("Jailed"),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                    }

                    //başarısız ekranı aç
                    //anim oynat
                    //kaybettiğini göster

                    //düşman ismi'ni yenemedin

                }
                break;

            case NetMessageTypes.TryCommitCrimeFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.SyncHealth:
                {
                    int curHealth = Convert.ToInt32(netMessage.message);

                    if (Player.instance != null)
                    {
                        Player.instance.SetCurHealth(curHealth);

                        Player.instance.healthRegenerationTime = GameManager.instance.regenerationMinute * 60;
                    }
                }
                break;

            case NetMessageTypes.SyncMoney:
                {
                    int curMoney = Convert.ToInt32(netMessage.message);

                    if (Player.instance != null)
                    {
                        Player.instance.SetMoney(curMoney);
                    }
                }
                break;

            case NetMessageTypes.BuyRealEstateSucceed:
                {
                    PlayerRealEstateMoney prem = JsonConvert.DeserializeObject<PlayerRealEstateMoney>(netMessage.message);

                    Player.instance.SetMoney(prem.money);
                    Player.instance.AddRealEstate(new RealEstate(ScriptableRealEstate.dict[prem.realEstateHash]), 1);

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.BuyRealEstateFailed:
                {
                    CGUIPopUp.instance.Hide();

                    int realEstateHash = Convert.ToInt32(netMessage.message);

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(realEstateHash.ToString()),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.TryBuyRealEstateFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.GetIncomeRealEstateSucceed:
                {
                    PlayerRealEstateMoney prem = JsonConvert.DeserializeObject<PlayerRealEstateMoney>(netMessage.message);

                    Player.instance.SetMoney(prem.money);

                    int realEstatesIndex = Player.instance.GetRealEstateIndexByName(ScriptableRealEstate.dict[prem.realEstateHash].name);

                    if (realEstatesIndex != -1)
                    {
                        RealEstate realEstatePlayer = Player.instance.realEstates[realEstatesIndex];
                        realEstatePlayer.lastCollectTime = GameManager.instance.ServerTime;
                        Player.instance.realEstates[realEstatesIndex] = realEstatePlayer;
                    }
                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.GetIncomeRealEstateFailed:
                {
                    CGUIPopUp.instance.Hide();

                    int realEstateHash = Convert.ToInt32(netMessage.message);

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(realEstateHash.ToString()),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.TryGetIncomeRealEstateFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;


            case NetMessageTypes.BuyManagementSucceed:
                {
                    PlayerManagementMoney prem = JsonConvert.DeserializeObject<PlayerManagementMoney>(netMessage.message);

                    Player.instance.SetMoney(prem.money);
                    Player.instance.AddManagement(new Management(ScriptableManagement.dict[prem.managementHash]), 1);

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.BuyManagementFailed:
                {
                    CGUIPopUp.instance.Hide();

                    int realEstateHash = Convert.ToInt32(netMessage.message);

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(realEstateHash.ToString()),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.TryBuyManagementFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.GetIncomeManagementSucceed:
                {
                    PlayerManagementMoney prem = JsonConvert.DeserializeObject<PlayerManagementMoney>(netMessage.message);

                    Player.instance.SetMoney(prem.money);

                    int managementsIndex = Player.instance.GetManagementIndexByName(ScriptableManagement.dict[prem.managementHash].name);

                    if (managementsIndex != -1)
                    {
                        Management managementPlayer = Player.instance.managements[managementsIndex];
                        managementPlayer.lastCollectTime = GameManager.instance.ServerTime;
                        Player.instance.managements[managementsIndex] = managementPlayer;
                    }
                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.GetIncomeManagementFailed:
                {
                    CGUIPopUp.instance.Hide();

                    int managementHash = Convert.ToInt32(netMessage.message);

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(managementHash.ToString()),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.TryGetIncomeManagementFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.TrainSucceed:
                {
                    PlayerHealthMoneyStrengthIntelligence phmsi = JsonConvert.DeserializeObject<PlayerHealthMoneyStrengthIntelligence>(netMessage.message);

                    if (UIManager.instance != null)
                    {
                        if (phmsi.strength != Player.instance._strength)
                        {
                            UIManager.instance.AnimateSTR();
                        }
                        else if (phmsi.intelligence != Player.instance._intelligence)
                        {
                            UIManager.instance.AnimateINT();
                        }
                    }

                    Player.instance.SetCurHealth(phmsi.health);
                    Player.instance.SetMoney(phmsi.money);
                    Player.instance._strength = phmsi.strength;
                    Player.instance._intelligence = phmsi.intelligence;

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.TrainFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(reason),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.TryTrainFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.GiveABribeSucceed:
                {
                    int money = Convert.ToInt32(netMessage.message);

                    Player.instance.SetMoney(money);

                    Player.instance.SetJailFinishTime(GameManager.instance.ServerTime);

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.GiveABribeFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(reason),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.TryGiveABribeFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.EscapeFromPrisonSucceed:
                {
                    Player.instance.SetJailFinishTime(GameManager.instance.ServerTime);

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation("EscapeFromPrisonSucceed"),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.EscapeFromPrisonFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation("EscapeFromPrisonFailed"),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.TryEscapeFromPrisonFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.MoneyBankTransferSucceed:
                {
                    PlayerMoneyBankMoney pmbm = JsonConvert.DeserializeObject<PlayerMoneyBankMoney>(netMessage.message);

                    Player.instance.SetMoney(pmbm.money);
                    Player.instance.moneyBank = pmbm.bankMoney;

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.MoneyBankTransferFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(reason),
                        "OK", () =>
                        {
                            CGUIPopUp.instance.Hide();
                        },
                        "VIP", () =>
                        {
                            CGUIPopUp.instance.Hide();
                            UIManager.instance.CloseMenuPanels();
                            UIManager.instance.OpenVIPMenuPanel(true);
                        });
                }
                break;

            case NetMessageTypes.TryMoneyBankTransferFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    Player player = Player.instance;

                    float vipBonus = player.vip.hash != 0 ? player.vip.data.maxMoneyBankIncreaser : 0;
                    int totalMoney = player.money + player.moneyBank;
                    int maxBankMoney = Mathf.FloorToInt(totalMoney * (50 + vipBonus) / 100);

                    CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(reason).Replace("{maxmoney}", (maxBankMoney - player.moneyBank).ToString()),
                        "OK", () =>
                        {
                            CGUIPopUp.instance.Hide();
                        },
                        "VIP", () =>
                        {
                            CGUIPopUp.instance.Hide();
                            UIManager.instance.CloseMenuPanels();
                            UIManager.instance.OpenVIPMenuPanel(true);
                        });
                }
                break;



            case NetMessageTypes.GetAnnouncementsSucceed:
                {
                    List<AnnouncementDatas> ad = JsonConvert.DeserializeObject<List<AnnouncementDatas>>(netMessage.message);

                    Player.instance.announcements = ad;
                    Player.instance.RefreshAnnouncements();
                }
                break;

            case NetMessageTypes.GetAnnouncementsFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(reason),
                        "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.DoStorySucceed:
                {
                    HealthEnemyMoneyCrimePointsLastSucceedStoryNo hemcplssn = JsonConvert.DeserializeObject<HealthEnemyMoneyCrimePointsLastSucceedStoryNo>(netMessage.message);

                    HealthEnemyMoneyCrimePointsLastSucceedStoryNo earnedValues = new HealthEnemyMoneyCrimePointsLastSucceedStoryNo();
                    earnedValues.curHealth = hemcplssn.curHealth - Player.instance.curHealth;
                    earnedValues.curMoney = hemcplssn.curMoney - Player.instance.money;
                    earnedValues.curCrimePoints = hemcplssn.curCrimePoints - Player.instance.crimePoints;

                    Player.instance.SetCurHealth(hemcplssn.curHealth);
                    Player.instance.SetMoney(hemcplssn.curMoney);
                    Player.instance.SetCrimePoints(hemcplssn.curCrimePoints);
                    Player.instance.lastSucceedStoryNo = hemcplssn.lastSucceedStoryNo;

                    Player.instance.CheckGrade();

                    Player.instance.totalPvEKill++;

                    CGUIPopUp.instance.Hide();

#if UNITY_EDITOR && !UNITY_ANDROID && !UNITY_IOS
                    CGUIPopUpWinLose.instance.ShowStatus(LocalizationManager.GetTermTranslation("WIN"), LocalizationManager.GetTermTranslation("Dostorysucceed"),
                         earnedValues.curCrimePoints, earnedValues.curMoney, earnedValues.curHealth, true, null, CGUIPopUpWinLoseTypes.Win);
#elif UNITY_ANDROID
                                    CGUIPopUpWinLose.instance.ShowInfo(LocalizationManager.GetTermTranslation("WIN"), LocalizationManager.GetTermTranslation("Dostorysucceed"),
                                        LocalizationManager.GetTermTranslation("ShowAdsAndEarn2x"), () => { AdvertisementService.instance.ShowRewardedAds(rewardedAdsTypes.freeMoney);  CGUIPopUpWinLose.instance.Hide(); },
                                        earnedValues.curCrimePoints, earnedValues.curMoney, earnedValues.curHealth, true, null, CGUIPopUpWinLoseTypes.Win);
#elif UNITY_IOS
                                    CGUIPopUpWinLose.instance.ShowInfo(LocalizationManager.GetTermTranslation("WIN"), LocalizationManager.GetTermTranslation("Dostorysucceed"),
                                        LocalizationManager.GetTermTranslation("ShowAdsAndEarn2x"), () => { AdvertisementService.instance.ShowRewardedAds(rewardedAdsTypes.freeMoney);  CGUIPopUpWinLose.instance.Hide(); },
                                        earnedValues.curCrimePoints, earnedValues.curMoney, earnedValues.curHealth, true, null, CGUIPopUpWinLoseTypes.Win);
#else
            
                                    CGUIPopUpWinLose.instance.ShowStatus(LocalizationManager.GetTermTranslation("WIN"), LocalizationManager.GetTermTranslation("Dostorysucceed"),                                        
                                        earnedValues.curCrimePoints, earnedValues.curMoney, earnedValues.curHealth, true, null, CGUIPopUpWinLoseTypes.Win);
#endif

                }
                break;

            case NetMessageTypes.DoStoryFailed:
                {
                    CGUIPopUp.instance.Hide();

                    EnemyHashHealthJail ehj = JsonConvert.DeserializeObject<EnemyHashHealthJail>(netMessage.message);

                    int enemyHash = ehj.enemyHash;
                    int health = ehj.health;

                    Player.instance.SetCurHealth(health);
                    Player.instance.totalPvEDeath++;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                        LocalizationManager.GetTermTranslation(ScriptableStory.dict[enemyHash].name.ToString()) + " " + LocalizationManager.GetTermTranslation("Dostoryfailed"),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.TryDoStoryFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.SwapItemSucceed:
                {
                    CGUIPopUp.instance.Hide();

                    SwapItemDatas sid = JsonConvert.DeserializeObject<SwapItemDatas>(netMessage.message);

                    Player.instance.SwapInventoryEquip(sid.invNo, sid.eqNo);

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.SwapItemFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.TrySwapItemFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.BuyShopItemSucceed:
                {
                    CGUIPopUp.instance.Hide();

                    ShopItemDatas sid = JsonConvert.DeserializeObject<ShopItemDatas>(netMessage.message);

                    Player.instance.BuyShopItem(sid.eqType, sid.index, sid.amount);

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.BuyShopItemFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(reason),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.TryBuyShopItemFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.SellShopItemSucceed:
                {
                    CGUIPopUp.instance.Hide();

                    ShopItemDatas sid = JsonConvert.DeserializeObject<ShopItemDatas>(netMessage.message);

                    Player.instance.SellShopItem(sid.index, sid.amount);

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.SellShopItemFailed:
                {
                    CGUIPopUp.instance.Hide();

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(""),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.TrySellShopItemFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;


            case NetMessageTypes.GetRankCrimePointsSucceed:
                {
                    CGUIPopUp.instance.Hide();

                    RankDatasCrimePoint rdcp = JsonConvert.DeserializeObject<RankDatasCrimePoint>(netMessage.message);

                    //Player.instance.SetCrimePointsRankUI;

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.GetRankCrimePointsFailed:
                {
                    CGUIPopUp.instance.Hide();

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(""),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.GetRankMoneySucceed:
                {
                    CGUIPopUp.instance.Hide();

                    RankDatasCrimePoint rdcp = JsonConvert.DeserializeObject<RankDatasCrimePoint>(netMessage.message);

                    //Player.instance.SetMoneyRankUI;

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.GetRankMoneyFailed:
                {
                    CGUIPopUp.instance.Hide();

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(""),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.GetRankTotalPvEKillSucceed:
                {
                    CGUIPopUp.instance.Hide();

                    RankDatasCrimePoint rdcp = JsonConvert.DeserializeObject<RankDatasCrimePoint>(netMessage.message);

                    //Player.instance.SetPvEKillRankUI;

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.GetRankTotalPvEKillFailed:
                {
                    CGUIPopUp.instance.Hide();

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(""),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;
            case NetMessageTypes.GetRankTotalPvEDeathSucceed:
                {
                    RankDatasCrimePoint rdcp = JsonConvert.DeserializeObject<RankDatasCrimePoint>(netMessage.message);

                    CGUIPopUp.instance.Hide();
                }
                break;


            case NetMessageTypes.GetRankTotalPvEDeathFailed:
                {
                    CGUIPopUp.instance.Hide();

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(""),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.GetSmugglingItemsSucceed:
                {
                    List<ScriptableItemAndPrice> siap = JsonConvert.DeserializeObject<List<ScriptableItemAndPrice>>(netMessage.message);

                    Player.instance.smugglingItems = siap;

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.GetSmugglingItemsFailed:
                {
                    CGUIPopUp.instance.Hide();

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(""),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.BuySmugglingItemsSucceed:
                {
                    ShopItemDatas sid = JsonConvert.DeserializeObject<ShopItemDatas>(netMessage.message);

                    Player.instance.BuySmugglingItem(sid.index, sid.amount);

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.BuySmugglingItemsFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(reason),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.TryBuySmugglingItemsFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.SellSmugglingItemsSucceed:
                {
                    ShopItemDatas sid = JsonConvert.DeserializeObject<ShopItemDatas>(netMessage.message);

                    Player.instance.SellSmugglingItem(sid.index, sid.amount);

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.SellSmugglingItemsFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(reason),
                    "OK", () => { CGUIPopUp.instance.Hide(); });

                }
                break;

            case NetMessageTypes.TrySellSmugglingItemsFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.ArenaAttackSucceed:
                {
                    ArenaAttackDatas aad = JsonConvert.DeserializeObject<ArenaAttackDatas>(netMessage.message);

                    ArenaAttackDatas earnedValues = new ArenaAttackDatas();
                    earnedValues.curCrimePoints = aad.curCrimePoints - Player.instance.crimePoints;
                    earnedValues.curMoney = aad.curMoney - Player.instance.money;
                    earnedValues.curHealth = aad.curHealth - Player.instance.curHealth;

                    aad.CopyPlayer(Player.instance);

                    Player.instance.totalArenaKill++;

                    CGUIPopUp.instance.Hide();

                    CGUIPopUpWinLose.instance.ShowInfo(LocalizationManager.GetTermTranslation("WIN"),
                       aad.enemyName + " - " + LocalizationManager.GetTermTranslation("arenaAttackSucceed"),
                       "OK", () => {
                           GetArenaListRequest();
                           CGUIPopUpWinLose.instance.Hide();
                       },
                         earnedValues.curCrimePoints, earnedValues.curMoney, earnedValues.curHealth, true, () => { GetArenaListRequest(); }, CGUIPopUpWinLoseTypes.Win);

                    /*CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(aad.enemyName) + " - " + LocalizationManager.GetTermTranslation("arenaAttackSucceed"),
                    "OK", () => {
                        CGUIPopUp.instance.Hide();
                        GetArenaListRequest();
                    });*/

                    if (GameAnalyticsService.instance != null)
                    {
                        GameAnalyticsService.instance.AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, "ArenaAttack", 1);
                    }
                    //Başarılı ekranı aç,
                    //anim oynat,
                    //kazancı göster
                }
                break;

            case NetMessageTypes.ArenaAttackFailed:
                {
                    ArenaAttackDatas aad = JsonConvert.DeserializeObject<ArenaAttackDatas>(netMessage.message);

                    aad.CopyPlayer(Player.instance);

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                    LocalizationManager.GetTermTranslation(aad.enemyName) + " - " + LocalizationManager.GetTermTranslation("arenaAttackFailed"),
                    "OK", () => {
                        CGUIPopUp.instance.Hide();
                        GetArenaListRequest();
                    });

                    //başarısız ekranı aç
                    //anim oynat
                    //kaybettiğini göster

                    //düşman ismi'ni yenemedin

                }
                break;

            case NetMessageTypes.TryArenaAttackFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.GetArenaListSucceed:
                {
                    List<ArenaListDatas> ald = JsonConvert.DeserializeObject<List<ArenaListDatas>>(netMessage.message);

                    Player.instance.arenaPlayerList = ald;

                    CGUIPopUp.instance.Hide();
                    //Başarılı ekranı aç,
                    //anim oynat,
                    //kazancı göster
                }
                break;

            case NetMessageTypes.GetArenaListFailed:
                {
                    CGUIPopUp.instance.Hide();

                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.SendChatMessage:
                {
                    ChatMessage cm = JsonConvert.DeserializeObject<ChatMessage>(netMessage.message);

                    UIChat.instance.AddMessage(cm);
                }
                break;

            case NetMessageTypes.SetServerTime:
                {
                    DateTime serverTime = JsonConvert.DeserializeObject<SYSTEMTIME>(netMessage.message).Get();

                    if (GameManager.instance != null)
                    {
                        GameManager.instance.serverTime = serverTime;
                        GameManager.instance.serverMinusClientTimeSpan = serverTime - DateTime.UtcNow;
                    }
                }
                break;

            case NetMessageTypes.BroadcastGuildData:
                {
                    Guild guild = JsonConvert.DeserializeObject<Guild>(netMessage.message);

                    Player.instance.guild = guild;
                }
                break;

            case NetMessageTypes.SetInvitedGuilds:
                {
                    List<string> invitedGuilds = JsonConvert.DeserializeObject<List<string>>(netMessage.message);

                    Player.instance.invitedGuilds = invitedGuilds;
                }
                break;

            case NetMessageTypes.SendGuildNamesForApplyRequest:
                {
                    List<string> guildListForApply = JsonConvert.DeserializeObject<List<string>>(netMessage.message);

                    Player.instance.guildListForApply = guildListForApply;

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.SyncMails:
                {
                    string mails = netMessage.message;

                    if (String.IsNullOrEmpty(mails))
                    {
                        Player.instance.mails = new List<Mail>();
                    }
                    else
                    {
                        Player.instance.mails = JsonConvert.DeserializeObject<List<Mail>>(mails);
                    }
                }
                break;

            case NetMessageTypes.GetItemsFromMailSucceed:
                {
                    MailInventory mi = JsonConvert.DeserializeObject<MailInventory>(netMessage.message);

                    Player.instance.inventory = mi.inventory;
                    Player.instance.mails = mi.mails;

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.GetItemsFromMailFailed:
                {
                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation("getitemsfrommailfailed") + " - " + LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.StealMoneySucceed:
                {
                    StealMoneyDatas smd = JsonConvert.DeserializeObject<StealMoneyDatas>(netMessage.message);

                    //aad.enemyName 'i yendin

                    int moneyDifference = smd.curMoney - Player.instance.money;

                    smd.CopyPlayer(Player.instance);

                    Player.instance.totalArenaKill++;

                    CGUIPopUp.instance.Hide();

                    CGUIPopUpWinLose.instance.ShowStatus(LocalizationManager.GetTermTranslation("WIN"), LocalizationManager.GetTermTranslation("StealMoneySucceed"),
                        0, moneyDifference, 0, true, null, CGUIPopUpWinLoseTypes.Win);
                }
                break;

            case NetMessageTypes.StealMoneyFailed:
                {
                    string reason = netMessage.message;

                    Player.instance.totalArenaDeath++;

                    float vipBonus = Player.instance.vip.hash != 0 ? Player.instance.vip.data.stealMoneyAttackTimeDecreaser : 0;

                    Player.instance.lastStealMoneyTime = GameManager.instance.ServerTime + new TimeSpan(0, 0, Mathf.FloorToInt(GameManager.instance.stealMoneyAttackTime * (1 - vipBonus / 100)));


                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation("stealmoneyfailed") + " - " + LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.TryStealMoneyFailed:
                {
                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.GetStealMoneyPlayersListSucceed:
                {
                    List<string> stealMoneyPlayerList = JsonConvert.DeserializeObject<List<string>>(netMessage.message);

                    Player.instance.stealMoneyPlayerList = stealMoneyPlayerList;

                    CGUIPopUp.instance.Hide();
                }
                break;

            case NetMessageTypes.GetStealMoneyPlayersListFailed:
                {
                    string reason = netMessage.message;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation("getStealMoneyPlayerListFailed") + " - " + LocalizationManager.GetTermTranslation(reason),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.InAppPurchaseSucceed:
                {
                    InAppPurchaseDatas iapData = JsonConvert.DeserializeObject<InAppPurchaseDatas>(netMessage.message);

                    ScriptableVIP vip = new ScriptableVIP();

                    switch (iapData.definitionId)
                    {
                        case "VIP1":
                            vip = ScriptableVIP.dict["Vip1".GetStableHashCode()];
                            vip.Apply(Player.instance);
                            GameAnalyticsService.instance.AddBusinessEvent("Vip1", 1, "VIP", "VIP1");
                            break;
                        case "VIP2":
                            vip = ScriptableVIP.dict["Vip2".GetStableHashCode()];
                            vip.Apply(Player.instance);
                            GameAnalyticsService.instance.AddBusinessEvent("Vip2", 1, "VIP", "VIP2");
                            //TODO: GameAnalytics
                            break;
                        case "VIP3":
                            vip = ScriptableVIP.dict["Vip3".GetStableHashCode()];
                            vip.Apply(Player.instance);
                            GameAnalyticsService.instance.AddBusinessEvent("Vip3", 1, "VIP", "VIP3");
                            //TODO: GameAnalytics
                            break;
                        case "VIP4":
                            vip = ScriptableVIP.dict["Vip4".GetStableHashCode()];
                            vip.Apply(Player.instance);
                            GameAnalyticsService.instance.AddBusinessEvent("Vip4", 1, "VIP", "VIP4");
                            //TODO: GameAnalytics
                            break;

                        default:
                            break;
                    }
                }
                break;

            case NetMessageTypes.GetFreeEnergySucceed:
                {
                    int curHealth = Convert.ToInt32(netMessage.message);

                    Player.instance.SetCurHealth(curHealth);

                    Player.instance.lastEnergyCollected = GameManager.instance.ServerTime;

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation("GetFreeEnergySucceed"),
                     "OK", () => { CGUIPopUp.instance.Hide(); });


                }
                break;

            case NetMessageTypes.GetFreeEnergyFailed:
                {
                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation("GetFreeEnergyFailed"),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            case NetMessageTypes.GetFreeMoneySucceed:
                {
                    int money = Convert.ToInt32(netMessage.message);

                    Player.instance.ChangeMoney(money);

                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation("GetFreeMoneySucceed") + " - " + money,
                     "OK", () => { CGUIPopUp.instance.Hide(); });


                }
                break;

            case NetMessageTypes.GetFreeMoneyFailed:
                {
                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                     LocalizationManager.GetTermTranslation("GetFreeMoneyFailed"),
                     "OK", () => { CGUIPopUp.instance.Hide(); });
                }
                break;

            default:

                break;
        }
    }

   

    public void SendData(NetworkMessage networkMessage)
    {
        _netClient.Send(networkMessage.ObjectToByteArray());
    }

    public void RegisterRequest(string mail, string mail2, string password, string password2)
    {
        PlayerMailPassword pmp = new PlayerMailPassword();
        pmp.version = version;
        pmp.mail = mail;
        pmp.mail2 = mail2;
        pmp.password = password;
        pmp.password2 = password2;

        //check mails are same?
        //check passwords are same?
        //check mail correct?

        if(mail == mail2 && !String.IsNullOrEmpty(mail))
        {
            if(password == password2 && !String.IsNullOrEmpty(password))
            {
                if(password.Length >= GameManager.instance.minPasswordLenght)
                {
                    if (GameUtils.IsValidEmail(mail))
                    {
                        SendData(new NetworkMessage(NetMessageTypes.RegisterRequest, JsonConvert.SerializeObject(pmp)));
                    }
                    else
                    {
                        //mail adress is not valid
                        CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Error"),
                            LocalizationManager.GetTermTranslation("Mailaddressisnotvalid"),
                            LocalizationManager.GetTermTranslation("Retry"), () => CGUIPopUp.instance.Hide());
                    }
                }
                else
                {
                    //password is too short 
                    CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Error"),
                    LocalizationManager.GetTermTranslation("Passwordistooshort"),
                            LocalizationManager.GetTermTranslation("Retry"), () => CGUIPopUp.instance.Hide());
                }
            }
            else
            {
                //passwords are not same
                CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Error"),
                    LocalizationManager.GetTermTranslation("Passwordsarenotsame"),
                    LocalizationManager.GetTermTranslation("Retry"), () => CGUIPopUp.instance.Hide());
            }
        }
        else
        {
            //mails are not same
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Error"),
                LocalizationManager.GetTermTranslation("Mailsarenotsame"),
                LocalizationManager.GetTermTranslation("Retry"), () => CGUIPopUp.instance.Hide());
        }
    }
    public void LoginRequest(string mail, string password)
    {
        PlayerMailPassword pmp = new PlayerMailPassword();
        pmp.version = version;
        pmp.mail = mail;
        pmp.password = password;
        
        SendData(new NetworkMessage(NetMessageTypes.LoginRequest, JsonConvert.SerializeObject(pmp)));
    }

    public void SetCharnameRequest(string charname)
    {
        SendData(new NetworkMessage(NetMessageTypes.SetCharnameRequest, charname));

        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("Characteriscreating"));
    }
    public void CommitCrimeRequest(int enemyHash)
    {
        if (!CanAttack())
            return;

        
        SendData(new NetworkMessage(NetMessageTypes.CommitCrimeRequest, enemyHash.ToString())); 

        
            

        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("CommitingCrime"));
    }

    public void BuyRealEstateRequest(int realEstateHash)
    {

        SendData(new NetworkMessage(NetMessageTypes.BuyRealEstateRequest, realEstateHash.ToString())); 

        
            

        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("buyingRealEstate"));
    }

    public void GetIncomeRealEstateRequest(int realEstateHash)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.GetIncomeRealEstateRequest, realEstateHash.ToString())); 

        
            

        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("gettingIncomeRealEstate"));
    }

    public void BuyManagementRequest(int managementHash)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.BuyManagementRequest, managementHash.ToString())); 

        
            

        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("buyingManagement"));
    }

    public void GetIncomeManagementRequest(int managementHash)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.GetIncomeManagementRequest, managementHash.ToString())); 

        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("gettingIncomeManagement"));
    }

    public void TrainRequest(string trainType)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.TrainRequest, trainType)); 

        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("training"));
    }
    public void GiveABribeRequest()
    {
        
        SendData(new NetworkMessage(NetMessageTypes.GiveABribeRequest, ""));        
            
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("givingABribe"));
    }
    public void EscapeFromPrisonRequest()
    {
        
        SendData(new NetworkMessage(NetMessageTypes.EscapeFromPrisonRequest, "")); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("tryingEscape"));
    }

    public void PutMoneyBank(bool fromBankToInventory, int money)
    {
        PlayerMoneyBankMoney pmbm = new PlayerMoneyBankMoney();
        pmbm.money = money;
        pmbm.fromBankToInventory = fromBankToInventory;

        
        SendData(new NetworkMessage(NetMessageTypes.MoneyBankTransferRequest, JsonConvert.SerializeObject(pmbm))); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("PutingMoney"));
    }
    public void DoStoryRequest(int storyHash)
    {
        if (!CanAttack())
            return;

        
        SendData(new NetworkMessage(NetMessageTypes.DoStoryRequest, storyHash.ToString())); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("DoingStory"));
    }

    public void SwapItemRequest(int invNo, int eqNo)
    {
        SwapItemDatas sid = new SwapItemDatas();
        sid.invNo = invNo;
        sid.eqNo = eqNo;

        
        SendData(new NetworkMessage(NetMessageTypes.SwapItemRequest, JsonConvert.SerializeObject(sid))); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("swappingItems"));
    }

    public void BuyShopItemRequest(PlayerEquipmentTypes eqType, int index, int amount = 1)
    {
        ShopItemDatas sid = new ShopItemDatas();
        sid.eqType = eqType;
        sid.index = index;
        sid.amount = amount;

        
        SendData(new NetworkMessage(NetMessageTypes.BuyShopItemRequest, JsonConvert.SerializeObject(sid))); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("swappingItems"));
    }

    public void SellShopItemRequest(int index, int amount)
    {
        ShopItemDatas sid = new ShopItemDatas();
        sid.index = index;
        sid.amount = amount;

        
        SendData(new NetworkMessage(NetMessageTypes.SellShopItemRequest, JsonConvert.SerializeObject(sid))); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("swappingItems"));
    }

    public void GetRankRequest(RankDataTypes rankType, int pageNo, int userPerPage)
    {
        GetRankDatas grd = new GetRankDatas();
        grd.myRank = false;
        grd.rankDataType = rankType;
        grd.pageNo = pageNo;
        grd.userPerPage = userPerPage;
        
        switch (rankType)
        {
            case RankDataTypes.crimepoints:
                SendData(new NetworkMessage(NetMessageTypes.GetRankCrimePointsRequest, JsonConvert.SerializeObject(grd))); 
                break;
            case RankDataTypes.money:
                SendData(new NetworkMessage(NetMessageTypes.GetRankMoneyRequest, JsonConvert.SerializeObject(grd))); 
                break;
            case RankDataTypes.totalPvEKill:
                SendData(new NetworkMessage(NetMessageTypes.GetRankTotalPvEKillRequest, JsonConvert.SerializeObject(grd))); 
                break;
            case RankDataTypes.totalPvEDeath:
                SendData(new NetworkMessage(NetMessageTypes.GetRankTotalPvEDeathRequest, JsonConvert.SerializeObject(grd))); 
                break;
            default:
                break;
        }
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("gettingRankDatas"));
    }
    public void GetMyRankRequest(RankDataTypes rankType, int userPerPage)
    {
        GetRankDatas grd = new GetRankDatas();
        grd.myRank = true;
        grd.rankDataType = rankType;
        grd.userPerPage = userPerPage;

        
        switch (rankType)
        {
            case RankDataTypes.crimepoints:
                SendData(new NetworkMessage(NetMessageTypes.GetRankCrimePointsRequest, JsonConvert.SerializeObject(grd))); 
                break;
            case RankDataTypes.money:
                SendData(new NetworkMessage(NetMessageTypes.GetRankMoneyRequest, JsonConvert.SerializeObject(grd))); 
                break;
            case RankDataTypes.totalPvEKill:
                SendData(new NetworkMessage(NetMessageTypes.GetRankTotalPvEKillRequest, JsonConvert.SerializeObject(grd))); 
                break;
            case RankDataTypes.totalPvEDeath:
                SendData(new NetworkMessage(NetMessageTypes.GetRankTotalPvEDeathRequest, JsonConvert.SerializeObject(grd))); 
                break;
            default:
                break;
        }
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("gettingRankDatas"));
    }


    public void BuySmugglingItemRequest(int index, int amount)
    {
        ShopItemDatas sid = new ShopItemDatas();
        sid.index = index;
        sid.amount = amount;

        
        SendData(new NetworkMessage(NetMessageTypes.BuySmugglingItemsRequest, JsonConvert.SerializeObject(sid))); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("buyingSmugglingItems"));
    }
    public void SellSmugglingItemRequest(int index, int amount = 1)
    {
        ShopItemDatas sid = new ShopItemDatas();
        sid.index = index;
        sid.amount = amount;

        
        SendData(new NetworkMessage(NetMessageTypes.SellSmugglingItemsRequest, JsonConvert.SerializeObject(sid))); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("swappingItems"));
    }

    public void ArenaAttackRequest(string enemyName)
    {
        if (!CanAttack())
            return;

        
        SendData(new NetworkMessage(NetMessageTypes.ArenaAttackRequest, enemyName)); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("arenaAttacking"));
    }

    public void GetArenaListRequest()
    {
        
        SendData(new NetworkMessage(NetMessageTypes.GetArenaListRequest, "")); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("gettingArenaList"));
    }

    public void SendChatMessage(ChatMessage cm)
    {
        SendData(new NetworkMessage(NetMessageTypes.SendChatMessage, JsonConvert.SerializeObject(cm)));  
    }

    public void CreateGuildRequest(string guildName)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.CreateGuildRequest, guildName)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("creatingGuild"));*/
    }

    public void LeaveGuildRequest()
    {
        
        SendData(new NetworkMessage(NetMessageTypes.LeaveGuildRequest, "")); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("leavingGuild"));*/
    }
    public void TerminateGuildRequest()
    {
        
        SendData(new NetworkMessage(NetMessageTypes.TerminateGuildRequest, "")); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("termiatingGuild"));*/
    }
    public void SetGuildNoticeRequest(string notice)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.SetGuildNoticeRequest, notice)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("settingGuildNotice"));*/
    }
    public void KickFromGuildRequest(string member)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.KickFromGuildRequest, member)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("kickingFromGuild"));*/
    }
    public void InviteToGuildRequest(string member)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.InviteToGuildRequest, member)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("invitingToGuild"));*/
    }
    public void ApplyToGuildRequest(string guildName)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.ApplyToGuildRequest, guildName)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("applyingGuild"));*/
    }
    public void PromoteMemberRequest(string member)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.PromoteMemberRequest, member)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("promotingMember"));*/
    }
    public void DemoteMemberRequest(string member)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.DemoteMemberRequest, member)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("demotingMember"));*/
    }

    public void GetGuildNamesForApplyRequest(string guildName)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.GetGuildNamesForApplyRequest, guildName)); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("gettingGuildNamesForApplyRequest"));
    }

    public void AcceptGuildRequest(string guildName)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.AcceptGuildRequest, guildName)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("acceptingGuildRequest"));*/
    }

    public void RejectGuildRequest(string guildName)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.RejectGuildRequest, guildName)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("rejectingGuildRequest"));*/
    }

    public void AcceptGuildApplyRequest(string appliedName)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.AcceptGuildApplyRequest, appliedName)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("acceptingGuildApplyRequest"));*/
    }

    public void RejectGuildApplyRequest(string appliedName)
    {
        
        SendData(new NetworkMessage(NetMessageTypes.RejectGuildApplyRequest, appliedName)); 
        
        /*CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("rejectingGuildApplyRequest"));*/
    }

    
    public void GetItemsFromMailRequest()
    {
        
        SendData(new NetworkMessage(NetMessageTypes.GetItemsFromMailRequest, "")); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("gettingItemsFromMail"));
    }

    public void GetStealMoneyPlayersList()
    {
        
        SendData(new NetworkMessage(NetMessageTypes.GetStealMoneyPlayersList, "")); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("gettingPlayerListForStealMoney"));
    }

    public void StealMoneyRequest(string enemyName)
    {
        if (!CanAttack())
            return;

        
        SendData(new NetworkMessage(NetMessageTypes.StealMoneyRequest, enemyName)); 
        
        CGUIPopUp.instance.ShowStatus(LocalizationManager.GetTermTranslation("Info"),
                                LocalizationManager.GetTermTranslation("stealingMoney"));
    }
    

    public bool CanAttack()
    {
        Player player = Player.instance;

        if (player == null)
            return false;
        
        int weaponHash = 0;
        int armorsHash = 0;

        weaponHash = player.equipments[0].hash;

        for (int i = 1; i < Enum.GetNames(typeof(PlayerEquipmentTypes)).Length; i++)
        {
            armorsHash += player.equipments[i - 1].hash;
        }

        armorsHash -= weaponHash;
        armorsHash -= player.equipments[6].hash; // bag hash



        int inventoryNo = player.inventory.FindIndex(x => x.amount > 0 && x.item.hash != 0 && x.item is EquipmentItem);


        if (inventoryNo != -1 && (weaponHash == 0 || armorsHash == 0))
        {
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                        LocalizationManager.GetTermTranslation("Youneedtoequip"),
                                    "OK", () => { UIManager.instance.OpenInventoryMenuPanel(true); CGUIPopUp.instance.Hide(); });

            return false;
        }
        


        if (weaponHash == 0 || armorsHash == 0)
        {
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                        LocalizationManager.GetTermTranslation("Youneedweaponandarmors"),
                                    "OK", () => { UIManager.instance.OpenShopMenuPanel(true); CGUIPopUp.instance.Hide(); });

            return false;
        }
        


        if (player.intelligence == 1 && player.strength == 1)
        {
            CGUIPopUp.instance.ShowInfo(LocalizationManager.GetTermTranslation("Info"),
                                        LocalizationManager.GetTermTranslation("Youneedtotrain"),
                                    "OK", () => { UIManager.instance.OpenShootingRangePanel(true); CGUIPopUp.instance.Hide(); });

            return false;
        }


        return true;
    }

    public void InAppPurchaseRequest(InAppPurchaseDatas _iapData)
    {
        InAppPurchaseDatas iapData = _iapData;
                
        SendData(new NetworkMessage(NetMessageTypes.InAppPurchaseRequest, JsonConvert.SerializeObject(iapData))); 
  
    }

    public void GetFreeEnergyRequest()
    {
        
        SendData(new NetworkMessage(NetMessageTypes.GetFreeEnergyRequest, "")); 
        
    }

    public void GetFreeMoneyRequest()
    {
        
        SendData(new NetworkMessage(NetMessageTypes.GetFreeMoneyRequest, "")); 
        
    }


    public void OnApplicationFocus(bool focus)
    {
        if(focus)
        {
            if(!isConnected)
            {
                //ConnectToServer();

                CGUIPopUp.instance.ShowQuestion(LocalizationManager.GetTermTranslation("Disconnected"),
                LocalizationManager.GetTermTranslation("reconnect"),
                "IPv4", () => ConnectWithIPv4(),
                "IPv6", () => ConnectWithIPv6());
            }
        }
    }

    public void OnApplicationQuit()
    {
        if (_netClient != null && isConnected)
        {
            _netClient.Disconnect();
        }
    }
}
