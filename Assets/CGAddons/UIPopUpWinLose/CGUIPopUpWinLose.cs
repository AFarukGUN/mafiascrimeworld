﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public partial class CGUIPopUpWinLose : MonoBehaviour {

    public static CGUIPopUpWinLose instance;

    public GameObject panel;
    public Button buttonBackground;
    public Text Header;
    public Text Description;
    public Button YesButton;
    public Button NoButton;
    public Button backgroundButton;
    public Text YesButtonText;
    public Text NoButtonText;

    public Text textCrimePoint;
    public Text textMoney;
    public Text textHealth;

    public CGUIPopUpWinLoseTypes CGUIPopUpWinLoseType;

    public List<GameObject> PlayerWins = new List<GameObject>();
    public List<GameObject> EnemyLoses = new List<GameObject>();

    private void Awake()
    {
        instance = this;
    }

    public void ShowInfo(string header, string description, 
        string yesButtonText, UnityAction yesButtonAction, 
        int crimePoints = 0, int money = 0, int health = 0, 
        bool backgroundButtonEnable = true, UnityAction backgroundButtonAction = null,
        CGUIPopUpWinLoseTypes _CGUIPopUpWinLoseType = CGUIPopUpWinLoseTypes.None)
    {
        Header.text = header;
        Description.text = description;
        YesButtonText.text = yesButtonText;

        Header.gameObject.SetActive(true);
        Description.gameObject.SetActive(true);
        YesButton.gameObject.SetActive(true);
        NoButton.gameObject.SetActive(false);

        YesButton.onClick.RemoveAllListeners();
        YesButton.onClick.AddListener((yesButtonAction));

        backgroundButton.onClick.RemoveAllListeners();

        if (backgroundButtonAction != null)
            backgroundButton.onClick.AddListener(backgroundButtonAction);

        CGUIPopUpWinLoseType = _CGUIPopUpWinLoseType;

        textCrimePoint.text = crimePoints.ToString();
        textMoney.text = money.ToString();
        textHealth.text = health.ToString();

        buttonBackground.enabled = backgroundButtonEnable;

        if (_CGUIPopUpWinLoseType == CGUIPopUpWinLoseTypes.Win)
        {
            int randomInt = Random.Range(0, PlayerWins.Count - 1);

            if (randomInt >= 0 && PlayerWins.Count > randomInt)
            {
                PlayerWins[randomInt].SetActive(true);
            }

            randomInt = Random.Range(0, EnemyLoses.Count - 1);

            if (randomInt >= 0 && EnemyLoses.Count > randomInt)
            {
                EnemyLoses[randomInt].SetActive(true);
            }
        }

        panel.SetActive(true);
    }

    public void ShowQuestion(string header, string description, string yesButtonText, UnityAction yesButtonAction,
        string noButtonText, UnityAction noButtonAction,
        int crimePoints = 0, int money = 0, int health = 0, bool backgroundButtonEnable = true, UnityAction backgroundButtonAction = null,
        CGUIPopUpWinLoseTypes _CGUIPopUpWinLoseType = CGUIPopUpWinLoseTypes.None)
    {
        Header.text = header;
        Description.text = description;
        YesButtonText.text = yesButtonText;
        NoButtonText.text = noButtonText;

        Header.gameObject.SetActive(true);
        Description.gameObject.SetActive(true);
        YesButton.gameObject.SetActive(true);
        NoButton.gameObject.SetActive(true);

        YesButton.onClick.RemoveAllListeners();
        YesButton.onClick.AddListener((yesButtonAction));

        backgroundButton.onClick.RemoveAllListeners();

        if (backgroundButtonAction != null)
            backgroundButton.onClick.AddListener(backgroundButtonAction);

        NoButton.onClick.RemoveAllListeners();
        NoButton.onClick.AddListener((noButtonAction));

        CGUIPopUpWinLoseType = _CGUIPopUpWinLoseType;

        textCrimePoint.text = crimePoints.ToString();
        textMoney.text = money.ToString();
        textHealth.text = health.ToString();

        buttonBackground.enabled = backgroundButtonEnable;

        if (_CGUIPopUpWinLoseType == CGUIPopUpWinLoseTypes.Win)
        {
            int randomInt = Random.Range(0, PlayerWins.Count - 1);

            if (randomInt >= 0 && PlayerWins.Count > randomInt)
            {
                PlayerWins[randomInt].SetActive(true);
            }

            randomInt = Random.Range(0, EnemyLoses.Count - 1);

            if (randomInt >= 0 && EnemyLoses.Count > randomInt)
            {
                EnemyLoses[randomInt].SetActive(true);
            }
        }

        panel.SetActive(true);
    }

    public void ShowStatus(string header, string description, int crimePoints = 0, int money = 0, int health = 0,
        bool backgroundButtonEnable = true, UnityAction backgroundButtonAction = null
    , CGUIPopUpWinLoseTypes _CGUIPopUpWinLoseType = CGUIPopUpWinLoseTypes.None)
    {
        Header.text = header;
        Description.text = description;

        Header.gameObject.SetActive(true);
        Description.gameObject.SetActive(true);
        YesButton.gameObject.SetActive(false);
        NoButton.gameObject.SetActive(false);

        CGUIPopUpWinLoseType = _CGUIPopUpWinLoseType;

        textCrimePoint.text = crimePoints.ToString();
        textMoney.text = money.ToString();
        textHealth.text = health.ToString();

        buttonBackground.enabled = backgroundButtonEnable;

        
        backgroundButton.onClick.RemoveAllListeners();

        if(backgroundButtonAction != null)
        backgroundButton.onClick.AddListener(backgroundButtonAction);

        if (_CGUIPopUpWinLoseType == CGUIPopUpWinLoseTypes.Win)
        {
            int randomInt =  Random.Range(0, PlayerWins.Count - 1);

            if(randomInt >= 0 && PlayerWins.Count > randomInt)
            {
                PlayerWins[randomInt].SetActive(true);
            }

            randomInt = Random.Range(0, EnemyLoses.Count - 1);

            if (randomInt >= 0 && EnemyLoses.Count > randomInt)
            {
                EnemyLoses[randomInt].SetActive(true);
            }
        }

        panel.SetActive(true);
    }

    public void Hide()
    {
        panel.SetActive(false);
        
        foreach(GameObject go in PlayerWins)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in EnemyLoses)
        {
            go.SetActive(false);
        }
    }
}

public enum CGUIPopUpWinLoseTypes
{
    Win = 0,
    Lost = 1,
    None = 2,
}

