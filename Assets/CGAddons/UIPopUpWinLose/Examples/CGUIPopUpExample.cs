﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class CGUIPopUpWinLoseExample : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        
		if(Input.GetKeyDown(KeyCode.Q))
        {
            //Question Exaple
            CGUIPopUpWinLose.instance.ShowQuestion("1 Question", "Do You Like This Addon", "YES", () => CGUIPopUpWinLose.instance.Hide(), "NO", () => CGUIPopUpWinLose.instance.Hide());
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            //Status Example
            CGUIPopUpWinLose.instance.ShowStatus("Connecting", "Connecting To Server");
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            //Info Example
            CGUIPopUpWinLose.instance.ShowInfo("Logged - In", "Logged Successfully", "OK", () => CGUIPopUpWinLose.instance.Hide());
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            //Hide Example
            CGUIPopUpWinLose.instance.Hide();
        }
    }

}
