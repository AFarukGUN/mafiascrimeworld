-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 06 Mar 2020, 05:27:15
-- Sunucu sürümü: 10.4.11-MariaDB
-- PHP Sürümü: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `mafiascrimeworld`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `header` varchar(50) NOT NULL,
  `text` varchar(500) NOT NULL,
  `imageLink` varchar(500) NOT NULL,
  `clickLink` varchar(500) NOT NULL,
  `active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `announcements`
--

INSERT INTO `announcements` (`id`, `header`, `text`, `imageLink`, `clickLink`, `active`) VALUES
(1, 'header1', 'text1', 'https://scontent.fist4-1.fna.fbcdn.net/v/t1.0-9/31433666_1692364497523640_700521037267206144_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=mPZ2QmQTxRMAX8RP3UI&_nc_ht=scontent.fist4-1.fna&oh=48cce25b83eb8081128d945ec36b305b&oe=5EEF6AB8', 'clicklink1', 1),
(2, 'header2', 'text2', 'imagelink2', 'clicklink2', 0),
(3, 'header3', 'text1', 'https://scontent.fist4-1.fna.fbcdn.net/v/t1.0-9/31433666_1692364497523640_700521037267206144_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_ohc=mPZ2QmQTxRMAX8RP3UI&_nc_ht=scontent.fist4-1.fna&oh=48cce25b83eb8081128d945ec36b305b&oe=5EEF6AB8', 'clicklink1', 1),
(4, 'header4', 'text2', 'https://media.discordapp.net/attachments/680332363196399619/681211182212055172/unknown.png?width=1202&height=677', 'clicklink2', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `guilds`
--

CREATE TABLE `guilds` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `notice` varchar(500) NOT NULL DEFAULT '',
  `crimePoints` int(11) NOT NULL DEFAULT 0,
  `money` int(11) NOT NULL DEFAULT 0,
  `appliedPlayers` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `guilds`
--

INSERT INTO `guilds` (`id`, `name`, `notice`, `crimePoints`, `money`, `appliedPlayers`) VALUES
(5, 'OTTOMAN', 'Hello World 2!', 0, 0, '');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `orderNo` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` int(2) NOT NULL,
  `mail` varchar(75) NOT NULL,
  `password` varchar(50) NOT NULL,
  `banned` int(1) NOT NULL DEFAULT 0,
  `bannOpenDate` varchar(50) NOT NULL DEFAULT '',
  `created` varchar(50) NOT NULL DEFAULT '',
  `lastlogin` varchar(50) NOT NULL DEFAULT '',
  `charname` varchar(50) DEFAULT NULL,
  `strength` float NOT NULL DEFAULT 0,
  `intelligence` float NOT NULL DEFAULT 0,
  `playerGrade` int(11) NOT NULL DEFAULT 0,
  `crimePoints` int(11) NOT NULL DEFAULT 0,
  `money` int(11) NOT NULL DEFAULT 0,
  `totalPvEKill` int(11) NOT NULL DEFAULT 0,
  `totalPvEDeath` int(11) NOT NULL DEFAULT 0,
  `totalArenaKill` int(11) NOT NULL DEFAULT 0,
  `totalArenaDeath` int(11) NOT NULL DEFAULT 0,
  `lastLogout` varchar(50) NOT NULL DEFAULT '',
  `VIP` int(11) DEFAULT 0,
  `inventory` varchar(2500) DEFAULT NULL,
  `equipments` varchar(2500) DEFAULT NULL,
  `curHealth` int(11) NOT NULL DEFAULT 75,
  `lastAttackTime` varchar(50) NOT NULL DEFAULT '',
  `buffs` varchar(2500) DEFAULT NULL,
  `jailFinishTime` varchar(250) NOT NULL DEFAULT '',
  `realEstates` varchar(2500) DEFAULT NULL,
  `managements` varchar(2500) DEFAULT NULL,
  `moneyBank` int(11) NOT NULL DEFAULT 0,
  `lastSucceedStoryNo` int(11) DEFAULT -1,
  `lastArenaAttackTime` varchar(50) NOT NULL,
  `vipTimeEnd` varchar(50) NOT NULL,
  `guild` varchar(50) NOT NULL DEFAULT '',
  `guildRank` int(1) NOT NULL DEFAULT 0,
  `invitedGuilds` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `mail`, `password`, `banned`, `bannOpenDate`, `created`, `lastlogin`, `charname`, `strength`, `intelligence`, `playerGrade`, `crimePoints`, `money`, `totalPvEKill`, `totalPvEDeath`, `totalArenaKill`, `totalArenaDeath`, `lastLogout`, `VIP`, `inventory`, `equipments`, `curHealth`, `lastAttackTime`, `buffs`, `jailFinishTime`, `realEstates`, `managements`, `moneyBank`, `lastSucceedStoryNo`, `lastArenaAttackTime`, `vipTimeEnd`, `guild`, `guildRank`, `invitedGuilds`) VALUES
(1, 'vadsl05@gmail.com', '9cbf8a4dcb8e30682b927f352d6559a0', 0, '', '', '2020-03-06 04:13:27.477049', 'XAfgun', 10, 11, 0, 75, 999, 0, 0, 10, 0, '2020-03-06 04:00:39.380993', 23907435, '[{\"item\":{\"hash\":755054,\"name\":\"Eq1\",\"hideFlags\":0},\"amount\":1},{\"item\":{\"hash\":755055,\"name\":\"Eq2\",\"hideFlags\":0},\"amount\":1},{\"item\":{\"hash\":1350163263,\"name\":\"Cigaratte\",\"hideFlags\":0},\"amount\":2},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0}]', '[{\"hash\":755055},{\"hash\":0},{\"hash\":755054},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":23303648}]', 100, '4.03.2020 13:56:37', NULL, '', '[{\"hash\":23601826,\"lastCollectTime\":\"2020-02-23T20:00:26.7324079Z\",\"count\":10},{\"hash\":728386889,\"lastCollectTime\":\"2020-02-23T13:38:02.6666024Z\",\"count\":1}]', '[{\"hash\":938464057,\"lastCollectTime\":\"2020-02-23T15:49:33.259171Z\",\"count\":2},{\"hash\":938464057,\"lastCollectTime\":\"2020-02-23T15:56:13.9036785Z\",\"count\":1},{\"hash\":938464057,\"lastCollectTime\":\"2020-02-23T15:56:16.6514613Z\",\"count\":1},{\"hash\":938464057,\"lastCollectTime\":\"2020-02-23T15:56:19.9735651Z\",\"count\":1},{\"hash\":938464057,\"lastCollectTime\":\"2020-02-23T15:56:21.021301Z\",\"count\":1},{\"hash\":938464057,\"lastCollectTime\":\"2020-02-23T16:10:10.7185169Z\",\"count\":1},{\"hash\":938464057,\"lastCollectTime\":\"2020-02-23T16:10:13.2125395Z\",\"count\":1},{\"hash\":938464057,\"lastCollectTime\":\"2020-02-23T16:10:16.1632712Z\",\"count\":1},{\"hash\":938464057,\"lastCollectTime\":\"2020-02-23T16:10:19.0273723Z\",\"count\":1}]', 0, -1, '4.03.2020 13:56:37', '10.03.2020 07:14:59', 'OTTOMAN', 2, ''),
(21, 'vadsl05@gmail.co', '9cbf8a4dcb8e30682b927f352d6559a0', 0, '', '2020-02-21 13:46:15.867872', '2020-03-06 04:24:04.377023', 'asdf', 0, 0, 0, 5, 0, 0, 0, 0, 0, '2020-03-06 04:03:35.604066', 0, '[{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0}]', '[{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0}]', 100, '22.02.2020 07:14:59', NULL, '', NULL, NULL, 0, -1, '1.03.2020 15:18:18', '', '', 0, ''),
(22, 'vadsl05@gmail.com1', '', 0, '', '', '', '1', 0, 0, 0, 96, 0, 0, 0, 0, 0, '', 0, NULL, NULL, 75, '', NULL, '', NULL, NULL, 0, -1, '', '', '', 0, ''),
(23, 'vadsl05@gmail.com2', '', 0, '', '', '', '2', 0, 0, 0, 94, 0, 0, 0, 0, 0, '', 0, NULL, NULL, 75, '', NULL, '', NULL, NULL, 0, -1, '', '', '', 0, ''),
(24, 'vadsl05@gmail.com3', '', 0, '', '', '', '3', 0, 0, 0, 85, 0, 0, 0, 0, 0, '', 0, NULL, NULL, 75, '', NULL, '', NULL, NULL, 0, -1, '', '', '', 0, ''),
(25, 'vadsl05@gmail.com4', '', 0, '', '', '', '4', 0, 0, 0, 75, 0, 0, 0, 0, 0, '', 0, NULL, NULL, 75, '', NULL, '', NULL, NULL, 0, -1, '', '', '', 0, ''),
(26, 'vadsl05@gmail.com5', '', 0, '', '', '', '5', 0, 0, 0, 65, 0, 0, 0, 0, 0, '', 0, NULL, NULL, 75, '', NULL, '', NULL, NULL, 0, -1, '', '', '', 0, ''),
(27, 'vadsl05@gmail.com6', '', 0, '', '', '', '6', 0, 0, 0, 55, 0, 0, 0, 0, 0, '', 0, NULL, NULL, 75, '', NULL, '', NULL, NULL, 0, -1, '', '', '', 0, ''),
(28, 'vadsl05@gmail.com7', '', 0, '', '', '', '7', 0, 0, 0, 25, 0, 0, 0, 0, 2, '', 0, '[{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0}]', '[{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0}]', 75, '29.02.2020 22:24:21', NULL, '', NULL, NULL, 0, -1, '29.02.2020 22:24:21', '', '', 0, ''),
(29, 'vadsl05@gmail.com8', '', 0, '', '', '', '8', 0, 0, 0, 30, 0, 0, 0, 0, 1, '', 0, '[{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0}]', '[{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0}]', 75, '29.02.2020 22:34:54', NULL, '', NULL, NULL, 0, -1, '29.02.2020 22:34:54', '', '', 0, ''),
(30, 'vadsl05@gmail.com9', '', 0, '', '', '', '9', 0, 0, 0, 20, 0, 0, 0, 0, 1, '', 0, '[{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0},{\"item\":null,\"amount\":0}]', '[{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0},{\"hash\":0}]', 75, '29.02.2020 22:22:40', NULL, '', NULL, NULL, 0, -1, '', '', '', 0, '');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `announcements`
--
ALTER TABLE `announcements`
  ADD UNIQUE KEY `id` (`id`);

--
-- Tablo için indeksler `guilds`
--
ALTER TABLE `guilds`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Tablo için indeksler `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orderNo` (`orderNo`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `charname` (`charname`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `guilds`
--
ALTER TABLE `guilds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Tablo için AUTO_INCREMENT değeri `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
